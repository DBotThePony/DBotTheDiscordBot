

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


const stamp = (new Date()).getTime()
import {ConfigInstance} from './app/ConfigInstance'
import {BotInstance} from './app/BotInstance'

process.env['PATH'] = './bin;' + process.env['PATH']

process.on('uncaughtException', (err) => {
	console.error('Uncaught Exception: ', err)
})

let cfg
let mainBotInstance: BotInstance

console.log('Initializing bot...')

try {
	cfg = new ConfigInstance(require('./config.js'))
} catch(err) {
	console.error('---------------------------------------')
	console.error('FATAL: Unable to load config file')
	console.error('---------------------------------------')
	console.error(err)
	process.exit(1)
}

if (cfg) {
	console.log('Found config - token is ' + cfg.token.substr(1, 6) + '<...>; Logging in...')

	try {
		mainBotInstance = new BotInstance(cfg, true)
		/*mainBotInstance.on('sqlInitError', (err) => {
			console.error('---------------------------------------')
			console.error('FATAL: An SQL startup error occured')
			console.error('---------------------------------------')
			console.error(err)
			process.exit(3)
		})*/

		let shutdown = false

		async function doShutdown() {
			if (shutdown) {
				return
			}

			shutdown = true
			console.log('Shutting down bot...')

			const tnum = setTimeout(() => {
				console.log('[!] Forcing process shutdown')
				process.exit(0)
			}, 3000)

			try {
				await mainBotInstance.client.destroy()
			} catch(err) {

			}

			try {
				await mainBotInstance.sql.end()
			} catch(err) {

			}

			console.log('Shutdown successfull')
			clearTimeout(tnum)

			setTimeout(() => {
				process.exit(0)
			}, 200)
		}

		process.on('SIGTERM', doShutdown)
		process.on('SIGQUIT', doShutdown)
		process.on('SIGINT', doShutdown)
	} catch(err) {
		console.error('---------------------------------------')
		console.error('FATAL: Unable to start main bot instance')
		console.error('---------------------------------------')
		console.error(err)
		process.exit(2)
	}
}
