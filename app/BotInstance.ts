

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import events = require('events')
import {ConfigInstance} from './ConfigInstance'
import {CommandHolder} from './commands/CommandHolder'
import {CommandHelper} from './lib/CommandHelper'
import Discord = require('discord.js')
import {registerDefaultCommands} from './commands/DefaultCommands'
import pg = require('pg')
import _fs = require('fs')
import fs = _fs.promises
import http = require('http')
import https = require('https')
import os = require('os')
import crypto = require('crypto')
import numeral = require('numeral')
import querystring = require('querystring')
import {URL} from 'url'
import { RegisterStatusWatchdog } from './modules/BotStatus';
import { RemoveByKeywords } from './modules/RemoveByKeywords';
import { AsyncDBArrayHolder } from '../lib/AsyncDBArrayHolder';
import { PermanentStuff } from './modules/PermaStuff';
import { AutoReactManager } from './modules/AutoReact';
import { LoggingManager } from './modules/Logging';
import { VoiceRole } from './modules/VoiceRole';
import { NoFreedomUnits } from './modules/NoFreedomUnits';

const transformBase = (text: string) => text.replace(/\+/g, 'pp').replace(/\//g, 'ss')

interface BotStorage {
	[key: string]: any
}

interface AntispamStorage {
	// UserID -> UserObject, Score
	// Score should decrease each second
	[key: string]: number
}

class BotInstance extends events.EventEmitter {
	static max_http_filesize = 1024 * 1024 * 64

	client = new Discord.Client({})

	http_agent = new http.Agent({
		keepAlive: true,
		keepAliveMsecs: 20000
	})

	https_agent = new https.Agent({
		keepAlive: true,
		keepAliveMsecs: 20000
	})

	command_helper: CommandHelper
	command_holder: CommandHolder
	keyword_manager: RemoveByKeywords
	role_giver: AsyncDBArrayHolder
	permanent_stuff: PermanentStuff
	reaction_manager: AutoReactManager
	logging_manager: LoggingManager
	voice_role_manager: VoiceRole
	no_freedom_units: NoFreedomUnits

	sql!: pg.Client

	storage: BotStorage = {}
	antispam: AntispamStorage = {}
	antispam_update_timer = setInterval(() => this.updateAntispam(), 3000)

	tmpdir: string
	subdir: string

	get id() { return this.client.user!.id }
	get uid() { return this.client.user!.id }
	get me() { return this.client.user }
	get owners() { return this.config.owners }

	sqlIsValid = false

	connectSQL(doLogin = false) {
		this.sql = new pg.Client(this.config.getSQL()!)

		this.sql.on('error', (err) => {
			if (err.message.match(/connection\s+terminated/i)) {
				// idk there is no err.code
				this.connectSQL()
			}
		})

		this.sql.connect()
		.then(() => {
			this.error('SQL database connection established')

			if (doLogin) {
				this.login()
			}
		})
		.catch(err => {
			this.error('SQL connection error - ' + err)
			this.emit('sqlInitError', err)

			setTimeout(() => {
				this.connectSQL()
			}, 4000)
		})
	}

	constructor(public config: ConfigInstance, doLogin = false) {
		super()

		if (!config.isValidSQL()) {
			throw new Error('Config instance has no valid sql config')
		}

		const sqlConfig = config.getSQL()

		if (!sqlConfig) {
			throw new Error('Config instance has no valid sql config')
		}

		this.client.on('error', (err) => {
			if (!err) {
				return // каво
			}

			if (err.message == 'read ECONNRESET') {
				setTimeout(() => {
					this.login()
				}, 4000)
			}
		})

		this.connectSQL(doLogin)

		this.command_helper = new CommandHelper(this)
		this.command_holder = new CommandHolder(this)
		this.keyword_manager = new RemoveByKeywords(this)
		this.role_giver = new AsyncDBArrayHolder(this, 'role_giver', 'server', 'roles').disableCache(true).castToBigInt()
		this.permanent_stuff = new PermanentStuff(this)
		this.reaction_manager = new AutoReactManager(this)
		this.logging_manager = new LoggingManager(this)
		this.voice_role_manager = new VoiceRole(this)
		this.no_freedom_units = new NoFreedomUnits(this)

		registerDefaultCommands(this.command_holder)

		this.client.on('message', (msg: Discord.Message) => this.onMessage(msg))
		this.client.on('messageUpdate', (msg: Discord.Message, msg2: Discord.Message) => this.onMessageUpdate(msg, msg2))
		this.client.on('messageDelete', (msg: Discord.Message) => this.onMessageDelete(msg))

		this.subdir = crypto.createHash('sha256').update(config.token).digest('hex').substr(0, 12)
		this.tmpdir = os.tmpdir().replace(/\\/g, '/') + '/' + this.subdir

		fs.stat(this.tmpdir).catch(() => {
			fs.mkdir(this.tmpdir).then(() => {
				this.log('Created tmp directory with ID ' + this.subdir)
			}).catch(err => {
				this.error('Unable to create new temporary directory ' + this.tmpdir + '!')
				this.error(err.stack)
			})
		})
	}

	private _activityTimer: NodeJS.Timeout | null = null
	private _isInactive = true
	static inactivityTimeout = 60 * 1000 * 12

	private becomeInactive() {
		this._isInactive = true
		this.client.user!.setStatus('idle')
		this._activityTimer = null
	}

	triggerActivity() {
		if (this._isInactive) {
			this._isInactive = false
			this.client.user!.setStatus('online')

			this._activityTimer = setTimeout(() => {
				this.becomeInactive()
			}, BotInstance.inactivityTimeout)
		}

		if (this._activityTimer != null) {
			clearTimeout(this._activityTimer)
		}

		this._activityTimer = setTimeout(() => {
			this.becomeInactive()
		}, BotInstance.inactivityTimeout)
	}

	log(message: string) {
		process.stdout.write(message + '\n')
		return this
	}

	error(message: string) {
		process.stderr.write(message + '\n')
		return this
	}

	isOwner(id: string) {
		return this.owners.includes(id)
	}

	updateAntispam() {
		//if (this.client.status != 0) {
		//  return
		//}

		for (const ID in this.antispam) {
			const time = this.antispam[ID] - 1

			if (time > 0) {
				this.antispam[ID] = time
			} else {
				delete this.antispam[ID]
			}
		}
	}

	// less weight - stricter check
	checkAntispam(user: Discord.User, weight = 3) {
		if (!this.antispam[user.id]) {
			return true
		}

		if (this.antispam[user.id] >= weight) {
			return false
		}

		return true
	}

	addAntispam(user: Discord.User, weight = 1, limit = 3) {
		if (!this.antispam[user.id]) {
			this.antispam[user.id] = weight
			return true
		}

		if (this.antispam[user.id] >= limit) {
			return false
		}

		this.antispam[user.id] += weight
		return true
	}

	onMessage(msg: Discord.Message) {
		if (msg.author.id == this.client.user!.id) {
			return
		}

		//console.log('call onMessage ', msg.content)
		this.keyword_manager.onMessage(msg)
		.then((status) => {
			//console.log('resolvd onMessage ', msg.content)
			//console.log('with status ', status)
			if (status) {
				this.command_helper.onMessage(msg)

				if (!this.command_holder.call(msg)) {
					this.emit('validMessage', msg)
				}
			}
		}).catch((err) => {
			if (typeof err != 'string') {
				console.error('ERROR WHILE CALLING onMessage OF KeywordsBan!')
				console.error(err)
			}
		})
	}

	onMessageUpdate(oldmsg: Discord.Message, newmsg: Discord.Message) {
		this.keyword_manager.onMessage(newmsg)
		.then((status) => {
			if (status) {
				this.emit('validMessageUpdate', oldmsg, newmsg)
			}
		}).catch(() => {})
	}

	onMessageDelete(msg: Discord.Message) {
		this.emit('validMessageDelete', msg)
	}

	query(...args: any) {
		if (!this.sql) {
			throw new Error('Invalid intiialization')
		}

		return this.sql.query.apply(this.sql, args)
	}

	channel(id: string) {
		return this.client.channels.cache.get(id)
	}

	server(id: string) {
		return this.client.guilds.cache.get(id)
	}

	user(id: string) {
		return this.client.users.cache.get(id)
	}

	statusOnline = false

	online() {
		this.client.user?.setStatus('idle')

		if (!this.statusOnline) {
			this.statusOnline = true
			//RegisterStatusWatchdog(this)
		}
	}

	async login() {
		await this.client.login(this.config.token)
		this.log(`Bot with ID ${this.client.user?.id} is now online`)
		this.online()
	}

	identifyFilename(urlIn: string) {
		const transform = new URL(urlIn)
		const hash = crypto.createHash('sha256').update(urlIn)
		const split = transform.pathname.split('/')
		const filenameext = split[split.length - 1]
		const filename = filenameext.match(/([^\.]+)\.(.*)?/)

		if (filename) {
			const file = filename[1]
			const ext = filename[2]
			const sha = transformBase(hash.digest('hex'))
			const tpath = this.tmpdir + '/' + sha.substr(0, 12) + '.' + ext

			return [sha, file, ext, tpath]
		}

		return null
	}

	protected internalRequest(urlIn: string, headers: {} = {}, method: 'get' | 'post', params?: string | Buffer | {}, redirectCounter = 0): Promise<Buffer> {
		return new Promise((resolve, reject) => {
			if (redirectCounter > 10) {
				reject(new Error('Too many redirects'))
				return
			}

			const url = new URL(urlIn)

			if (!url.protocol.startsWith('http')) {
				throw new TypeError('URL provided is not HTTP')
			}

			const lib = url.protocol == 'http:' ? http : https
			const options = <http.RequestOptions> {
				headers: {
					'User-Agent': `Mozilla/5.0 (NodeJS/${process.version}; ${process.platform}; compatible) DiscordJS/${Discord.version} DevBot/universal`
				},

				method: method == 'get' ? 'GET' : 'POST'
			}

			if (headers) {
				Object.assign(options.headers!, headers)
			}

			const fn = method == 'get' ? lib.get : lib.request

			const request = fn(url, options, (response) => {
				if (response.headers['content-length']) {
					const num = Number(response.headers['content-length'])

					if (num == num) {
						if (num > BotInstance.max_http_filesize) {
							response.destroy()
							reject(new Error('Response is too big: ' + numeral(num).format('0.0a') + 'bytes long'))
						}
					} else {
						reject(new Error('Bad content-length header - ' + response.headers['content-length']))
						response.destroy()
						return
					}
				}

				if (response.statusCode == undefined) {
					reject(new Error('Status code is undefined'))
					return
				}

				if (response.statusCode == 204) {
					resolve(Buffer.from(''))
					return
				}

				if (response.statusCode == 302 || response.statusCode == 301 || response.statusCode == 307) {
					if (!response.headers!.location) {
						reject(new Error('Redirect reply without Location header'))
						return
					}

					this.internalRequest(response.headers!.location!, headers, 'get', undefined, ++redirectCounter).then(resolve).catch(reject)
				}

				if (response.statusCode! >= 400) {
					reject(new Error('Server replied ' + response.statusCode))
					return
				}

				if (response.statusCode != 200 && response.statusCode != 204) {
					resolve(Buffer.from(''))
					return
				}

				let chunks: Buffer[] = []
				let sizeof = 0

				response.on('data', (chunk: Buffer) => {
					chunks.push(chunk)
					sizeof += chunk.length

					if (sizeof > BotInstance.max_http_filesize) {
						response.destroy()
						reject(new Error('Response is too big'))
						chunks = []
					}
				})

				response.on('end', () => {
					const buff = Buffer.allocUnsafe(sizeof)
					let offset = 0

					for (const chunk of chunks) {
						for (let i = 0; i < chunk.length; i++) {
							buff[i + offset] = chunk[i]
						}

						offset += chunk.length
					}

					resolve(buff)
					chunks = []
				})

				response.on('error', (err) => {
					reject(err)
					chunks = []
				})
			})

			if (method == 'post') {
				if (params instanceof Buffer) {
					request.write(params)
				} else if (params instanceof Object) {
					request.write(querystring.stringify(params))
				} else if (params != undefined) {
					request.write(params)
				}

				request.end()
			}
		})
	}

	async get(urlIn: string, headers: {} = {}) {
		return await this.internalRequest(urlIn, headers, 'get')
	}

	async post(urlIn: string, headers: {} = {}, params: {} = {}) {
		return await this.internalRequest(urlIn, headers, 'post', params)
	}

	async downloadIntoCache(urlIn: string): Promise<string> {
		if (urlIn.match(/^\.\/resource\//) && !urlIn.match(/\.\.\//)) {
			return urlIn
		}

		const identify = this.identifyFilename(urlIn)

		if (!identify) {
			throw new TypeError('Unable to resolve URL')
		}

		const tpath = identify[3]

		try {
			await fs.stat(this.tmpdir)
		} catch(err) {
			this.error('Temp directory ' + this.subdir + ' gone missing!')

			fs.mkdir(this.tmpdir).then(() => {
				this.log('Re-created tmp directory with ID ' + this.subdir)
			}).catch(err => {
				this.error('Unable to create new temporary directory ' + this.tmpdir + '!')
				this.error(err.stack)
			})
		}

		let rethrow = false

		try {
			const stat = await fs.stat(tpath)

			if (!stat.isFile()) {
				rethrow = true
				throw new TypeError('Destination already exists and it is not a file!')
			}

			return tpath
		} catch(err) {
			if (rethrow) {
				throw err
			}
		}

		const body = await this.get(urlIn)
		await fs.writeFile(tpath, body)
		return tpath
	}

	toString() {
		return `[object BotInstance[id=${this.id}]]`
	}
}

export {BotInstance}
