

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import Discord = require('discord.js')
import {ParseString} from '../../lib/StringUtil'
import {BotInstance} from '../BotInstance'

const parseUser = /<@!?([0-9]+)>/
const parseRole = /<&([0-9]+)>/
const parseChannel = /<#([0-9]+)>/

export {parseUser, parseRole, parseChannel}

interface CommandFlags {
	parse_user_argument: boolean
	parse_member_argument: boolean
	parse_role_argument: boolean
	parse_channel_argument: boolean
	allow_piping: boolean
}

class CommandContext implements CommandFlags {
	static parseUser = /<@!?([0-9]+)>/
	static parseRole = /<&([0-9]+)>/
	static parseChannel = /<#([0-9]+)>/

	msg: Discord.Message | null = null
	author: Discord.User | null = null
	channel: Discord.TextChannel | Discord.DMChannel | null = null
	member: Discord.GuildMember | null = null
	self: Discord.GuildMember | null = null
	me: Discord.GuildMember | null = null
	server: Discord.Guild | null = null
	userid: string | null = null
	serverid: string | null = null

	edited = false
	editTarget: Discord.Message | null = null

	raw: string = ''
	private _rawArgs: string = ''
	get rawArgs() { return this.piping && this.rawPipe || this._rawArgs }
	args: string[] = []
	get currentArgs() { return this.piping && this.pipeArguments || this.args }
	argsPipes: string[][] = []
	parsedArgs: any[] = []
	parsed = false
	bot: BotInstance

	parse_user_argument = false
	parse_member_argument = false
	parse_role_argument = false
	parse_channel_argument = false
	allow_piping = true

	piping = false
	pipeid = 0

	messages: Discord.Message[] = []

	get sid() { return this.serverid }
	get uid() { return this.userid }
	get guild() { return this.server }
	get user() { return this.author }
	get sender() { return this.author }
	get isInServer() { return this.server != null }
	get isPM() { return this.channel && this.channel.type == 'dm' }
	get isOwner() { return this.uid && this.bot.config.owners.includes(this.uid) || false }
	get inDM() { return typeof this.channel == 'object' && this.channel instanceof Discord.DMChannel }

	constructor(bot: BotInstance, rawInput: string, msg?: Discord.Message) {
		this.raw = rawInput
		this.bot = bot

		if (msg) {
			this.setupMessage(msg)
		}
	}

	grabFieldsFrom(msg: Discord.Message) {
		this.msg = msg
		this.author = msg.author
		this.channel = <Discord.TextChannel | Discord.DMChannel | null> msg.channel
		this.userid = msg.author.id

		if (msg.guild) {
			this.server = msg.guild
			this.self = msg.guild.me
			this.me = msg.guild.me
			this.member = msg.member
			this.serverid = msg.guild.id
		}
	}

	setupMessage(msg: Discord.Message) {
		this.args = []
		this.argsPipes = []
		this.parsedArgs = []
		this.parsed = false

		this.grabFieldsFrom(msg)
	}

	setEdit(editOn: CommandContext) {
		if (!editOn.canEdit) {
			throw new Error('Illegal state of CommandContext provided to .setEdit()')
		}

		if (this.messages.length != 0) {
			throw new Error('Already got sent messages!')
		}

		this.edited = false
		this.editTarget = editOn.getEditMessage()
	}

	unsetEditOn() {
		this.edited = false
		this.editTarget = null
	}

	importFlags(flags: CommandFlags) {
		this.parse_user_argument = flags.parse_user_argument
		this.parse_member_argument = flags.parse_member_argument
		this.parse_role_argument = flags.parse_role_argument
		this.parse_channel_argument = flags.parse_channel_argument
		this.allow_piping = flags.allow_piping
	}

	get canEdit() {
		return this.messages.length == 1
	}

	getEditMessage(): Discord.Message | null {
		return this.messages[0] || null
	}

	clearAll() {
		const promises = []

		for (const message of this.messages) {
			const promise = message.delete()
			promise.catch(console.error)
			promises.push(promise)
		}

		return promises
	}

	get areMessagesAllowed() {
		if (this.isPM) {
			return true
		}

		if (this.server && this.server.me!.hasPermission('ADMINISTRATOR')) {
			return true
		}

		if (this.channel && this.server) {
			const perms = (<Discord.TextChannel> this.channel).permissionsFor(this.server.me!)

			if (perms) {
				return perms.has('SEND_MESSAGES')
			} else {
				return this.server.me!.hasPermission('SEND_MESSAGES')
			}
		}

		return false
	}

	send(content: string, attach?: (Discord.MessageOptions & { split?: false | undefined; }) | Discord.MessageAttachment): Promise<Discord.Message | Discord.Message[]> | null {
		if (!this.channel) {
			if (!this.author) {
				return null
			}

			return this.author.send(content, <any> attach)
		}

		if (this.editTarget && attach) {
			this.editTarget.delete().catch(console.error)
			this.edited = true
			this.editTarget = null
		}

		if (!this.editTarget) {
			const promise = this.areMessagesAllowed ? this.channel.send(content, <any> attach) : this.author != null ? this.author.send(content, <any> attach) : null

			if (promise == null) {
				return null
			}

			promise.catch(console.error)

			promise.then((message) => {
				if (Array.isArray(message)) {
					for (const m of message) {
						this.messages.push(m)
					}
				} else {
					this.messages.push(message)
				}
			})

			return promise
		}

		const promise = this.editTarget.edit(content)

		promise.catch(console.error)

		promise.then((message) => {
			this.messages.push(message)
		})

		return promise
	}

	typing(status: boolean) {
		if (!this.msg) {
			return null
		}

		if (this.areMessagesAllowed) {
			if (status) {
				this.msg.channel.startTyping()
			} else {
				this.msg.channel.stopTyping()
			}
		} else if (this.author && this.author.dmChannel) {
			if (status) {
				this.author.dmChannel.startTyping()
			} else {
				this.author.dmChannel.stopTyping()
			}
		}

		return this.msg
	}

	rawPipe = ''
	pipeCommand: string | null = null
	pipeArguments: string[] = []
	originalCommand: string | null = null
	nextpipeid = 0

	pipe(pipeid: number, append: string[], raw: string) {
		this.piping = true
		this.pipeid = pipeid
		this.nextpipeid = pipeid + 1
		const args = this.getPipeArguments(pipeid)

		if (!args) {
			throw new Error('No pipe found with ID ' + pipeid)
		}

		const rawPrev = args.join(' ')
		this.pipeCommand = this.getPipe(pipeid)
		args.push(...append)
		this.rawPipe = (rawPrev + ' ' + raw).trim()
		this.pipeArguments = args
		this.parseFull()

		return this
	}

	getCommand() {
		if (this.piping) {
			return this.pipeCommand
		}

		return this.originalCommand
	}

	parseArgs(strIn: string) {
		if (this.parsed) {
			return this
		}

		this.parsed = true
		const parsedData = ParseString(this.raw)
		this.args = parsedData[0]
		parsedData.splice(0, 1)
		this.originalCommand = this.args[0] && this.args[0].toLowerCase() || null

		if (this.allow_piping) {
			let hit = false

			for (const layer of parsedData) {
				if (layer[0]) {
					const command = layer[0]
					const getcommand = this.bot.command_holder.getCommand(command.toLowerCase())

					if (getcommand && !getcommand.allow_piping) {
						hit = true
						break
					}
				}
			}

			if (!hit) {
				this.argsPipes = parsedData
			} else {
				for (const obj of parsedData) {
					for (const obj2 of obj) {
						this.args.push(obj2)
					}
				}
			}
		} else {
			for (const obj of parsedData) {
				for (const obj2 of obj) {
					this.args.push(obj2)
				}
			}
		}

		this.rebuildRaw()

		return this
	}

	static patternSafe = /(\+|\|\\|\-|\]|\(|\)|\[])/g

	rebuildRaw() {
		if (this.args[0] && this.allow_piping) {
			//this._rawArgs = this.raw.substr(this.args[0].length + 1)
			let match

			try {
				if (this.args[1]) {
					match = this.raw.match(new RegExp('^' + this.args[0] + '([^|]+)(' + this.args[this.args.length - 1].replace(CommandContext.patternSafe, '\\$1') + ')("?\'?)'))
				} else {
					match = this.raw.match(new RegExp('^' + this.args[0] + '([^|]+)'))
				}

				if (match) {
					this._rawArgs = (match[1] + (match[2] || '') + (match[3] || '')).trim()
				} else {
					this._rawArgs = this.args.join(' ')
				}
			} catch(err) {
				// console.error(err)
				this._rawArgs = this.raw.substr(this.args[0].length + 1)
			}
		} else if (this.args[0]) {
			this._rawArgs = this.raw.substr(this.args[0].length + 1)
		}
	}

	rawArgsFrom(pos: number) {
		if (pos == 0) {
			return this.raw
		}

		if (pos == 1) {
			return this.rawArgs
		}

		if (pos >= this.currentArgs.length) {
			return ''
		}

		let len = 0

		for (let i = 1; i < pos; i++) {
			if (len == 0) {
				len = this.currentArgs[i].length
			} else {
				len += this.currentArgs[i].length + 1
			}
		}

		return this.rawArgs.substr(len + 1)
	}

	parseFull() {
		this.parsedArgs = []

		for (const i in this.currentArgs) {
			const arg = this.currentArgs[i]

			if (typeof arg != 'string') {
				break
			}

			if (this.parse_user_argument) {
				const user = arg.match(parseUser)

				if (user) {
					this.parsedArgs[i] = this.bot.client.users.cache.get(user[1]) || arg
					continue
				} else {
					switch (arg) {
						case '@me':
						case '@myself':
						case '@self':
						case '@user':
						case '%self%':
						case '%me%':
						case '%myself%':
						case '%user%':
							this.parsedArgs[i] = this.author
							break
						case '@bot':
						case '@notdbot':
						case '%bot%':
						case '%notdbot%':
							this.parsedArgs[i] = this.bot.client.user
							break
						case '@owner':
						case '@serverowner':
						case '@server_owner':
						case '%owner%':
						case '%serverowner%':
						case '%server_owner%':
							if (this.server) {
								this.parsedArgs[i] = this.server.owner?.user
							}
							break
					}

					if (this.parsedArgs[i]) {
						continue
					}
				}
			}

			if (this.parse_member_argument && this.server) {
				const user = arg.match(parseUser)

				if (user) {
					this.parsedArgs[i] = this.server.member(user[1]) || arg
					continue
				} else {
					switch (arg) {
						case '@me':
						case '@myself':
						case '@self':
						case '@user':
						case '%self%':
						case '%me%':
						case '%myself%':
						case '%user%':
							this.parsedArgs[i] = this.member
							break
						case '@bot':
						case '@notdbot':
						case '%bot%':
						case '%notdbot%':
							this.parsedArgs[i] = this.server.member(this.bot.client.user!)
							break
						case '@owner':
						case '@serverowner':
						case '@server_owner':
						case '%owner%':
						case '%serverowner%':
						case '%server_owner%':
							if (this.server) {
								this.parsedArgs[i] = this.server.owner
							}
							break
					}

					if (this.parsedArgs[i]) {
						continue
					}
				}
			}

			if (this.parse_channel_argument) {
				const channel = arg.match(parseChannel)

				if (channel) {
					this.parsedArgs[i] = this.bot.client.channels.cache.get(channel[1]) || arg
					continue
				}
			}

			if (this.parse_role_argument && this.server) {
				const role = arg.match(parseRole)

				if (role) {
					this.parsedArgs[i] = this.server.roles.cache.get(role[1]) || arg
					continue
				}
			}

			this.parsedArgs[i] = arg
		}

		return this
	}

	getPipe(pipeNum: number): string | null {
		if (!this.argsPipes[pipeNum] || this.argsPipes[pipeNum].length == 0) {
			return null
		}

		return this.argsPipes[pipeNum][0]
	}

	getPipeArguments(pipeNum: number): string[] | null {
		if (!this.argsPipes[pipeNum] || this.argsPipes[pipeNum].length == 0) {
			return null
		}

		const reply = []

		for (let i = 1; i < this.argsPipes[pipeNum].length; i++) {
			reply.push(this.argsPipes[pipeNum][i])
		}

		return reply
	}

	hasPipes() {
		return this.argsPipes.length >= 1
	}

	hasArguments() {
		return this.args.length > 1
	}

	*next() {
		for (const arg of this.args) {
			yield arg
		}
	}

	concatArgs(split = ' ') {
		if (!this.parsed) {
			return ''
		}

		return this.args.join(split)
	}

	parse() {
		return this.parseArgs(this.raw)
	}
}

export {CommandContext, CommandFlags}
