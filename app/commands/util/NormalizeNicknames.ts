
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import Discord = require('discord.js')
import unorm = require('unorm')
import { CommandHelper } from '../../lib/CommandHelper';
import { Aesthetics, TextFlip, TextFlop } from '../fun/text/TextManip';

function normalizeName(input: string) {
	return TextFlop.unmap(TextFlip.unmap(Aesthetics.unmap(CommandHelper.fuckOffZalgoText(unorm.nfkd(input)))))
}

class CheckNicknames extends CommandBase {
	can_be_banned = false
	allow_in_dm = false
	help = 'Prints list of users which got non normalized unicode nicknames/usernames'

	constructor() {
		super('nicknormalizecheck')
	}

	executed(instance: CommandExecutionInstance) {
		const reply: [Discord.GuildMember, string, string][] = []
		let maxWidth = 10

		for (const member of instance.server!.members.cache.values()) {
			const username = member.nickname != undefined && member.nickname != null ? member.nickname : member.user.username
			const normalized = normalizeName(username)

			if (username != normalized) {
				reply.push([member, username, normalized])
				maxWidth = Math.max(maxWidth, username.length)
			}
		}

		if (reply.length == 0) {
			return 'There are no members with non normalized names.'
		}

		const lines = []

		for (const [member, username, normalized] of reply) {
			lines.push('<@' + member.id + '> ' + username + ' '.repeat(maxWidth - username.length + 2) + '-> ' + normalized)
		}

		return '```\n' + lines.join('\n') + '\n```'
	}
}

export {CheckNicknames}
