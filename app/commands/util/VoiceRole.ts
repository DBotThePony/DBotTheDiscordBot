

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import Discord = require('discord.js')

class VoiceRoleCommand extends CommandBase {
	command_help = 'Manage role(s) bound to voice channel(s). Available actions are add, remove, list'
	help_arguments = '<action> <channel> <role>'
	parse_channel_argument = true
	allow_in_dm = false
	can_be_banned = false

	constructor() {
		super('voicerole', 'voiceroles')
	}

	async add(instance: CommandExecutionInstance, channel: Discord.VoiceChannel) {
		if (!instance.hasPermission('MANAGE_ROLES')) {
			instance.reply('Reminder that i got no `MANAGE_ROLES` permission, so i won\'t do anything.')
		}

		const role = instance.findRole(instance.from(3).join(' '))

		if (!role) {
			instance.error('Unable to find specified role', 3)
			return
		}

		try {
			await this.bot.voice_role_manager.voice.add(role.id, channel.id)
		} catch(err) {
			instance.reply('Unable to add role:\n```js\n' + err + '\n```')
			return
		}

		instance.reply('Role added successfully')
	}

	async remove(instance: CommandExecutionInstance, channel: Discord.VoiceChannel) {
		const role = instance.findRole(instance.from(3).join(' '))

		if (!role) {
			instance.error('Unable to find specified role', 3)
			return
		}

		try {
			await this.bot.voice_role_manager.voice.remove(role.id, channel.id)
		} catch(err) {
			instance.reply('Unable to remove role:\n```js\n' + err + '\n```')
			return
		}

		instance.reply('Role removed successfully')
	}

	async list(instance: CommandExecutionInstance, channel: Discord.VoiceChannel) {
		const roles = await this.bot.voice_role_manager.voice.asyncGet(channel.id)

		if (roles.length == 0) {
			instance.reply('No roles are registered for that channel')
			return
		}

		const rolenames = []
		let role

		for (const roleid of roles) {
			if (role = instance.server!.roles.cache.get(roleid)) {
				rolenames.push(role.name)
			}
		}

		if (rolenames.length == 0) {
			instance.reply('None of registered roles are valid')
			return
		}

		instance.reply('```\n' + rolenames.join('\n') + '\n```')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_ROLES')) {
			instance.error('You do not have `MANAGE_ROLES` permission!', 1)
			return
		}

		if (!instance.assert(1, 'missing command. Valid are add, remove, list')) {
			return
		}

		if (!instance.assert(2, 'missing channel')) {
			return
		}

		const channel = instance.findChannel(instance.get(2), 'voice')

		if (!(channel instanceof Discord.VoiceChannel)) {
			instance.error('Channel does not exist or not a voice channel', 2)
			return
		}

		switch ((<string> instance.getString(1)).toLowerCase()) {
			case 'add':
				return this.add(instance, channel)
			case 'remove':
				return this.remove(instance, channel)
			case 'list':
				return this.list(instance, channel)
			default:
				instance.error('Invalid action specified. Valid are add, remove, list', 1)
		}
	}
}

export {VoiceRoleCommand}
