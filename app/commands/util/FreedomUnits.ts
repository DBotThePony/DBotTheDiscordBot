
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import Discord = require('discord.js')

class FreedomUnitsCommand extends CommandBase {
	allow_in_dm = false
	can_be_banned = false

	command_help = 'Opt-out/in of freedom units auto conversion'
	help_arguments = '<channel/server>'

	constructor() {
		super('freedomunits', 'nofreedomunits', 'metricbot', 'metric_bot', 'autometric', 'autometricconversion', 'bot_metric', 'botmetric')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_GUILD')) {
			instance.error('You must have MANAGE_GUILD permission!', 1)
			return
		}

		if (!instance.assert(1)) {
			return
		}

		switch (<string> instance.get(1).toLowerCase()) {
			case 'channel':
				if (this.bot.no_freedom_units.isOptedInChannel(instance.channel!.id)) {
					await this.sql.query(`DELETE FROM "metric_optin_channel" WHERE "channel" = '${instance.channel!.id}'`)
					instance.reply('This channel is now opted out the conversion')
				} else {
					await this.sql.query(`INSERT INTO "metric_optin_channel" VALUES ('${instance.channel!.id}')`)
					instance.reply('This channel is now opted in the conversion')
				}

				break
			case 'server':
				if (this.bot.no_freedom_units.isOptedIn(instance.server!.id)) {
					await this.sql.query(`DELETE FROM "metric_optin" WHERE "server" = '${instance.server!.id}'`)
					instance.reply('Server is now opted out the conversion')
				} else {
					await this.sql.query(`INSERT INTO "metric_optin" VALUES ('${instance.server!.id}')`)
					instance.reply('Server is now opted in the conversion')
				}

				break
			default:
				instance.error('Invalid subcommand specified. Valid subcommands are server, client', 1)
		}
	}
}

export {FreedomUnitsCommand}
