

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import Discord = require('discord.js')

class SearchForAllPinsCommand extends CommandBase {
	allow_piping = false
	can_be_banned = false
	allow_in_dm = false
	parse_channel_argument = true
	command_help = 'Searches for all pins ever existed. This is very intense command.'

	static working = false

	constructor() {
		super('searchforpins')
	}

	async searchIn(channelID: string, set: Discord.Message[]): Promise<[number, Discord.Message, Discord.Message]> {
		let min: Discord.Message, max: Discord.Message
		let added = 0

		for (const message of set) {
			if (message.system && message.type == 'PINS_ADD') {
				await this.sql.query(`INSERT INTO "pin_search_result" ("channel", "message", "type") VALUES (${channelID}, ${message.id}, true)`)
				added++
			} else if (message.pinned) {
				await this.sql.query(`INSERT INTO "pin_search_result" ("channel", "message", "type") VALUES (${channelID}, ${message.id}, false)`)
				added++
			}

			if (!min! || min!.createdTimestamp > message.createdTimestamp) {
				min = message
			}

			if (!max! || max!.createdTimestamp < message.createdTimestamp) {
				max = message
			}
		}

		if (min! && max!) {
			await this.sql.query(`UPDATE "pin_search_status" SET "oldest" = MIN("oldest", ${min!.id}), "newest" = MAX("newest", ${max!.id}), "stamp" = now()`)
		} else if (min!) {
			await this.sql.query(`UPDATE "pin_search_status" SET "oldest" = MIN("oldest", ${min!.id}), "stamp" = now()`)
		} else if (max!) {
			await this.sql.query(`UPDATE "pin_search_status" SET "newest" = MAX("newest", ${max!.id}), "stamp" = now()`)
		}

		return [added, min!, max!]
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.error('You must have `MANAGE_MESSAGES` permission!', 1)
			return
		}

		if (SearchForAllPinsCommand.working) {
			instance.reply('Already searching somewhere')
			return
		}

		let channel: Discord.TextChannel

		if (instance.get(1)) {
			if (!(instance.get(1) instanceof Discord.TextChannel)) {
				instance.error('Invalid channel specified', 1)
				return
			}

			channel = instance.get(1)
		} else {
			channel = <Discord.TextChannel> instance.channel!
		}

		const result = await instance.query(`SELECT "oldest", "newest" FROM "pin_search_status" WHERE "channel" = ${channel.id}`)
		let oldest: string, newest: string, poldest: number, pnewest: number

		if (result.rowCount == 0) {
			await instance.query(`INSERT INTO "pin_search_status" ("channel") VALUES (${channel.id})`)
		} else {
			oldest = result.rows[0].oldest
			newest = result.rows[0].newest
			poldest = parseInt(oldest)
			pnewest = parseInt(newest)
		}

		if (!oldest!) {
			// Search in currently known messages, update oldest newest, then go and search regulary

			if (channel.messages.cache.size == 0) {
				await channel.messages.fetch()
			}

			if (channel.messages.cache.size == 0) {
				instance.error('Channel is empty (?!)', 1)
				return
			}

			const [added, min, max] = await this.searchIn(channel.id, channel.messages.cache.array())
			oldest = min.id
			newest = max.id
			poldest = parseInt(oldest)
			pnewest = parseInt(newest)
		}

		const msg = <Discord.Message> await instance.send('Expanding down...')
		channel.startTyping()

		let sweepIn = 10
		let updateIn = 5
		let scanned = 0
		let direction = true

		// search U/D, expand first down, then expand up (?)
		while (true) {
			const params: Discord.ChannelLogsQueryOptions = {}

			if (direction) {
				params.before = oldest!
			} else {
				params.after = newest!
			}

			const fetched = await channel.messages.fetch(params)

			sweepIn--
			updateIn--
			scanned += fetched.size

			if (fetched.size == 0) {
				if (sweepIn <= 0) {
					this.bot.client.sweepMessages(60)
					sweepIn = 10
				}

				direction = !direction

				if (direction) {
					break
				}

				continue
			}

			if (updateIn <= 0) {
				updateIn = 5
				msg.edit(`Expanding ${direction ? 'down' : 'up'}... Seeked ${scanned} messages`)
			}

			const [added, min, max] = await this.searchIn(channel.id, fetched.array())

			if (direction) {
				const pmin = parseInt(min.id)

				if (poldest! > pmin) {
					oldest = min.id
					poldest = pmin
				}
			} else {
				const pmax = parseInt(max.id)

				if (pnewest! < pmax) {
					newest = max.id
					pnewest = pmax
				}
			}

			if (fetched.size < 50) {
				if (sweepIn <= 0) {
					this.bot.client.sweepMessages(60)
					sweepIn = 10
				}

				direction = !direction

				if (direction) {
					break
				}

				continue
			}
		}

		instance.send(`Finished. Seeked ${scanned} messages.`)

		const build = ['Messages which are currently (or was on past scans) pinned']
		const res1 = await this.sql.query(`SELECT "message" FROM "pin_search_result" WHERE "channel" = ${channel.id} AND NOT "type"`)

		for (const row of res1.rows) {
			build.push(`https://discordapp.com/channels/${instance.server!.id}/${channel.id}/${row.message}`)
		}

		build.push('\nMessages which are notification about pin')

		const res2 = await this.sql.query(`SELECT "message" FROM "pin_search_result" WHERE "channel" = ${channel.id} AND "type"`)

		for (const row of res2.rows) {
			build.push(`https://discordapp.com/channels/${instance.server!.id}/${channel.id}/${row.message}`)
		}

		channel.stopTyping()
		instance.send(build.join('\n'))
	}
}

export {SearchForAllPinsCommand}
