

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../CommandBase'
import Discord = require('discord.js')

class PurgeCommand extends CommandBase {
	allow_piping = false
	can_be_banned = false
	allow_in_dm = false
	help_arguments = '<amount>'
	command_help = 'Purges X last messages in current channel'

	constructor() {
		super('purge')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.error('You must have `MANAGE_MESSAGES` permission!', 1)
			return
		}

		if (!instance.hasPermission('MANAGE_MESSAGES')) {
			instance.error('Bot can not remove messages in current channel!', 1)
			return
		}

		if (!instance.assert(1, 'missing message amount')) {
			return
		}

		const getNum = Number(instance.get(1))

		if (getNum != getNum) {
			instance.error('invalid number supplied', 1)
			return
		}

		if (getNum >= 100) {
			instance.error('Can not remove that much at once!', 1)
			return
		}

		if (getNum < 3) {
			instance.error('At least three messages is required to purge', 1)
			return
		}

		if (instance.channel!.messages.cache.size < getNum) {
			await instance.channel!.messages.fetch({limit: getNum - instance.channel!.messages.cache.size + 2})
		}

		// this is slow (on large servers)
		// this needs different code
		const arr = instance.channel!.messages.cache.array()

		arr.sort((a, b) => {
			if (a.createdTimestamp < b.createdTimestamp) {
				return 1
			}

			if (a.createdTimestamp > b.createdTimestamp) {
				return -1
			}

			return 0
		})

		for (let i = 0; i < Math.min(getNum + 1, arr.length); i++) {
			await arr[i].delete({reason: 'purge command'})
		}

		const msg = await instance.reply('Removed `' + Math.min(getNum + 1, arr.length) + '` messages')

		if (msg) {
			(<Discord.Message> msg).delete({timeout: 1000, reason: 'Purge command'})
		}
	}
}

export {PurgeCommand}

class PurgeCommand2 extends CommandBase {
	allow_piping = false
	can_be_banned = false
	allow_in_dm = false
	help_arguments = '<amount>'
	command_help = 'Purges X last messages in current channel using bulkDelete'

	constructor() {
		super('purge2')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.error('You must have `MANAGE_MESSAGES` permission!', 1)
			return
		}

		if (!instance.hasPermission('MANAGE_MESSAGES')) {
			instance.error('Bot can not remove messages in current channel!', 1)
			return
		}

		if (!instance.assert(1, 'missing message amount')) {
			return
		}

		let getNum = Number(instance.get(1))

		if (getNum != getNum) {
			instance.error('invalid number supplied', 1)
			return
		}

		if (getNum >= 4000) {
			instance.error('Can not remove that much at once!', 1)
			return
		}

		if (getNum < 3) {
			instance.error('At least three messages is required to purge', 1)
			return
		}

		let numRemoved = 0

		while (getNum > 0) {
			try {
				numRemoved += (await (<Discord.TextChannel> instance.channel!).bulkDelete(Math.min(100, getNum))).size
				getNum -= 100
			} catch(err) {
				const msg = await instance.send('Removed `' + numRemoved + '` messages')

				if (msg) {
					(<Discord.Message> msg).delete({timeout: 1000, reason: 'Purge2 command'})
				}

				break
			}
		}

		const msg = await instance.send('Removed `' + numRemoved + '` messages')

		if (msg) {
			(<Discord.Message> msg).delete({timeout: 1000, reason: 'Purge2 command'})
		}
	}
}

export {PurgeCommand2}
