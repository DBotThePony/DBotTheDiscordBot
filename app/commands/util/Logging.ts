
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import { ILoggingManerOptions } from '../../modules/Logging';

class LogProperty {
	constructor(public propName: string) {

	}

	is(prop: string) {
		return this.propName == prop
	}

	switch(instance: CommandExecutionInstance, options: ILoggingManerOptions): string {
		throw new Error('Not implemented')
	}
}

class LogPropertyChannel extends LogProperty {
	switch(instance: CommandExecutionInstance, options: any) {
		if (instance.channel!.id == options[this.propName]) {
			delete options[this.propName]
			return `Property \`${this.propName}\` has been unset.`
		}

		options[this.propName] = instance.channel!.id
		return `Property \`${this.propName}\` has been set to current channel.`
	}
}

class LogPropertyBoolean extends LogProperty {
	switch(instance: CommandExecutionInstance, options: any) {
		if (options[this.propName] == undefined || options[this.propName] == false) {
			options[this.propName] = true
			return `Property \`${this.propName}\` has been switched on.`
		}

		options[this.propName] = false
		return `Property \`${this.propName}\` has been switched off.`
	}
}

class LoggingCommand extends CommandBase {
	allow_in_dm = false
	can_be_banned = false

	properties: LogProperty[] = []

	constructor() {
		super('log')
		this.addProp(new LogPropertyBoolean('log_join'))
		this.addProp(new LogPropertyBoolean('log_leave'))
		this.addProp(new LogPropertyBoolean('log_edits'))
		this.addProp(new LogPropertyBoolean('log_deletions'))
		this.addProp(new LogPropertyBoolean('log_nicknames'))
		this.addProp(new LogPropertyChannel('channel_log'))
		this.addProp(new LogPropertyChannel('channel_join'))
		this.rebuildHelp()
	}

	protected addProp(...prop: LogProperty[]) {
		this.properties.push(...prop)
		return this
	}

	protected rebuildHelp() {
		const properties = []
		for (const prop of this.properties) {properties.push(prop.propName)}
		this.command_help = 'Allows you to setup logging. Available options are: ' + properties.join(', ')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_GUILD')) {
			instance.error('You must have MANAGE_GUILD permission!', 1)
			return
		}

		if (!instance.assert(1)) {
			return
		}

		const option = <string> instance.get(1).toLowerCase()

		for (const prop of this.properties) {
			if (prop.is(option)) {
				let options = await this.bot.logging_manager.servers.get(instance.server!.id)
				options = options != null ? options : {}
				const status = prop.switch(instance, options)
				const status2 = await this.bot.logging_manager.servers.set(options, JSON.stringify(options), instance.server!.id)

				instance.reply('Execition status:\n' + status + '\n' + status2)

				return
			}
		}

		await instance.error('Unknown option. Use }help log to see list of available options.', 1)
	}
}

export {LoggingCommand}
