

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import crypto = require('crypto')

class CoinFlipCommand extends CommandBase {
	allow_piping = false
	command_help = 'Flips virtual coin (crypto steady)'

	constructor() {
		super('coinflip')
	}

	async executed(instance: CommandExecutionInstance) {
		const buff = crypto.randomBytes(1)

		if (buff[0] < 125) {
			instance.reply(`Coin landed at heads!`)
		} else if (buff[0] > 130) {
			instance.reply(`Coin landed at tails!`)
		} else {
			instance.reply(`**Coin landed at side!**`)
		}
	}
}

export {CoinFlipCommand}

class ChooseCommand extends CommandBase {
	allow_piping = false
	command_help = 'Chooses one of arguments (crypto steady)'

	constructor() {
		super('choose')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1)) {
			return
		}

		if (!instance.assert(2)) {
			return
		}

		const buff = crypto.randomBytes(1)
		const num = Math.floor(buff[0] / 0x100 * (instance.parsedArguments.length - 1))
		instance.reply(`My choice is ${instance.get(num + 1)} (probability: ${Math.floor(buff[0] / 0x100 * 1000) / 1000})`)
	}
}

export {ChooseCommand}
