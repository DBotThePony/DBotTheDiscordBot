

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import dns = require('dns')
import dgram = require('dgram')

import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

const sign = Buffer.from([
	0xFF, 0xFF, 0xFF, 0xFF, 0x54,
	0x53, 0x6F, 0x75, 0x72, 0x63,
	0x65, 0x20, 0x45, 0x6E, 0x67,
	0x69, 0x6E, 0x65, 0x20, 0x51,
	0x75, 0x65, 0x72, 0x79, 0x00
])

const CurTime = function() {
	return (new Date()).getTime() / 1000
}

const ReadString = function(buf: Buffer, offset: number) {
	let output = ''
	let len = 0

	while (true) {
		const readChar = buf.readUInt8(offset + len)
		len++

		if (readChar == 0) {
			break
		}

		output += String.fromCharCode(readChar)
	}

	return [output, len]
}

class SourceServerPing extends CommandBase {
	command_help = 'Pings source server'
	help_arguments = '<ip/domain name>[:port]'

	constructor(command?: string) {
		super(command == undefined ? 'sping' : command)
	}

	async determineIP(ip: string, instance: CommandExecutionInstance) {
		const matchIP = ip.match(/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/)

		if (matchIP) {
			const
				A = Number(matchIP[1]),
				B = Number(matchIP[2]),
				C = Number(matchIP[3]),
				D = Number(matchIP[4])

			const invalid =
				A != A ||
				B != B ||
				C != C ||
				D != D ||
				A < 0 || A > 255 ||
				B < 0 || B > 255 ||
				C < 0 || C > 255 ||
				D < 0 || D > 255

			if (invalid) {
				instance.error('Invalid IP specified', 1)
				return
			}

			return matchIP[0]
		}

		const getip = await new Promise((resolve, reject) => {
			dns.lookup(ip, {family: 4, hints: dns.ADDRCONFIG | dns.V4MAPPED, all: false}, (err, address) => {
				if (err) {
					instance.error('Invalid DNS name specified: ' + err, 1)
					resolve(false)
					return
				}

				resolve(address)
			})
		})

		return getip
	}

	determinePort(possiblePort: string, instance: CommandExecutionInstance) {
		let port = 27015

		if (possiblePort) {
			const portNum = Number(possiblePort)

			if (portNum == portNum) {
				if (port < 43 || port >= 65565) {
					instance.error('Invalid port specified', 1)
					return
				}

				port = portNum
			} else {
				instance.error('Invalid port specified', 1)
				return
			}
		}

		return port
	}

	async executed(instance: CommandExecutionInstance) {
		const matchIP6 = instance.raw.match(/^\[?[a-f0-9]+:+[a-f0-9:]+[a-f0-9]\]?/i)

		if (matchIP6) {
			instance.error('Source engine based servers don\'t support IPv6 address, so it can\'t be pinged, sorry!', 1)
			return
		}

		let port: number | undefined = 27015

		const split = instance.raw.split(':')

		if (!split[0]) {
			instance.error('No valid IP/Domain name found!', 1)
			return
		}

		const ip = <string> await this.determineIP(split[0], instance)

		if (!ip) {
			return
		}

		if (split[1]) {
			port = this.determinePort(split[1], instance)

			if (!port) {
				return
			}
		}

		this.ping(ip, instance, port)
	}

	ping(ip: string, instance: CommandExecutionInstance, port: number) {
		const randPort = Math.floor(Math.random() * 5000) + 50000
		let Closed = false
		let sendStamp = CurTime()
		const socket = dgram.createSocket('udp4')

		socket.on('message', (buf, rinfo) => {
			try {
				let pingLatency = Math.floor((CurTime() - sendStamp) * 1000)
				let offset = 6
				const readName = ReadString(buf, offset)
				let name = readName[0]
				offset += <number> readName[1]

				const readMap = ReadString(buf, offset)
				const map = readMap[0]
				offset += <number> readMap[1]

				const readFolder = ReadString(buf, offset)
				const folder = readFolder[0]
				offset += <number> readFolder[1]

				const readGame = ReadString(buf, offset)
				const game = readGame[0]
				offset += <number> readGame[1]

				const readID = buf.readUInt16LE(offset)
				offset += 2

				const Players = buf.readUInt8(offset)
				offset += 1

				const MPlayers = buf.readUInt8(offset)
				offset += 1

				const Bots = buf.readUInt8(offset)
				offset += 1

				const Type = String.fromCharCode(buf.readUInt8(offset))
				offset += 1

				const OS = String.fromCharCode(buf.readUInt8(offset))
				offset += 1

				const Visibility = buf.readUInt8(offset)
				offset += 1

				const VAC = buf.readUInt8(offset)
				offset += 1

				const readVersion = ReadString(buf, offset)
				const Version = readVersion[0]
				offset += <number> readVersion[1]

				let output = '\n```'

				output += 'Ping to the server:      ' + Math.floor(pingLatency) + 'ms\n'
				output += 'Server IP:               ' + ip + '\n'
				output += 'Server Port:             ' + port + '\n'
				output += 'Server Name:             ' + name + '\n'
				output += 'Server current map:      ' + map + '\n'
				output += 'Server game folder:      ' + folder + '\n'
				output += 'Server game:             ' + game + '\n'
				output += 'Server game ID:          ' + readID + '\n'
				output += 'Server Current players:  ' + Players + '\n'
				output += 'Server Max players:      ' + MPlayers + '\n'
				output += 'Server Load:             ' + Players + '/' + MPlayers + ' (' + Math.floor(Players / MPlayers * 100) + '%)' + '\n'
				output += 'Server Bots:             ' + Bots + '\n'
				output += 'Server Type:             ' + (Type == 'd' && 'Dedicated' || Type == 'l' && 'Listen' || Type == 'p' && 'SourceTV' || 'WTF?') + '\n'
				output += 'Server OS:               ' + (OS == 'l' && 'Linux' || OS == 'w' && 'Windows' || OS == 'm' && 'Apple OS/X' || OS == 'o' && 'Apple OS/X' || 'WTF?') + '\n'
				output += 'Server is running VAC:   ' + (VAC == 1 && 'Yes' || 'Nope') + '\n'

				output += '\n```'

				instance.reply(output)
			} catch(err) {
				console.log(err)
				instance.reply('Internal error: ```js\n' + err.stack + '\n```\nMessage received from server was (binary):\n```\n' + buf.toString('hex') + '\n```')
			}

			socket.close()
		})

		socket.on('listening', () => {
			socket.send(sign, 0, sign.length, port, ip)
			sendStamp = CurTime()
		})

		socket.on('error', (err) => {
			console.error(err)
			instance.reply('OSHI ```\n' + err.stack + '\n```')
		})

		socket.on('close', () => {
			Closed = true
		})

		setTimeout(() => {
			if (Closed) {
				return
			}

			instance.reply('Failed to ping: Connection timeout!')
			socket.close()
		}, 4000)

		socket.bind(randPort, '0.0.0.0')
	}
}

const pingable = [4242, 27215]

for (let i = 27015; i < 27100; i++) {
	pingable.push(i)
}

for (let i = 26900; i < 26990; i++) {
	pingable.push(i)
}

class SourceServerHackPing extends SourceServerPing {
	command_help = 'Pings source server ports. Bot owner only'
	help_arguments = '<ip/domain name>[:port]'

	constructor() {
		super('hackping')
	}

	executed(instance: CommandExecutionInstance): any {
		if (!instance.isOwner) {
			instance.reply('Not a bot owner.')
			return
		}

		return super.executed(instance)
	}

	ping(ip: string, instance: CommandExecutionInstance, port: number) {
		const randPort = Math.floor(Math.random() * 5000) + 50000
		const socket = dgram.createSocket('udp4')
		const received: number[] = []

		socket.on('message', (buf, rinfo) => {
			received.push(rinfo.port)
		})

		socket.on('listening', () => {
			for (const port of pingable) {
				socket.send(sign, 0, sign.length, port, ip)
			}
		})

		socket.on('error', (err) => {
			console.error(err)
			instance.reply('OSHI ```\n' + err.stack + '\n```')
		})

		setTimeout(() => {
			if (received.length == 0) {
				instance.reply('Received no replies.')
			} else {
				instance.reply('Received replies on next ports: ```\n' + received.sort().join(', ') + '\n```')
			}

			socket.close()
		}, 2000)

		socket.bind(randPort, '0.0.0.0')
	}
}

export {SourceServerPing, SourceServerHackPing}
