
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import { SypexDB } from '../../../lib/SypexDB';
import _fs = require('fs')
const fs = _fs.promises

import maxmind = require('maxmind')
import { SypexIPAddress } from '../../../lib/IPAddress';

let loaded = false

class IPLookupCommand extends CommandBase {
	allow_piping = false
	help_arguments = '<ip address>'
	command_help = 'Look ip IP address in both SupexGeo and MaxMind GeoCityLite2 databases'

	static sypex: SypexDB | null = null
	static maxmind: maxmind.Reader<maxmind.CityResponse> | null = null

	constructor() {
		super('ip', 'iplookup', 'geolite', 'geolitecity', 'supex', 'supexgeo')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1)) {
			return
		}

		if (!loaded) {
			await loadDatabases()
		}

		if (!IPLookupCommand.sypex && !IPLookupCommand.maxmind) {
			instance.reply('No databases are loaded to serve your lookup')
			return
		}

		const ip = <string> instance.get(1)
		const ipv6 = ip.match(/^\[?[a-f0-9]+:+[a-f0-9:]+[a-f0-9]\]?$/i)
		const ipv4_match = ip.match(/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/)

		if (!ipv4_match && !ipv6) {
			instance.error('This does not look like a valid IP address.', 1)
			return
		}

		const lines = ['Reply: ```']

		if (IPLookupCommand.sypex && ipv4_match) {
			const region = await IPLookupCommand.sypex.readIP(new SypexIPAddress(ip))

			if (region) {
				const country = region.entryCountry!.get('name_en')!.string
				const city = region.entryCity && region.entryCity.get('name_en')!.string
				const iso_country_code = region.entryCountry!.get('iso')!.string

				lines.push('Sypex : Country : ' + (country ? country : 'Unknown'))
				lines.push('Sypex : City : ' + (city ? city : 'Unknown'))
				lines.push('Sypex : ISO Code : ' + (iso_country_code ? iso_country_code : 'Unknown'))
			} else {
				lines.push('Sypex : Unknown')
			}
		} else if (IPLookupCommand.sypex && !ipv4_match) {
			lines.push('Sypex : IPv6 is not supported')
		}

		if (IPLookupCommand.maxmind) {
			const response = await IPLookupCommand.maxmind.get(ip)

			if (response) {
				lines.push('MaxMind GeoCityLite2 : City : ' + (response.city ? response.city.names.en : 'Unknown'))
				lines.push('MaxMind GeoCityLite2 : Country : ' + (response.country ? response.country.names.en : 'Unknown'))
				lines.push('MaxMind GeoCityLite2 : ISO Code : ' + (response.country ? response.country.iso_code : 'Unknown'))
			} else {
				lines.push('MaxMind GeoCityLite2 : Unknown')
			}
		}

		lines.push('```')

		return lines.join('\n')
	}
}

async function loadDatabases() {
	try {
		await fs.stat('./resource/SxGeoCity.dat')
		IPLookupCommand.sypex = new SypexDB('./resource/SxGeoCity.dat')
		await IPLookupCommand.sypex.readHeader()
		console.log('Loaded Sypex DB')
	} catch(err) {
		console.error('Sypex Geo DB is not present, IP lookup using this DB will not be available')
	}

	try {
		await fs.stat('./resource/GeoLite2-City.mmdb')
		IPLookupCommand.maxmind = await maxmind.open('./resource/GeoLite2-City.mmdb')
		console.log('Loaded Maxmind DB')
	} catch(err) {
		console.error('MaxMindDB GeoCityLite2 is not present, IP lookup using this DB will not be available')
	}

	loaded = true
}

export {IPLookupCommand}
