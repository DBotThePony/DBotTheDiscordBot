

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class AddPermaNickname extends CommandBase {
	command_help = 'Sets/removes (if empty provided) user nickname permanently (permanent - means bot will re-apply this nickname if user left and joined guild again)'
	allow_in_dm = false
	parse_user_argument = true
	can_be_banned = false

	constructor() {
		super('permanick')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermission('MANAGE_NICKNAMES')) {
			instance.reply('Bot is missing MANAGE_NICKNAMES permission!')
			return
		}

		if (!instance.hasPermissionExecutor('MANAGE_NICKNAMES')) {
			instance.reply('You need to have MANAGE_NICKNAMES permission!')
			return
		}

		if (!instance.assert(1, 'missing member')) {
			return
		}

		if (!(instance.get(1) instanceof Discord.User)) {
			instance.error('Not a user!', 1)
			return
		}

		if (instance.server!.member(instance.get(1))) {
			if (!instance.checkTargeting(instance.server!.member(instance.get(1))!)) {
				instance.error('Cannot target that', 1)
				return
			}
		}

		this.bot.permanent_stuff.setNickname(instance.server!, instance.get(1), instance.from(2).join(' ')).then((result) => {
			instance.reply(`Nickname change status: ${result}`)
		}).catch((result) => {
			instance.reply(`Unable to change nickname: ${result}`)
		})
	}
}

export {AddPermaNickname}

class ManagePermaRoles extends CommandBase {
	command_help = 'Adds a permarole to user (permanent - means bot will re-apply this role if user left and joined guild again). Valid are add, remove, list'
	allow_in_dm = false
	parse_user_argument = true

	constructor() {
		super('permarole', 'permaroles')
	}

	add(instance: CommandExecutionInstance) {
		const findrole = instance.findRole(instance.from(3).join(' '))

		if (!findrole) {
			instance.error('Unable to find specified role', 3)
			return
		}

		this.bot.permanent_stuff.addRole(instance.server!, instance.getUserID(2)!, findrole).then((result) => {
			instance.reply(`Role added successfully: ${result}`)
		}).catch((result) => {
			instance.reply(`Unable to add role: ${result}`)
		})
	}

	remove(instance: CommandExecutionInstance) {
		const findrole = instance.findRole(instance.from(3).join(' '))

		if (!findrole) {
			instance.error('Unable to find specified role', 3)
			return
		}

		this.bot.permanent_stuff.removeRole(instance.server!, instance.getUserID(2)!, findrole).then((result) => {
			instance.reply(`Role removed successfully: ${result}`)
		}).catch((result) => {
			instance.reply(`Unable to reomve role: ${result}`)
		})
	}

	list(instance: CommandExecutionInstance) {
		this.bot.permanent_stuff.listRole(instance.server!, instance.getUserID(2)!).then((values) => {
			if (values.length == 0) {
				instance.reply(`None listed`)
				return
			}

			const reply = []

			for (const value of values) {
				if (instance.server!.roles.cache.has(value)) {
					reply.push(instance.server!.roles.cache.get(value)!.name)
				} else {
					reply.push(value)
				}
			}

			instance.reply('Permaroles are: ```\n' + reply.join(', ') + '\n```')
		}).catch((result) => {
			instance.reply(`Unable to list roles: ${result}`)
		})
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermission('MANAGE_ROLES')) {
			instance.reply('Bot is missing MANAGE_ROLES permission!')
			return
		}

		if (!instance.hasPermissionExecutor('MANAGE_ROLES')) {
			instance.reply('You need to have MANAGE_ROLES permission!')
			return
		}

		if (!instance.assert(1, 'missing action')) {
			return
		}

		if (!instance.assert(2, 'missing member')) {
			return
		}

		/*if (!instance.getMember(2, false) && !instance.getUser(2, false)) {
			instance.error('Unable to find user/member', 2)
			return
		}*/

		switch ((<string> instance.getString(1)).toLowerCase()) {
			case 'add':
				this.add(instance)
				return
			case 'remove':
				this.remove(instance)
				return
			case 'list':
				this.list(instance)
				return
		}

		instance.error('Invalid action. Valid are add, remove, list', 1)
	}
}

export {ManagePermaRoles}
