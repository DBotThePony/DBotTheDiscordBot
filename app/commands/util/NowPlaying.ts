

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import numeral = require('numeral')

class NowPlayingCommand extends CommandBase {
	command_help = 'http://radio.ffgs.ru'
	statusURL = 'http://radio.ffgs.ru/radio/loadStatus/'

	constructor() {
		super('np', 'nowplaying', 'radiostatus')
	}

	async executed(instance: CommandExecutionInstance) {
		const rawbody = await this.bot.get(this.statusURL, {
			'Accept': 'application/json'
		})

		let body: any

		try {
			body = JSON.parse(rawbody.toString('utf8'))
		} catch(err) {
			instance.reply('Failed to gather info: ```js\n' + err + '\n```')
			return
		}

		if (typeof body.Body != 'object') {
			instance.reply('Failed to gather info!')
			return
		}

		if (body.Body.server_running != 'yes') {
			instance.reply('Server is not broadcasting anything at this moment.')
			return
		}

		if (typeof body.Body.now_playing != 'object') {
			instance.reply('Failed to gather info!')
			return
		}

		const artist = body.Body.now_playing.Artist
		const title = body.Body.now_playing.Title
		const join = artist + ' — ' + title
		const lengthParse = body.Body.now_playing.TrackLength.match(/([0-9]+):([0-9]+)/)
		const length = Number(lengthParse[1]) * 60 + Number(lengthParse[2])
		let playPerc = -1

		if (typeof body.Body.play_progress == 'number') {
			playPerc = Math.min(1, body.Body.play_progress / 100)
		}

		const lines = ['```']

		lines.push(join)

		if (playPerc == -1) {
			lines.push('───────────────?─────────────────── ' + body.Body.now_playing.TrackLength)
		} else {
			const now = Math.ceil(length * playPerc)
			const ftime = ' ' + numeral((now - now % 60) / 60).format('00') + ':' + numeral((now % 60)).format('00')
			lines.push('─'.repeat(Math.floor(playPerc * 40)) + '⚪' + '─'.repeat(Math.floor((1 - playPerc) * 40)) + ftime + ' / ' + body.Body.now_playing.TrackLength)
		}

		if (typeof body.Body.playlist == 'object' && body.Body.playlist[0]) {
			lines.push('\nUp next:')

			for (let i = 0; i <= 3; i++) {
				if (!body.Body.playlist[i]) {
					break
				}

				const artist = body.Body.playlist[i].Artist
				const title = body.Body.playlist[i].Title

				lines.push(artist + ' — ' + title)
			}
		}

		instance.reply('Now playing at http://radio.ffgs.ru:\n' + lines.join('\n') + '```')
	}
}

export {NowPlayingCommand}
