

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class HackbanCommand extends CommandBase {
	command_help = 'Bans a member, or an arbitrary user'
	allow_in_dm = false
	// parse_user_argument = true
	can_be_banned = false

	constructor() {
		super('hackban', 'ban')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('BAN_MEMBERS')) {
			instance.error('You cannot ban members', 1)
			return
		}

		if (!instance.hasPermission('BAN_MEMBERS')) {
			instance.error('I cannot ban members', 1)
			return
		}

		const getUserID = instance.getUserID(1)

		if (!getUserID) {
			return
		}

		if (getUserID == this.bot.id) {
			instance.error('Cannot ban myself', 1)
			return
		}

		let member

		try {
			await instance.server!.members.fetch({user: getUserID})
			member = instance.server!.members.cache.get(getUserID)
		} catch(err) {

		}

		if (member && (!instance.checkTargeting(member) || !member.bannable)) {
			instance.error('Cannot target that', 1)
			return
		}

		if (member) {
			await member.ban({days: 0, reason: instance.has(2) ? instance.tag! + ': ' + instance.from(2).join(' ') : 'Hackban by ' + instance.tag!})
			instance.reply('Banned <@' + member.id + '>!')
			return
		}

		await instance.server!.members.ban(getUserID, {days: 0, reason: instance.has(2) ? instance.tag! + ': ' + instance.from(2).join(' ') : 'Hackban by ' + instance.tag!})
		instance.reply('Banned <@' + getUserID + '>!')
	}
}

export {HackbanCommand}
