

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'

import {SteamID} from '../../../lib/SteamID'
import { BotInstance } from '../../BotInstance';

class SteamIDCommand extends CommandBase {
	command_help = 'Displays steamid information'
	help_arguments = '[SteamID2/SteamID64/SteamID3/User URL]'
	apiBase!: string
	resolveBase!: string

	constructor() {
		super('steamid', 'steam', 'sid', 'steam2', 'steamid2', 'steam3', 'steamid3', 'steamid64', 'steam64')
	}

	setupBot(bot: BotInstance) {
		this.apiBase = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' + bot.config.steam + '&steamids='
		this.resolveBase = 'http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=' + bot.config.steam + '&vanityurl='
	}

	async resolveProfileID(id: string): Promise<SteamID> {
		const steamid = new SteamID(id)

		if (steamid.valid()) {
			return steamid
		}

		const result = await this.bot.sql.query(`SELECT * FROM "steamid_cache" WHERE "expires" > ${Math.floor(Date.now() / 1000)} AND LOWER("profileurl") = LOWER(${this.bot.sql.escapeLiteral('https://steamcommunity.com/id/' + id + '/')})`)

		if (result.rowCount != 0) {
			const resolved = new SteamID(result.rows[0].steamid)

			if (resolved.valid()) {
				return resolved
			}
		}

		const rawbody = (await this.bot.get(this.resolveBase + encodeURI(id), {
			'Accept': 'application/json'
		})).toString('utf8')

		if (!rawbody || rawbody == '') {
			throw new Error('Invalid reply from ISteamUser/ResolveVanityURL API')
		}

		const json = JSON.parse(rawbody)

		// if (!json.response || !json.response.success) {
		//  reject('ISteamUser/ResolveVanityURL: Invalid Vanity ID')
		//  return
		// }

		if (typeof json != 'object' || !json.response || json.response.success != 1) {
			throw new Error('ISteamUser/ResolveVanityURL: Invalid Vanity ID (status code ' + json.response.success + ', message: ' + (json.response.message || '<empty>') + ')')
		}

		const resolved = new SteamID(json.response.steamid)

		if (resolved.valid()) {
			return resolved
		}

		throw new Error('Invalid SteamID received from ISteamUser/ResolveVanityURL API')
	}

	static fields = [
		"steamid",
		"communityvisibilitystate",
		"profilestate",
		"personaname",
		"lastlogoff",
		"profileurl",
		"avatar",
		"avatarmedium",
		"avatarfull",
		"personastate",
		"realname",
		"primaryclanid",
		"timecreated",
		"personastateflags",
		"loccountrycode",
		"locstatecode",
		"loccityid",
	]

	async resolveSteamID(steamid: SteamID) {
		const values = await this.bot.sql.query(`SELECT * FROM "steamid_cache" WHERE "steamid" = ${steamid.steamid64!} AND "expires" > ${Math.floor(Date.now() / 1000)}`)

		if (values.rowCount != 0) {
			return values.rows[0]
		}

		await this.bot.sql.query(`DELETE FROM "steamid_cache" WHERE "steamid" = ${steamid.steamid64!}`)

		const rawresult = (await this.bot.get(this.apiBase + encodeURI(steamid.steamid64!), {
			'Accept': 'application/json'
		})).toString('utf8')

		if (!rawresult || rawresult == '') {
			throw new Error('Invalid reply from ISteamUser/GetPlayerSummaries API')
		}

		const json = JSON.parse(rawresult)

		// if (!json.response || !json.response.players || !json.response.players[0]) {
		//  reject('ISteamUser/GetPlayerSummaries: Account with specified SteamID does not exist')
		//  return
		// }

		if (typeof json != 'object' || !json.response || !json.response.players || !json.response.players[0]) {
			throw new Error('ISteamUser/GetPlayerSummaries: Account with specified SteamID does not exist')
		}

		const player = json.response.players[0]
		const rowmap: string[] = []

		for (const field of SteamIDCommand.fields) {
			rowmap.push(player[field] && player[field] != '' && this.bot.sql.escapeLiteral(String(player[field])) || 'DEFAULT')
		}

		rowmap.push(String((Date.now() / 1000) + 3600))
		this.bot.sql.query(`INSERT INTO "steamid_cache" VALUES (${rowmap.join(',')})`)
		return player
	}

	static profileState = [
		'Offline',
		'Online',
		'Busy',
		'Away',
		'Snooze',
		'looking to trade',
		'looking to play'
	]

	executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1, 'Missing steamid')) {
			return
		}

		const steamidString = (<string> instance.get(1))
			.replace(/^https?:\/\/steamcommunity.com\/(id|profiles)\//i, '')
			.replace(/\//g, '')
			.trim()

		if (steamidString.length < 3) {
			instance.error('SteamID/ID is too short', 1)
			return
		}

		this.resolveProfileID(steamidString)
		.then((steamid) => {
			this.resolveSteamID(steamid)
			.then((result: any) => {
				const reply = `${result.profileurl}
\`\`\`
Nickname:            ${result.personaname}
Avatar:              ${result.avatar}
Profile state:       ${result.profilestate == 1 && 'PUBLIC' || 'PRIVATE'}
Time created:        ${new Date(result.timecreated * 1000)}
Last logoff:         ${new Date(result.lastlogoff * 1000)}
Real Name:           ${result.realname && result.realname != '' && result.realname || '<not set>'}
Location:            ${result.loccountrycode && result.loccountrycode != '' && result.loccountrycode || '<not set>'}
Status:              ${SteamIDCommand.profileState[result.personastate]}
SteamID:             ${steamid.steamid}
SteamID3:            ${steamid.steamid3}
SteamID64:           ${steamid.steamid64}
\`\`\``

				instance.reply(reply)
			})
			.catch((err) => {
				instance.reply('Failed to fetch data: ```\n' + err + '\n```')
			})
		})
		.catch((err) => {
			instance.reply('Failed to resolve data: ```\n' + err + '\n```')
		})
	}
}

export {SteamIDCommand}
