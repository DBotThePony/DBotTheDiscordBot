

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class RoleGiverCommand extends CommandBase {
	command_help = 'Gives you a pre-defined role'
	allow_in_dm = false

	constructor() {
		super('iam', 'im', 'givemerole')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermission('MANAGE_ROLES')) {
			instance.reply('Bot is missing MANAGE_ROLES permission!')
			return
		}

		if (!instance.assert(1, 'missing role')) {
			return
		}

		const findRole = instance.findRole(instance.from(1).join(' '))

		if (!findRole) {
			instance.error('This role doesnt even exist. LULLLLLLLLL', 1)
			return
		}

		if (instance.member!.roles.cache.has(findRole.id)) {
			instance.error('You already have this role!', 1)
			return
		}

		this.bot.role_giver.load(instance.server!.id).then(values => {
			if (!values.includes(findRole.id)) {
				instance.error('You can not choose this role: insufficient funds', 1)
				return
			}

			instance.member!.roles.add(findRole).then(() => {
				instance.reply('Role added successfully')
			}).catch(err => {
				instance.reply('Something went wrong: ```\n' + err + '\n```')
			})
		})
	}
}

class RoleRemoverCommand extends CommandBase {
	command_help = 'Removes you from a pre-defined role'
	allow_in_dm = false

	constructor() {
		super('iamnot', 'imno', 'removemerole', 'removemyrole')
	}

	executed(instance: CommandExecutionInstance) {
		if (instance.server && instance.server.me && !instance.server.me.hasPermission('MANAGE_ROLES')) {
			instance.reply('Bot is missing MANAGE_ROLES permission!')
			return
		}

		if (!instance.assert(1, 'missing role')) {
			return
		}

		const findRole = instance.findRole(instance.from(1).join(' '))

		if (!findRole) {
			instance.error('This role doesnt even exist. LULLLLLLLLL', 1)
			return
		}

		if (!instance.member!.roles.cache.has(findRole.id)) {
			instance.error('You already do not have this role!', 1)
			return
		}

		this.bot.role_giver.load(instance.server!.id).then(values => {
			if (!values.includes(findRole.id)) {
				instance.error('You can not choose this role: insufficient funds', 1)
				return
			}

			instance.member!.roles.remove(findRole).then(() => {
				instance.reply('Role removed successfully')
			}).catch(err => {
				instance.reply('Something went wrong: ```\n' + err + '\n```')
			})
		})
	}
}

export {RoleGiverCommand, RoleRemoverCommand}

class ModRoleGiverCommand extends CommandBase {
	command_help = 'Gives an user a pre-defined role'
	allow_in_dm = false
	parse_member_argument = true

	constructor() {
		super('giverole')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermission('MANAGE_ROLES')) {
			instance.reply('Bot is missing MANAGE_ROLES permission!')
			return
		}

		if (!instance.hasPermissionExecutor('MANAGE_NICKNAMES') || !instance.hasPermissionExecutor('BAN_MEMBERS') || !instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.reply('You need MANAGE_NICKNAMES, BAN_MEMBERS and MANAGE_MESSAGES rights to do that')
			return
		}

		if (!instance.assert(2, 'missing role')) {
			return
		}

		if (!instance.getMember(1)) {
			instance.error('Not a user!', 2)
		}

		const findRole = instance.findRole(instance.from(2).join(' '))

		if (!findRole) {
			instance.error('This role doesnt even exist. LULLLLLLLLL', 2)
			return
		}

		if (instance.getMember(1)!.roles.cache.has(findRole.id)) {
			instance.error('User already has this role!', 2)
			return
		}

		this.bot.role_giver.load(instance.server!.id).then(values => {
			if (!values.includes(findRole.id)) {
				instance.error('You can not choose this role: insufficient funds', 1)
				return
			}

			instance.getMember(1)!.roles.add(findRole).then(() => {
				instance.reply('Role added successfully')
			}).catch(err => {
				instance.reply('Something went wrong: ```\n' + err + '\n```')
			})
		})
	}
}

class ModRoleRemoverCommand extends CommandBase {
	command_help = 'Removes a pre-defined role from specified user'
	allow_in_dm = false
	parse_member_argument = true

	constructor() {
		super('removerole')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermission('MANAGE_ROLES')) {
			instance.reply('Bot is missing MANAGE_ROLES permission!')
			return
		}

		if (!instance.hasPermissionExecutor('MANAGE_NICKNAMES') || !instance.hasPermissionExecutor('BAN_MEMBERS') || !instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.reply('You need MANAGE_NICKNAMES, BAN_MEMBERS and MANAGE_MESSAGES rights to do that')
			return
		}

		if (!instance.assert(1, 'missing user')) {
			return
		}

		if (!instance.assert(2, 'missing role')) {
			return
		}

		if (!(instance.get(1) instanceof Discord.GuildMember)) {
			instance.error('Not a user!', 2)
		}

		const findRole = instance.findRole(instance.from(2).join(' '))

		if (!findRole) {
			instance.error('This role doesnt even exist. LULLLLLLLLL', 2)
			return
		}

		if (!(<Discord.GuildMember> instance.get(1)).roles.cache.has(findRole.id)) {
			instance.error('User already do not has this role!', 2)
			return
		}

		this.bot.role_giver.load(instance.server!.id).then(values => {
			if (!values.includes(findRole.id)) {
				instance.error('You can not choose this role: insufficient funds', 1)
				return
			}

			(<Discord.GuildMember> instance.get(1)).roles.remove(findRole).then(() => {
				instance.reply('Role removed successfully')
			}).catch(err => {
				instance.reply('Something went wrong: ```\n' + err + '\n```')
			})
		})
	}
}

export {ModRoleGiverCommand, ModRoleRemoverCommand}

class ManageRolesCommand extends CommandBase {
	command_help = 'Manages roles for giving them to users'
	allow_in_dm = false

	constructor() {
		super('managerole', 'manageroles')
	}

	findRoles(instance: CommandExecutionInstance) {
		const roles = instance.from(2)
		const findRoles = []

		if (roles.length == 0) {
			instance.error('Specify at least one role', 2)
			return null
		}

		for (const role of roles) {
			const find = instance.findRole(role)

			if (find) {
				findRoles.push(find)
			}
		}

		if (findRoles.length == 0) {
			instance.error('Unable to find any matching role ever', 2)
			return null
		}

		return findRoles
	}

	add(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_ROLES')) {
			instance.reply('You lack MANAGE_ROLES permission to execute this command!')
			return
		}

		const findRoles = this.findRoles(instance)

		if (!findRoles) {
			return
		}

		const toadd = []

		for (const role of findRoles) {
			toadd.push(role.id)
		}

		this.bot.role_giver.bulkAdd(instance.server!.id, ...toadd).then((result) => {
			const reply = []

			for (const [id, status, reason] of result) {
				reply.push(`"${id}": ${reason} (${status})`)
			}

			instance.reply('Adding roles to role giver command: ```\n' + reply.join('\n') + '\n```')
		}).catch((err) => {
			instance.reply('```\n' + err + '\n```')
		})
	}

	remove(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_ROLES')) {
			instance.reply('You lack MANAGE_ROLES permission to execute this command!')
			return
		}

		const findRoles = this.findRoles(instance)

		if (!findRoles) {
			return
		}

		const toremove = []

		for (const role of findRoles) {
			toremove.push(role.id)
		}

		this.bot.role_giver.bulkRemove(instance.server!.id, ...toremove).then((result) => {
			const reply = []

			for (const [id, status, reason] of result) {
				reply.push(`"${id}": ${reason} (${status})`)
			}

			instance.reply('Removing roles from role giver command: ```\n' + reply.join('\n') + '\n```')
		}).catch((err) => {
			instance.reply('```\n' + err + '\n```')
		})
	}

	list(instance: CommandExecutionInstance) {
		this.bot.role_giver.load(instance.server!.id).then(values => {
			if (values.length == 0) {
				instance.reply('None registered')
				return
			}

			const reply = []

			for (const value of values) {
				if (instance.server!.roles.cache.has(value)) {
					reply.push(instance.server!.roles.cache.get(value)!.name)
				} else {
					reply.push(value)
				}
			}

			instance.reply('Avaliable roles are: ```\n' + reply.join(', ') + '\n```')
		})
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1, 'missing command. Valid are add, remove, list')) {
			return
		}

		switch ((<string> instance.getString(1)).toLowerCase()) {
			case 'add':
				return this.add(instance)
			case 'remove':
				return this.remove(instance)
			case 'list':
				return this.list(instance)
			default:
				instance.error('Invalid action specified. Valid are add, remove, list', 1)
		}
	}
}

export {ManageRolesCommand}
