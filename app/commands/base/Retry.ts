

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandContext} from '../CommandContext'
import {CommandHolder} from '../CommandHolder'

class Retry extends CommandBase {
	allow_piping = false
	command_help = 'Re-executes previous command (based on CommandContext, but creates new CommandExecutionInstance)'
	can_be_banned = false
	remember_command_context = false

	constructor() {
		super('retry')
	}

	executed(instance: CommandExecutionInstance) {
		if (!this.bot) {
			throw new Error('Bad bot instance')
		}

		const context = this.bot.command_holder.lastContextChannel(instance.channel, instance.author)

		if (!context) {
			instance.reply('There isn\'t any command executed previously!')
			return
		}

		const command = context.getCommand()

		if (!command) {
			return true // what
		}

		const commandObject = this.bot.command_holder.get(command)

		if (!commandObject) {
			return true // wtf
		}

		context.unsetEditOn()
		commandObject.execute(context)
		return true
	}
}

export {Retry}
