

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import crypto = require('crypto')
import unorm = require('unorm')

class HashCommand extends CommandBase {
	hasher: string
	help_arguments = '<string>'
	can_be_banned = false

	constructor(hasher: string) {
		super('hash' + hasher.toLowerCase())
		this.command_help = 'Hash a text with ' + hasher
		this.hasher = hasher.toLowerCase()
	}

	executed(instance: CommandExecutionInstance) {
		return '```' + crypto.createHash(this.hasher).update(instance.raw).digest('hex') + '```'
	}
}

class UnZalgoCommand extends CommandBase {
	help_arguments = '<string>'
	can_be_banned = false
	help = 'Remove kool kids characters from text'

	constructor() {
		super('unzalgo', 'makeitreadable', 'makereadable')
	}

	executed(instance: CommandExecutionInstance) {
		return '```' + this.bot.command_helper.fuckOffZalgoText(instance.raw) + '```'
	}
}

class NFDCommand extends CommandBase {
	help_arguments = '<string>'
	can_be_banned = false
	help = 'Canonical Decomposition'

	constructor() {
		super('nfd')
	}

	executed(instance: CommandExecutionInstance) {
		return '```' + unorm.nfd(instance.raw) + '```'
	}
}

class NFCCommand extends CommandBase {
	help_arguments = '<string>'
	can_be_banned = false
	help = 'Canonical Decomposition, followed by Canonical Composition'

	constructor() {
		super('nfc')
	}

	executed(instance: CommandExecutionInstance) {
		return '```' + unorm.nfc(instance.raw) + '```'
	}
}

class NFKCCommand extends CommandBase {
	help_arguments = '<string>'
	can_be_banned = false
	help = 'Compatibility Decomposition, followed by Canonical Composition'

	constructor() {
		super('nfkc')
	}

	executed(instance: CommandExecutionInstance) {
		return '```' + unorm.nfkc(instance.raw) + '```'
	}
}

class NFKDCommand extends CommandBase {
	help_arguments = '<string>'
	can_be_banned = false
	help = 'Compatibility Decomposition'

	constructor() {
		super('nfkd')
	}

	executed(instance: CommandExecutionInstance) {
		return '```' + unorm.nfkd(instance.raw) + '```'
	}
}

export {NFCCommand, NFDCommand, NFKCCommand, NFKDCommand, UnZalgoCommand}

class ServerOwnerCommand extends CommandBase {
	command_help = 'Displays owner of the guild'
	can_be_banned = false
	allow_in_dm = false

	constructor() {
		super('owner')
	}

	executed(instance: CommandExecutionInstance) {
		return 'Owner of the server is <@' + instance.server!.ownerID + '>'
	}
}

const permissionlist: Discord.PermissionResolvable[] = [
	'CREATE_INSTANT_INVITE',
	'KICK_MEMBERS',
	'BAN_MEMBERS',
	'MANAGE_CHANNELS',
	'MANAGE_GUILD',
	'ADD_REACTIONS',
	// 'VIEW_AUDIT_LOG',
	'VIEW_CHANNEL',
	'SEND_MESSAGES',
	'SEND_TTS_MESSAGES',
	'MANAGE_MESSAGES',
	'EMBED_LINKS',
	'ATTACH_FILES',
	'READ_MESSAGE_HISTORY',
	'MENTION_EVERYONE',
	'USE_EXTERNAL_EMOJIS',
	'CONNECT',
	'SPEAK',
	'MUTE_MEMBERS',
	'DEAFEN_MEMBERS',
	'MOVE_MEMBERS',
	'USE_VAD',
	'CHANGE_NICKNAME',
	'MANAGE_NICKNAMES',
	'MANAGE_ROLES',
	'MANAGE_WEBHOOKS',
	'MANAGE_EMOJIS',
]

class PermissionsList extends CommandBase {
	allow_in_dm = false
	command_help = 'Displays permission of specified user'
	help_arguments = '[user = you]'
	parse_member_argument = true

	constructor() {
		super('permissions', 'perms', 'perm')
	}

	executed(instance: CommandExecutionInstance) {
		let user = instance.member!

		if (instance.get(1)) {
			if (!instance.getMember(1)) {
				return
			}

			user = instance.getMember(1)!
		}

		if (!user) {
			return true
		}

		if (!user.hasPermission('ADMINISTRATOR')) {
			const lines: string[] = []

			for (const perm of permissionlist) {
				lines.push(perm + (' ').repeat(Math.max(20 - (<string> perm).length, 0)) + ': ' + user.hasPermission(perm))
			}

			instance.reply('Permissions: ```\n' + lines.join('\n') + '```')
		} else {
			instance.reply('User is an Administrator\n(all permissions, bypass some checks (both Discord\'s channel permission overwrites and this bot\'s some checks))')
		}
	}
}

class AdminList extends CommandBase {
	allow_in_dm = false
	command_help = 'Displays users with specified permission'
	help_arguments = '[permission = ADMINISTRATOR]'

	constructor() {
		super('admins', 'adminlist')
	}

	executed(instance: CommandExecutionInstance) {
		const perm = <Discord.PermissionResolvable> (<string> (instance.get(1) || 'ADMINISTRATOR')).toUpperCase()

		if (!permissionlist.includes(perm) && perm != 'ADMINISTRATOR') {
			instance.error('Invalid permission supplied', 1)
			return
		}

		const users: string[] = []

		for (const member of instance.server!.members.cache.values()) {
			if (member.hasPermission(perm)) {
				users.push(member.nickname || member.user.username)
			}
		}

		if (users.length != 0 && users.length < 30) {
			instance.reply('Users with ' + perm + ' permission are: ```\n' + users.join(', ') + '```')
		} else if (users.length < 30) {
			instance.reply('No users are avaliable with ' + perm + ' permission!')
		} else {
			instance.reply('There are ' + users.length + ' users with ' + perm + ' permission')
		}
	}
}

class ServerInfo extends CommandBase {
	allow_in_dm = false
	command_help = 'Displays server information'

	constructor() {
		super('server', 'serverinfo')
	}

	executed(instance: CommandExecutionInstance) {
		instance.reply(
			`\`\`\`
Server name:               ${instance.server!.name}
Server ID:                 ${instance.server!.id}
Server owner:              @${instance.server!.owner?.nickname || instance.server!.owner?.user.username} <@${instance.server!.owner?.id}>
Server avatar: ${instance.server!.iconURL || '<no image>'}
Server users:              ${instance.server!.members.cache.size} on fetch/${instance.server!.memberCount} total
Server channels:           ${instance.server!.channels.cache.size}?
Server roles:              ${instance.server!.roles.cache.size}?
Server region:             ${instance.server!.region}
Server created at:         ${instance.server!.createdAt }
Server verification:       ${instance.server!.verificationLevel}
\`\`\``
		)
	}
}

export {HashCommand, ServerOwnerCommand, PermissionsList, AdminList, ServerInfo}
