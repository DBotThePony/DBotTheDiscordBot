

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import { BotInstance } from '../../BotInstance';

class Invite extends CommandBase {
	command_help = 'Invite link'
	can_be_banned = false

	constructor() {
		super('invite')
	}

	executed(instance: CommandExecutionInstance) {
		return 'Link https://discordapp.com/api/oauth2/authorize?client_id=' + (<BotInstance> this.bot).id + '&scope=bot&permissions=0\nJoin https://discord.gg/HG9eS79';
	}
}

class SetAvatar extends CommandBase {
	command_help = 'Set bot avatar'
	display_help = false
	parse_user_argument = true
	can_be_banned = false

	constructor() {
		super('setavatar')
	}

	executed(instance: CommandExecutionInstance) {
		const img = instance.findImage(instance.next())

		if (!img) {
			instance.error('Nu image? ;n;', 1)
			return
		}

		instance.loadImage(img).then((path: string) => {
			(<BotInstance> this.bot).client.user!.setAvatar(path)
			.catch((err) => {
				instance.send(err)
			}).then(() => {
				instance.reply('Avatar updated successfully')
			})
		})
	}
}

class GetAvatar extends CommandBase {
	command_help = 'Get user(s) avatar(s)'
	parse_user_argument = true

	constructor() {
		super('avatar')
	}

	executed(instance: CommandExecutionInstance) {
		let reply = 'Avatars: '

		for (let [i,  user] of instance) {
			if (!(user instanceof Discord.User)) {
				if (!instance.getUser(i)) {
					return
				}

				user = instance.getUser(i)
			}

			reply += '\n' + (<Discord.User> user).avatarURL + ' '
		}

		return reply
	}
}

class About extends CommandBase {
	command_help = 'About'
	can_be_banned = false

	constructor() {
		super('about')
	}

	executed(instance: CommandExecutionInstance) {
		return 'DeeeBootTheDeescordBot V2.0\nRewrite because this thing needs to work better.'
	}
}

class DateCommand extends CommandBase {
	command_help = 'Shows current date'
	can_be_banned = false

	constructor() {
		super('about')
	}

	executed(instance: CommandExecutionInstance) {
		return 'Today is: `' + (new Date()).toString() + '` for me.'
	}
}

import numeral = require('numeral')
import humanizeDuration = require('humanize-duration')

interface NodeRelease {
	name: string
	sourceUrl: string
	headersUrl: string
	libUrl: string
}

class BotInfoCommand extends CommandBase {
	command_help = 'Shows bot information'
	can_be_banned = false

	constructor() {
		super('botinfo')
	}

	executed(instance: CommandExecutionInstance) {
		const memory = process.memoryUsage()

		return `\`\`\`
Platform:           ${process.platform}
Release name:       ${(<NodeRelease> process.release).name}
Release URL:        ${(<NodeRelease> process.release).sourceUrl}
Release Headers:    ${(<NodeRelease> process.release).headersUrl}
Version:            ${process.version}
Uptime:             ${humanizeDuration(process.uptime() * 1000)}
Mem Allocated:      ${numeral(memory.rss).format('0b')}
Heap:               ${numeral(memory.heapTotal).format('0b')}
Heap used:          ${numeral(memory.heapUsed).format('0b')}
\`\`\``
	}
}

const initMessage = [
	'Bleh', 'Pne?', 'Ponies are coming for you', 'Ponis everiwhere!', 'gnignip', 'k', 'Am I a bot?',
	'It is so fun!', 'Lookin\' for something interesting', '*jumps*', 'pew pew', 'vroom'
];

const finishMessage = [
	'this server', 'equestrian bunkers', 'dimension portal controller', 'factories', 'russian botnet',
	'pony PC botnet', 'your PC', 'your Router', 'NSA', 'Mars', 'laboratories', 'bad humans jail',
	'the Machine', 'skynet prototype', 'Russia', 'USA nuclear bombs timer', 'Discord status server',
	'burning fire', 'Google DNS', 'leafletjs.com maps', 'GitLab', 'not working GitHub', 'NotSoSuper',
	'DBot', 'a cat', 'Java application', 'british secret bases', 'China supercomputers', '\\n',
	'your code', 'cake', 'NotSoBot', 'command', 'trap music DJ', 'localhost', '127.0.0.1', '127.199.199.1',
	'meow', 'hacked Miecraft server', 'GMod updates', 'isitdownrightnow.com', 'Google AI', 'Samsung smartphone',
	'memeland', 'block cutting machine', 'HAYO PRODUCTIONS!', 'SCP-173'
];

class Ping extends CommandBase {
	command_help = 'Time to reply'
	can_be_banned = false

	constructor() {
		super('ping')
	}

	executed(instance: CommandExecutionInstance) {
		const now = Date.now()
		const promise = instance.send(initMessage[Math.floor(Math.random() * (initMessage.length - 1))])

		if (promise) {
			promise.then((msg) => {
				(<Discord.Message> msg).edit(`It takes **${(Date.now() - now)}** ms to ping **${instance.next() || finishMessage[Math.floor(Math.random() * (finishMessage.length - 1))]}**`)
			})
		}
	}
}

export {Invite, SetAvatar, GetAvatar, About, Ping, DateCommand, BotInfoCommand}
