

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import { BotInstance } from '../../BotInstance';
import { AsyncDBArrayHolder } from '../../../lib/AsyncDBArrayHolder';

const getname = (channel: boolean, user: boolean) => {
	let name = 'keywords'

	if (channel) {
		name += '_channel'
	}

	if (user) {
		name += '_user'
	}

	return name
}

class KeywordsManip extends CommandBase {
	allow_piping = false
	can_be_banned = false
	allow_in_dm = false
	parse_user_argument = true
	help_arguments = '<subaction> <arguments: channel/?/user> <target>'

	constructor(public isChannel: boolean, public isUser: boolean) {
		super(getname(isChannel, isUser))
		this.command_help = 'Allows keywords banning and unbanning. Subcommands are list, ban, unban, log, test and whitelist'
	}

	get keywords() { return this.bot.keyword_manager }

	canManage(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_GUILD')) {
			instance.reply('You need MANAGE_GUILD permission!')
			return false
		}

		return true
	}

	constructArgs(instance: CommandExecutionInstance, listing = false): [string[], AsyncDBArrayHolder] {
		const user = instance.getUser(2, false)
		let startfrom = !this.isUser && 1 || 2
		const words = [instance.server!.id]
		let banObject

		if (this.isChannel) {
			words.push(instance.channel!.id)
		}

		if (this.isUser) {
			words.push(user!.id)
		}

		if (this.isChannel && this.isUser) {
			banObject = this.keywords.perChannelUser
		} else if (this.isChannel && !this.isUser) {
			banObject = this.keywords.perChannel
		} else if (!this.isChannel && this.isUser) {
			banObject = this.keywords.perServerUser
		} else {
			banObject = this.keywords.perServer
		}

		if (listing) {
			return [words, banObject]
		}

		words.push(instance.rawInput(startfrom).toLowerCase())

		return [words, banObject]
	}

	async ban(instance: CommandExecutionInstance) {
		if (this.isUser && !instance.getUser(2)) {
			return
		}

		const [words, banObject] = this.constructArgs(instance)

		for (const word of words) {
			try {
				new RegExp(word)
			} catch(err) {
				instance.reply('Word `' + word + '` is not a valid regexp: ```js\n' + err + '\n```')
				return
			}
		}

		const statuses = await banObject.bulkAdd(...words)
		const strs = []

		for (const [value, status, reason] of statuses) {
			strs.push(`"${value}": ${reason} (${status})`)
		}

		instance.reply('```\n' + strs.join('\n') + '\n```')
	}

	async unban(instance: CommandExecutionInstance) {
		if (this.isUser && !instance.getUser(2)) {
			return
		}

		const [words, banObject] = this.constructArgs(instance)
		const statuses = await banObject.bulkRemove(...words)
		const strs = []

		for (const [value, status, reason] of statuses) {
			strs.push(`"${value}": ${reason} (${status})`)
		}

		instance.reply('```\n' + strs.join('\n') + '\n```')
	}

	async list(instance: CommandExecutionInstance) {
		if (this.isUser && !instance.getUser(2)) {
			return
		}

		const [words, banObject] = this.constructArgs(instance, true)
		const values = await banObject.load(...words)

		if (values.length == 0) {
			instance.reply('No keywords present in database')
			return
		}

		instance.reply('```\n"' + values.join('", "') + '"\n```')
	}

	async log(instance: CommandExecutionInstance) {
		const result = await instance.query(`SELECT "channel" FROM "keywords_log" WHERE "server" = ${instance.server!.id}`)

		if (result.rowCount == 0) {
			await instance.query(`INSERT INTO "keywords_log" ("server", "channel") VALUES (${instance.server!.id}, ${instance.channel!.id})`)
			instance.reply(`Now i will output message removal log by keywords log into <#${instance.channel!.id}>`)
		} else if (result.rows[0].channel != instance.channel!.id) {
			await instance.query(`DELETE FROM "keywords_log" WHERE "server" = ${instance.server!.id}`)
			instance.reply(`Disabled logging of message removal by keywords module`)
		} else if (result.rows[0].channel != instance.channel!.id) {
			await instance.query(`UPDATE "keywords_log" SET "channel" = ${instance.channel!.id} WHERE "server" = ${instance.server!.id}`)
			instance.reply(`Updated channel of message removal log by keywords log to <#${instance.channel!.id}>`)
		}
	}

	async test(instance: CommandExecutionInstance) {
		if (this.isUser) {
			instance.error('You can not test keywords in this mode', 1)
			return
		}

		const [words, banObject] = this.constructArgs(instance, true)
		const values = await banObject.load(...words)

		if (values.length == 0) {
			instance.reply('No keywords present in database')
			return
		}

		let arr

		if (this.isChannel) {
			arr = await this.keywords.perChannel.load(instance.server!.id, instance.channel!.id)
		} else {
			arr = await this.keywords.perServer.load(instance.server!.id)
		}

		const checkup = <string[]> this.keywords.check(instance.from(2).join(' '), arr, true)

		if (checkup.length == 0) {
			instance.reply('None of filters matched input')
		} else {
			instance.reply('Next filters matched input: ```\n' + checkup.join('\n') + '\n```')
		}
	}

	async whitelist(instance: CommandExecutionInstance) {
		if (await this.keywords.switchWhitelistState(<Discord.TextChannel> instance.channel!)) {
			instance.reply(`<#${instance.channel!.id}> is now whitelisted`)
		} else {
			instance.reply(`<#${instance.channel!.id}> is not longer whitelisted`)
		}
	}

	async executed(instance: CommandExecutionInstance) {
		if (!this.canManage(instance)) {
			instance.reply('insufficient funds for managing this')
			return
		}

		if (!instance.assert(1, 'Invalid action specified. Valid are ban, unban, log, whitelist, test and list')) {
			return
		}

		switch ((<string> instance.getString(1)).toLowerCase()) {
			case 'ban':
				await this.ban(instance)
				return
			case 'unban':
				await this.unban(instance)
				return
			case 'list':
				await this.list(instance)
				return
			case 'log':
				await this.log(instance)
				return
			case 'whitelist':
				await this.whitelist(instance)
				return
			case 'test':
				await this.test(instance)
				return
		}

		instance.error('Invalid action specified. Valid are ban, unban, log, whitelist, test and list', 1)
	}
}

export {KeywordsManip}
