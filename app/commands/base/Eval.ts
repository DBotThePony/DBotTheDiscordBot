

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../CommandBase'
import {CommandContext} from '../CommandContext'
import {CommandHolder} from '../CommandHolder'
import {ParseString} from '../../../lib/StringUtil'
import Discord2 = require('discord.js')
import unorm = require('unorm')

const Discord = Discord2
const ParseString2 = ParseString
const unorm2 = unorm

class Eval extends ImageCommandBase {
	allow_piping = false
	display_help = false
	command_help = 'Debug'
	can_be_banned = false

	constructor() {
		super('eval', 'debug', 'repl')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.isOwner) {
			instance.reply('Not a bot owner')
			return
		}

		try {
			const status = eval(instance.raw)
			instance.send('```js\n' + status + '```')

			if (typeof status == 'object' && status instanceof Promise) {
				status.catch((err) => {
					if (typeof err == 'object' && err instanceof Error) {
						instance.reply('Rejection: ```js\n' + err.stack + '```')
					} else {
						instance.reply('Rejection: ```js\n' + String(err) + '```')
					}
				})
			}

		} catch(err) {
			instance.send('```js\n' + err.stack + '```')
			console.log(instance.context.rawArgs, err)
		}
	}
}

export {Eval}
