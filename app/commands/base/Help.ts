

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandContext} from '../CommandContext'
import {CommandHolder} from '../CommandHolder'
import {BotInstance} from '../../BotInstance'
import {validNumber} from '../../../lib/NumberUtil'

const replaceDesc = /\r?\n/gi

class Help extends CommandBase {
	helpStrs: string[] = []
	allow_piping = false
	can_be_banned = false

	constructor() {
		super('help', '?', 'h')
		this.command_help = 'Shows help'
	}

	buildHelp() {
		this.helpStrs = []

		let helpPages = 0

		for (const command of (<BotInstance> this.bot).command_holder.values()) {
			if (!command.display_help) {
				continue
			}

			helpPages++
		}

		helpPages = Math.ceil(helpPages / 15)

		let page = 1
		let current = `Help page: ${page}/${helpPages}\n` + '```'
		let i = 1

		for (const command of (<BotInstance> this.bot).command_holder.values()) {
			if (!command.display_help) {
				continue
			}

			current += '\n - ' + command.id

			if (command.hasHelpArguments()) {
				current += ' ' + command.getArgumentsString()
			}

			if (command.hasAlias()) {
				current += ' (' + command.command_names.join(', ') + ')'
			}

			if (command.hasHelp()) {
				current += '\n       ' + command.command_help.replace(replaceDesc, '\n    ')
			}

			i++

			if (i > 15) {
				current += '```'
				this.helpStrs[page - 1] = current
				page++
				current = `Help page: ${page}/${helpPages}\n` + '```'
				i = 1
			}
		}

		if (i > 1) {
			current += '```'
			this.helpStrs[page - 1] = current
		}
	}

	executed(instance: CommandExecutionInstance) {
		if (this.helpStrs.length == 0) {
			this.buildHelp()
		}

		if (instance.hasArguments()) {
			const command = <string> instance.next().toLowerCase()
			const commandObj = (<BotInstance> this.bot).command_holder.get(command)
			const page = validNumber(command)

			if (commandObj) {
				let usage = 'Usage: ' + commandObj.id + ' ' + commandObj.getArgumentsString()

				if (commandObj.hasHelp()) {
					usage += '\nHelp:\n```' + commandObj.command_help + '```'
				}

				instance.send(usage)
			} else if (page) {
				if (!this.helpStrs[page - 1]) {
					instance.reply('Not a valid page number!')
					return
				}

				if (instance.isInServer) {
					instance.reply('Sent over DM')
				}

				instance.sendPM(this.helpStrs[page - 1])
			} else {
				instance.reply('Unknown command: ' + command)
			}
		} else {
			if (instance.isInServer) {
				instance.reply('Sent over DM')
			}

			instance.sendPM(this.helpStrs[0])
		}
	}
}

export {Help}
