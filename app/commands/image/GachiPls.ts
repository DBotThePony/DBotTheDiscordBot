

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase, RegularImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class GachiPls extends ImageCommandBase {
	command_help = 'DETH'
	help_arguments = '<text>'

	constructor() {
		super('gachipls')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1)) {
			return
		}

		const magikArgs = [
			'-background', '#0B0B05',

			'(',
				'(',
					'-size', '112x112',
					'canvas:transparent',
				')',

				'-size', 'x112',
				'canvas:transparent',
				'-fill', '#62A0E0',
				'-family', 'PT Sans',
				'-style', 'Normal',
				'label:' + this.escapeLiterals(instance.raw),

				'+append',

			')',

			'-repage', '0x0',
			'null:',

			'(',
				process.cwd() + '/resource/files/gachipls.gif',
				'-coalesce',
			')',

			'-alpha', 'off',

			'-compose', 'Over', '-layers', 'composite', '-layers', 'optimize',

			'gif:-'
		]

		this.convert(instance, ...magikArgs)
		.then((buffer) => {
			instance.reply('', new Discord.MessageAttachment(buffer, 'gachi.gif'))
		})
	}
}

export {GachiPls}
