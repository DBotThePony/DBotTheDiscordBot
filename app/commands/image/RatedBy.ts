

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase, RegularImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class RatedByCommand extends ImageCommandBase {
	command_help = 'rated by U'
	help_arguments = '<top text> <age string/number> <commentary 1> [...commentaries]'

	constructor() {
		super('ratedby')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1, 'Top text is needed')) {
			return
		}

		if (!instance.assert(2, 'Age is needed')) {
			return
		}

		if (!instance.assert(3, 'At least one commentary is required')) {
			return
		}

		let toptext = <string> instance.get(1)
		let agestring = <string> instance.get(2)
		let commentaries = <string[]> instance.from(3)

		const parseAge = parseInt(agestring)

		if (parseAge == parseAge && String(parseAge).length == agestring.length) {
			agestring = agestring + '+™'
		}

		const magikArgs: string[] = [
			'-size', '640x350',
			'canvas:black',
			'-background', 'black',
			'-fill', 'white',

			'-draw', 'rectangle 3,3 637,6',
			'-draw', 'rectangle 3,3 6,347',
			'-draw', 'rectangle 6,347 637,344',
			'-draw', 'rectangle 637,6 634,344',
			'-draw', 'rectangle 15,40 195,300',
			'-draw', 'rectangle 205,40 625,300',

			'-font', 'BebasNeue',
			'-gravity', 'NorthWest',
			'-pointsize', '36',
			'-draw', 'text 15,4 ' + this.escapeText(toptext),

			'-gravity', 'NorthEast',
			'-draw', 'text 10,4 ' + this.escapeText(agestring),

			'-gravity', 'NorthWest',
			'-draw', 'text 15,300 "' + (instance.member && instance.member.nickname || instance.author!.username) + '\'s CONTENT RATING"',

			'-pointsize', '300',
			'-fill', 'black',
			'-draw', 'rotate -20 text 10,40 "' + toptext.substr(0, 1) + '"',
			'-pointsize', '36',
		]

		let i = 0

		for (const arg of commentaries) {
			magikArgs.push('-draw', 'text 220,' + (40 + i * 28) + ' ' + this.escapeText(arg))
			i++
		}

		magikArgs.push('png:-')

		this.convert(instance, ...magikArgs)
		.then((buff) => {
			instance.reply('', new Discord.MessageAttachment(buff!, 'ratedby.png'))
		})
	}
}

export {RatedByCommand}
