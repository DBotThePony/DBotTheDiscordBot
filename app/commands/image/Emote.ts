

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase, RegularImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import { EMOJI_REGEXP_BASE, EMOJI_FUNCS } from '../../lib/Emoji';

const globalExp = new RegExp('\\!?(<:[^:]+:[0-9]+>|' + EMOJI_REGEXP_BASE + '|\\n)', 'gi')
const discordEmoteExp = /<:[^:]+:([0-9]+)>/

const rep = ['0⃣', '1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣']
const rMap = new Map<string, string>()

for (let i = 0; i < 10; i++) {
	rMap.set(String(i), rep[i])
}

for (let i = 0; i < 26; i++) {
	rMap.set(String.fromCharCode(0x61 + i), String.fromCharCode(55356, 56806 + i))
}

// `https://cdn.discordapp.com/emojis/.png?v=1`

interface EmoteData {
	invert?: boolean
	path?: string | null
	url?: string | null
	newline: boolean
}

class EmoteCommand extends ImageCommandBase {
	command_help = 'Posts emotes sequence as image'
	help_arguments = '[!]<emote(s)>'

	constructor() {
		super('e', 'emote', 'emoji', 'epost')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1)) {
			return
		}

		const slice = instance.raw.toLowerCase().split(/\!?<:[^:]+:[0-9]+>/)
		const findEmojis = instance.raw.toLowerCase().match(/\!?<:[^:]+:[0-9]+>/gi) || []
		const nslice = []
		let next = 0

		for (const str of slice) {
			nslice.push(str.replace(/[0-9a-z]/gi, (substr) => rMap.has(substr) ? rMap.get(substr)! + ' ' : substr))

			if (findEmojis[next]) {
				nslice.push(findEmojis[next])
				next++
			}
		}

		const matchThings = nslice.join().match(globalExp)

		if (matchThings == null) {
			instance.reply('None emojis were detected!')
			return
		}

		let valid = false

		for (const thing of matchThings) {
			if (thing != '\n') {
				valid = true
				break
			}
		}

		if (!valid) {
			instance.reply('None emojis were detected!')
			return
		}

		const pathes: EmoteData[] = []
		let I = 0
		let hasCustom = false

		const continueWork = () => {
			const magikArgs: string[] = [
				'-background', 'transparent',
				'-alpha', 'on',
			]

			let args = []

			for (const path of pathes) {
				if (!path) {
					continue // hi js
				}

				if (path.newline) {
					if (args.length != 0) {
						magikArgs.push('(')
						magikArgs.push(...args)
						magikArgs.push('+append', ')')
						args = []
					}

					continue
				}

				if (!path.url) {
					args.push(
						'(',
							'-density', '2048',
							path.path!,
							'-resize', 'x512',
					)
				} else {
					args.push(
						'(',
							path.path!,
							'-resize', 'x512',
					)
				}

				if (path.invert) {
					args.push('-flop')
				}

				args.push(')')
			}

			if (args.length != 0) {
				magikArgs.push('(')
				magikArgs.push(...args)
				magikArgs.push('+append', ')')
			}

			magikArgs.push('-append', 'png:-')

			this.convert(instance, ...magikArgs)
			.then((buffer) => {
				instance.reply('', new Discord.MessageAttachment(buffer, 'emotes.png'))
			})
		}

		for (let thing of matchThings) {
			if (thing == '\n') {
				pathes[I] = {
					newline: true,
				}

				I++
				continue
			}

			let invert = thing.substr(0, 1) == '!'

			if (invert) {
				thing = thing.substr(1)
			}

			if (EMOJI_FUNCS.is(thing)) {
				pathes[I] = {
					newline: false,
					'invert': invert,
					path: EMOJI_FUNCS.mapOnDisk(thing),
					url: null
				}

				I++
			} else if (thing.trim() != '') {
				const ID = thing.trim().match(discordEmoteExp)

				if (ID) {
					hasCustom = true

					pathes[I] = {
						newline: false,
						'invert': invert,
						path: null,
						url: `https://cdn.discordapp.com/emojis/${ID[1]}.png?v=1`
					}
				}

				I++
			}
		}

		if (pathes.length >= 80 && !instance.isOwner) {
			instance.reply('Too many emotes!')
			return
		}

		if (hasCustom) {
			let images = 0

			for (const path of pathes) {
				if (path.url) {
					images++

					instance.loadImage(path.url).then((path2) => {
						path.path = path2
						images--

						if (images == 0) {
							continueWork()
						}
					})
				}
			}
		} else {
			continueWork()
		}
	}
}

export {EmoteCommand}
