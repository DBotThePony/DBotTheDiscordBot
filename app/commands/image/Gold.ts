

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase, RegularImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class GoldCommand extends ImageCommandBase {
	command_help = 'we need more gold'
	help_arguments = '<text>'

	constructor() {
		super('gold')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1)) {
			return
		}

		const esc = this.escapeText('+' + instance.raw)

		const magikArgs = [
			process.cwd() + '/resource/files/wc3.jpg',
			'-gravity', 'center',
			'-pointsize', '48',

			'-draw', 'fill gold text 0,-50 ' + esc + ' fill rgba(255,215,0,0.6) text 0,-180 ' + esc + ' fill rgba(255,215,0,0.3) text 0,-300 ' + esc,

			'png:-'
		]

		this.convert(instance, ...magikArgs)
		.then((buffer) => {
			instance.send('', new Discord.MessageAttachment(buffer, 'gold.png'))
		})
	}
}

export {GoldCommand}
