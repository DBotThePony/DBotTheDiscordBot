

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase, RegularImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import { ImageIdentify } from '../../../lib/imagemagick/Identify';

class WastedCommand extends RegularImageCommandBase {
	command_help = 'wasted'
	parse_user_argument = true
	help_arguments = '<target>'

	constructor(public toptext = 'wasted', public bottomtext = '') {
		super(toptext.toLowerCase())
	}

	doImage(instance: CommandExecutionInstance, identify: ImageIdentify, w: number, h: number) {
		let signHeight = Math.min(w!, h!) / 7
		let pointsize = signHeight * 0.85

		if (this.bottomtext != '') {
			signHeight *= 1.2
		}

		const image: string[] = [
			identify.path!, '-resize', '2048x2048>', '-resize', '512x512<',
			'-color-matrix', '.3 .1 .3 .3 .1 .3 .3 .1 .3', '-fill', 'rgba(0,0,0,0.5)',

			'-draw', 'rectangle 0, ' + (h! / 2 - signHeight / 2) + ', ' + w + ', ' + (h! / 2 + signHeight / 2),

			'-gravity', 'South',
			'-font', 'PricedownBl-Regular',
			'-weight', '300',
		]

		let textpos = Math.floor(h! / 2 - signHeight * .45)

		if (this.bottomtext != '') {
			image.push(
				'-fill', 'rgb(200,200,200)',
				'-pointsize', String(pointsize * 0.43),
				'-draw', 'text 0,' + (Math.floor(h! / 2 - signHeight * .45)) + ' "' + this.bottomtext + '"',
				'-gravity', 'North'
			)

			pointsize *= 0.9
			textpos -= h! * 0.025
		}

		image.push(
			'-fill', 'rgb(200,30,30)',
			'-stroke', 'black',
			'-strokewidth', String(Math.floor(Math.min(w!, h!) / 400)),

			'-pointsize', String(pointsize),
			'-draw', 'text 0,' + textpos + ' "' + this.toptext + '"',
			'png:-'
		)

		this.convert(instance, ...image)
		.then((value) => {
			instance.reply('', new Discord.MessageAttachment(value!, 'wasted.png'))
		})
	}
}

class YouDied extends RegularImageCommandBase {
	command_help = 'forsenPls YOU DIED forsenPls'
	parse_user_argument = true
	help_arguments = '<target>'

	constructor() {
		super('youdied', 'dead', 'ded', 'youareded', 'youaredead', 'darksouls')
	}

	doImage(instance: CommandExecutionInstance, identify: ImageIdentify, w: number, h: number) {
		const targettext = instance.has(2) && instance.from(2).join(' ') || 'YOU DIED'
		let signHeight = Math.min(w!, h!) / 7
		let pointsize = signHeight

		const image: string[] = [
			identify.path!, '-resize', '2048x2048>', '-resize', '512x512<',
			'-color-matrix', '.3 .1 .3 .3 .1 .3 .3 .1 .3', '-fill', 'rgba(0,0,0,0.05)',
		]

		const internsShadowCount = Math.floor(signHeight / 4)

		for (let i = internsShadowCount; i >= 0; i--) {
			image.push('-draw', 'rectangle 0, ' + (h! / 2 - signHeight / 2 - i) + ', ' + w! + ', ' + (h! / 2 + signHeight / 2 + i));
		}

		image.push(
			'-gravity', 'South',
			'-font', 'OptimusPrinceps',

			'-fill', 'rgb(160,30,30)',
			'-stroke', 'black',
			'-strokewidth', String(Math.floor(Math.min(w!, h!) / 400)),

			'-pointsize', String(pointsize),
			'-draw', 'text 0,' + Math.floor(h! / 2 - signHeight * .55) + ' ' + this.escapeText(targettext),
			'png:-'
		)

		this.convert(instance, ...image)
		.then((value) => {
			instance.reply('', new Discord.MessageAttachment(value!, 'wasted.png'))
		})
	}
}

export {WastedCommand, YouDied}
