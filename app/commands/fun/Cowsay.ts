

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import cowsay = require('cowsay')

class Cowsay extends CommandBase {
	askFile: string
	help_arguments = '<string>'

	constructor(cowname: string) {
		super(cowname + 'say', cowname)
		this.command_help = cowname + 'say the word'
		this.askFile = cowname

		if (cowname == 'cow') {
			this.askFile = 'default'
		}
	}

	executed(instance: CommandExecutionInstance) {
		const result = <string> cowsay.say({
			text: instance.raw.replace(/```/gi, '``\`'),
			f: this.askFile
		})

		return '```\n' + result + '\n```'
	}
}

class ICowsay extends ImageCommandBase {
	askFile: string
	help_arguments = '<string>'

	constructor(cowname: string) {
		super('i' + cowname + 'say', 'i' + cowname)
		this.command_help = cowname + 'say the word'
		this.askFile = cowname

		if (cowname == 'cow') {
			this.askFile = 'default'
		}
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionBoth('ATTACH_FILES')) {
			instance.reply('Can not attach files!')
			return
		}

		const result = <string> cowsay.say({
			text: instance.raw.replace(/```/gi, '``\\`'),
			f: this.askFile
		})

		this.convert(instance, '-font', 'PT-Mono', '-pointsize', '64', '-background', 'transparent', '-fill', 'Grey', '-gravity', 'NorthWest',
		'label:' + this.escapeLiterals(result), 'png:-')
		.then((buff) => {
			instance.send('', new Discord.MessageAttachment(buff, 'cowsay.png'))
		})
	}
}

const cows = [
	'cow',
	'tux',
	'sheep',
	'www',
	'dragon',
	'vader',
];

const RegisterCowsay = function(holder: CommandHolder) {
	for (const cow of cows) {
		holder.registerCommand(new Cowsay(cow))
		holder.registerCommand(new ICowsay(cow))
	}
}

export {RegisterCowsay}
