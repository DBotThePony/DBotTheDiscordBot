

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class AddAutoReact extends CommandBase {
	command_help = 'Adds auto react' + (this.isChannel ? ' per channel' : '')
	allow_in_dm = false

	constructor(public isChannel = false) {
		super(!isChannel && 'addreact' || 'addchannelreact')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.reply('You need MANAGE_MESSAGES permission!')
			return
		}

		if (!instance.assert(1, 'Missing reaction trigger (it IS a regular expression)')) {
			return
		}

		if (!instance.assert(2, 'Missing reaction text')) {
			return
		}

		if (this.isChannel) {
			this.bot.reaction_manager.addChannelPattern(<Discord.TextChannel> instance.channel!, instance.get(1), instance.from(2).join(' '))
			.then(result => {
				instance.reply('Per-channel reaction added successfully: ' + result)
			}).catch(err => {
				instance.reply('Unable to add per-channel reaction: ' + err)
			})
		} else {
			this.bot.reaction_manager.addPattern(instance.server!, instance.get(1), instance.from(2).join(' '))
			.then(result => {
				instance.reply('Per-server reaction added successfully: ' + result)
			}).catch(err => {
				instance.reply('Unable to add per-server reaction: ' + err)
			})
		}
	}
}

class RemoveAutoReact extends CommandBase {
	command_help = 'Removes auto react' + (this.isChannel ? ' per channel' : '') + ' (if any)'
	allow_in_dm = false

	constructor(public isChannel = false) {
		super(!isChannel && 'removereact' || 'removechannelreact')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.reply('You need MANAGE_MESSAGES permission!')
			return
		}

		if (!instance.assert(1, 'Missing reaction trigger')) {
			return
		}

		if (this.isChannel) {
			this.bot.reaction_manager.removeChannelPattern(<Discord.TextChannel> instance.channel!, instance.get(1))
			.then(result => {
				instance.reply('Per-channel reaction removed successfully: ' + result)
			}).catch(err => {
				instance.reply('Unable to remove per-channel reaction: ' + err)
			})
		} else {
			this.bot.reaction_manager.removePattern(instance.server!, instance.get(1))
			.then(result => {
				instance.reply('Per-server reaction removed successfully: ' + result)
			}).catch(err => {
				instance.reply('Unable to remove per-server reaction: ' + err)
			})
		}
	}
}

class ListAutoReact extends CommandBase {
	command_help = 'Lists auto react' + (this.isChannel ? ' per channel' : '') + ' (if any)'
	allow_in_dm = false

	constructor(public isChannel = false) {
		super(!isChannel && 'listreact' || 'listchannelreact')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.hasPermissionExecutor('MANAGE_MESSAGES')) {
			instance.reply('You need MANAGE_MESSAGES permission! (yep even for listing MEGALUL)')
			return
		}

		if (this.isChannel) {
			this.bot.reaction_manager.resolveChannel(<Discord.TextChannel> instance.channel!)
			.then(result => {
				const lines = []

				for (const pattern in result) {
					lines.push(`${pattern} -> ${result[pattern]}`)
				}

				instance.reply('Per-channel reaction list: ```\n' + lines.join('\n') + '\n```')
			}).catch(err => {
				instance.reply('Unable to get per-channel reaction list: ' + err)
			})
		} else {
			this.bot.reaction_manager.resolveServer(instance.server!)
			.then(result => {
				const lines = []

				for (const pattern in result) {
					lines.push(`${pattern} -> ${result[pattern]}`)
				}

				instance.reply('Per-server reaction list: ```\n' + lines.join('\n') + '\n```')
			}).catch(err => {
				instance.reply('Unable to get per-server reaction list: ' + err)
			})
		}
	}
}

export {AddAutoReact, RemoveAutoReact, ListAutoReact}
