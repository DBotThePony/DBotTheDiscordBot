

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../../CommandBase'
import {CommandHolder} from '../../CommandHolder'
import Discord = require('discord.js')

import fs = require('fs')

const avaliableCategories: string[] = []
const defCategories: string[] = []
const defCategoriesIndex = new Map<string, string[]>()

function addFortune(name: string) {
	fs.readdir(`./resource/fortune/${name}/`, (err, files) => {
		avaliableCategories.push(...files.map(a => `${name}/${a}`))

		for (const file of files) {
			const target: string[] = []
			defCategoriesIndex.set(`${name}/${file}`, target)

			fs.readFile(`./resource/fortune/${name}/${file}`, {encoding: 'utf8'}, (err, data) => {
				const lines = data.split(/%\r?\n/)

				for (const line of lines) {
					if (line.trim() == '') {
						continue
					}

					defCategories.push(line)
					target.push(line)
				}
			})
		}
	})
}

addFortune('debian')
addFortune('freebsd')

fs.readFile(`./resource/fortune/kernelnewbies.txt`, {encoding: 'utf8'}, (err, data) => {
	const target: string[] = []
	defCategoriesIndex.set(`kernelnewbies`, target)

	const lines = data.split(/%\r?\n/)

	for (const line of lines) {
		if (line.trim() == '') {
			continue
		}

		defCategories.push(line)
		target.push(line)
	}
})

fs.readFile(`./resource/fortune/cat-v.txt`, {encoding: 'utf8'}, (err, data) => {
	const target: string[] = []
	defCategoriesIndex.set(`cat-v`, target)

	const lines = data.split(/\r?\n\r?\n/)

	for (const line of lines) {
		if (line.trim() == '') {
			continue
		}

		defCategories.push(line)
		target.push(line)
	}
})

fs.readFile(`./resource/fortune/plan9.txt`, {encoding: 'utf8'}, (err, data) => {
	const target: string[] = []
	defCategoriesIndex.set(`plan9`, target)

	const lines = data.split(/\r?\n/)

	for (const line of lines) {
		if (line.trim() == '') {
			continue
		}

		defCategories.push(line)
		target.push(line)
	}
})

const avaliableCategoriesVulgar: string[] = []
const vulgarCategories: string[] = []
const vulgarCategoriesIndex = new Map<string, string[]>()

function addFortuneVulgar(name: string) {
	fs.readdir(`./resource/fortune_vulgar/${name}/`, (err, files) => {
		avaliableCategoriesVulgar.push(...files.map(a => `${name}/${a}`))

		for (const file of files) {
			const target: string[] = []
			vulgarCategoriesIndex.set(`${name}/${file}`, target)

			fs.readFile(`./resource/fortune_vulgar/${name}/${file}`, {encoding: 'utf8'}, (err, data) => {
				const lines = data.split(/%\r?\n/)

				for (const line of lines) {
					vulgarCategories.push(line)
					target.push(line)
				}
			})
		}
	})
}

addFortuneVulgar('debian')
addFortuneVulgar('freebsd')

class Fortune extends CommandBase {
	command_help = ':$ fortune [category]'

	constructor() {
		super('fortune')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.get(1)) {
			instance.reply('```\n' + defCategories[Math.floor(Math.random() * (defCategories.length - 1))] + '```')
			return
		}

		const category = (<string> instance.get(1)).toLowerCase()

		if (!defCategoriesIndex.has(category)) {
			instance.error('Invalid category. Valid are: ' + avaliableCategories.join(', '), 1)
			return
		}

		const list = <string[]> defCategoriesIndex.get(category)
		instance.reply('```\n' + list[Math.floor(Math.random() * (list.length - 1))] + '```')
	}
}

class Intel extends CommandBase {
	command_help = 'intel'

	constructor() {
		super('intel')
	}

	executed(instance: CommandExecutionInstance) {
		instance.send('```\n[2015-04-15] <NEO> Что за странные слова, пукнет, черепадла, у нас тут интеллигентное общество.```')
	}
}

class NoIntel extends CommandBase {
	command_help = 'nointel'

	constructor() {
		super('nointel')
	}

	executed(instance: CommandExecutionInstance) {
		instance.send('```\nВот ты лайкаешь пост Нео "что за странные слова у Алекса "пук, чих и черепадлики" а сам такие же странные слова и юзаешь, типа "блин" и "офигенен"\nЧто за "американские двойные" стандарты. Определись уже в нашем "интеллигентном" обществе. Или у тебя концепция "Баба яга всегда против (Алекса)"? :facepalm:```')
	}
}

class Ogon extends CommandBase {
	command_help = 'ogon'

	constructor() {
		super('ogon')
	}

	executed(instance: CommandExecutionInstance) {
		instance.send('```\nMeXaN1cK Yesterday at 11:12 PM\nэти "эмодзи" такая же бесполезная хрень как и смайлики на твиче\n\nMeXaN1cK Yesterday at 11:17 PM\nМне вот эти люди, которые помешаны на всяких там смайликах, эмодзи, мемчиках и прочем напоминают неандертальцев - "О, огонь", "О, новый смайлик", "О, новый мем", "О, новый говнофон" и т.п```')
	}
}

export {Ogon}

class Pogreb extends CommandBase {
	command_help = 'pogreb'

	constructor() {
		super('pogreb')
	}

	executed(instance: CommandExecutionInstance) {
		instance.send('Погреб - погреб, но не простой, а с психотропными веществами. И есть в том погребе негр. И зовут его LeshaInc.')
	}
}

class Zander extends CommandBase {
	command_help = 'zander'

	constructor() {
		super('zander')
	}

	executed(instance: CommandExecutionInstance) {
		instance.send('Мой ник Zander , меня забанил модератор topgamer с причиной неадекват, прошу меня разбанить а его снять с должности т к он меня обзывал фашистом так как я на незаприваченой территории сломал несколько блоков булыжника которые он назавал славянские руны понаставил свастики около моего региона , непотребно общался со мной и убил меня, хотя я ему не грубил , а потом забанил со словами ты меня за%бал скрины прилигаю , правда без его мата , неуспел перед баном заскринить. https://i.dbotthepony.ru/2018/10/cff9b979d882466788bad091a194d308.png https://i.dbotthepony.ru/2018/10/998480c448a5435881844a0020855f76.png https://i.dbotthepony.ru/2018/10/f6a1f45a1472432c9e5fb0edd0bdcbc5.png https://i.dbotthepony.ru/2018/10/a9ddb65af23f4a6c8e8a8741534739b3.png  Считаю его бан несправедливым т к он просто предвзято ко мне отнёсся , а а другими админами и игроками я общался нормально и адекватно')
	}
}

export {Zander}

class VulgarFortune extends CommandBase {
	command_help = ':$ fortune [category]'

	constructor() {
		super('vfortune', 'vulgarfortune', 'vulgar_fortune')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.get(1)) {
			instance.reply('```\n' + vulgarCategories[Math.floor(Math.random() * (defCategories.length - 1))] + '```')
			return
		}

		const category = (<string> instance.get(1)).toLowerCase()

		if (!vulgarCategoriesIndex.has(category)) {
			instance.error('Invalid category' + avaliableCategoriesVulgar.join(', '), 1)
			return
		}

		const list = <string[]> vulgarCategoriesIndex.get(category)
		instance.reply('```\n' + list[Math.floor(Math.random() * (list.length - 1))] + '```')
	}
}

const uniqueStr = '\x001\x001\x002'
const uniqueStrExp = new RegExp(uniqueStr, 'g')
const copyPastaDict: string[] = []

for (const str of fs.readFileSync('./resource/copypasta.csv', {encoding: 'utf8'}).replace(/""/g, uniqueStr).split(/"([^"]*)"/g)) {
	const result = str.trim().replace(uniqueStrExp, '"').replace(/\r?\n/g, '')

	if (result.length != 0) {
		copyPastaDict.push(result)
	}
}

class CopyPasta extends CommandBase {
	command_help = 'Posts a random quote from http://www.twitchquotes.com/'

	constructor() {
		super('copypasta', 'mfortune', 'tfortune')
	}

	executed(instance: CommandExecutionInstance) {
		instance.reply('```\n' + copyPastaDict[Math.floor(Math.random() * (copyPastaDict.length - 1))] + '```')
	}
}

export {Fortune, VulgarFortune, CopyPasta, Intel, NoIntel, Pogreb}
