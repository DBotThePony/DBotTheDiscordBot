
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import {CommandBase, CommandExecutionInstance} from '../../CommandBase'

class SCPCommand extends CommandBase {
	allow_piping = true
	display_help = true
	command_help = 'Formats input text as scp text'

	constructor() {
		super('scp')
	}

	async executed(instance: CommandExecutionInstance) {
		const text = instance.raw

		if (text == '' || text.length < 20) {
			instance.error('Text input is too short', 1)
			return
		}

		const body = (await this.bot.post('https://scpstuff.ru/scp-text', {
			'Content-Type': 'application/x-www-form-urlencoded'
		}, {
			'expunge_var': 30,
			'plaque_var': 10,
			'raw': 1,
			'source': text
		})).toString('utf8').trim()

		if (body == '') {
			instance.reply('Empty result.')
			return
		}

		instance.send('```\n' + body + '\n```')
	}
}

export {SCPCommand}
