

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../../CommandBase'
import {CommandHolder} from '../../CommandHolder'
import Discord = require('discord.js')

import fs = require('fs')
const AvaliablePonies: string[] = []
let AvaliablePoniesPNG: string[] = []

fs.readdir('./resource/poni_txt/', (err, files) => {
	for (const file of files) {
		fs.readFile('./resource/poni_txt/' + file, 'utf8', (err, data) => {
			AvaliablePonies.push(data)
		})
	}
})

fs.readdir('./resource/poni/', (err, files) => {
	AvaliablePoniesPNG = files
})

class ASCIIPonyCommand extends CommandBase {
	command_help = 'Posts an ASCII pone'

	constructor() {
		super('pony', 'pone', 'poneh')
	}

	executed(instance: CommandExecutionInstance) {
		instance.send('```\n' + AvaliablePonies[Math.floor(Math.random() * (AvaliablePonies.length - 1))] + '```')
	}
}

class ASCIIPonyImageCommand extends CommandBase {
	command_help = 'Posts an ASCII pone as image'

	constructor() {
		super('ipony', 'ipone', 'iponeh')
	}

	executed(instance: CommandExecutionInstance) {
		const path = AvaliablePoniesPNG[Math.floor(Math.random() * (AvaliablePoniesPNG.length - 1))]

		fs.readFile('./resource/poni/' + path, {encoding: null}, (err, data) => {
			if (err) {
				instance.reply('```\n' + err + '```')
				return
			}

			instance.send('', new Discord.MessageAttachment(data, 'ponie.png'))
		})

	}
}

export {ASCIIPonyCommand, ASCIIPonyImageCommand }
