

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../../CommandBase'
import {CommandHolder} from '../../CommandHolder'
import Discord = require('discord.js')

class RollTheDice extends CommandBase {
	command_help = 'Rolls the dice'
	help_arguments = '[edges] [times]'

	constructor() {
		super('rtd', 'rollthedice', 'dice')
	}

	executed(instance: CommandExecutionInstance) {
		let edges = instance.get(1) || 6
		let times = instance.get(2) || 1

		if (typeof edges == 'string') {
			const split = edges.split('d')

			if (split.length == 2) {
				edges = parseInt(split[0])
				times = parseInt(split[1])

				if (edges != edges) {
					instance.error('Invalid amount of edges provided' , 1)
					return
				}

				if (times != times) {
					instance.error('Invalid attempts provided' , 1)
					return
				}

				if (times > 20 || times <= 0) {
					instance.error('wtf' , 1)
					return
				}
			} else {
				edges = parseInt(edges)

				if (edges != edges) {
					instance.error('Invalid amount of edges provided' , 1)
					return
				}
			}

			if (edges > 100 || edges <= 1) {
				instance.error('wtf' , 1)
				return
			}
		}

		if (typeof times == 'string') {
			times = parseInt(times)

			if (times != times) {
				instance.error('Invalid attempts provided' , 2)
				return
			}

			if (times > 20 || times <= 0) {
				instance.error('wtf' , 2)
				return
			}
		}

		const rolls: number[] = []

		for (let roll = 1; roll <= times; roll++) {
			rolls.push(Math.floor(Math.random() * (edges - 1)) + 1)
		}

		if (instance.isDM) {
			instance.send('You rolled: ```\n' + rolls.join(', ') + '\n```')
		} else {
			instance.send('<@' + instance.author!.id + '> rolled: ```\n' + rolls.join(', ') + '\n```')
		}
	}
}

export {RollTheDice}
