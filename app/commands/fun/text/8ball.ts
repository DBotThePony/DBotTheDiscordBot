
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import {CommandBase, CommandExecutionInstance} from '../../CommandBase'
import Discord = require('discord.js')
import crypto = require('crypto')

// instead of being definitely good
// or definitely bad
// lets assume weights
const replies = [
	{
		weight: 100,
		replies: [
			'It is certain',
			'It is decidedly so',
			'Without a doubt',
			'Yes — definitely',
			'You may rely on it',
		]
	},

	{
		weight: 130,
		replies: [
			'As I see it, yes',
			'Most likely',
			'Outlook good',
			'Signs point to yes',
			'Yes',
		]
	},

	{
		weight: 400,
		nosave: true,
		replies: [
			'Reply hazy, try again',
			'Ask again later',
			'Better not tell you now',
			'Cannot predict now',
			'Concentrate and ask again',
		]
	},

	{
		weight: 100,
		replies: [
			'Don’t count on it',
			'My reply is no',
			'My sources say no',
			'Outlook not so good',
			'Very doubtful',
		]
	}
]

const listing: string[] = []
let _maxscore = 0

for (const member of replies) {
	listing.push(...member.replies)
	_maxscore += member.weight
}

class MagicBallCommand extends CommandBase {
	allow_piping = true
	command_help = 'Gives somewhat random answer to given phrase. Use numbers to specify message relative to command or absolute message ID'

	constructor() {
		super('8ball', 'magicball')
	}

	async executed(instance: CommandExecutionInstance) {
		if (!instance.assert(1)) {
			return
		}

		let message: string
		let user: Discord.User
		const num = parseInt(instance.get(1))

		if (num < 0) {
			instance.error('Negative message ID?', 1)
			return
		}

		if (num == num && num != 42) {
			const getmessage = instance.channel!.messages.cache.get(instance.get(1))

			if (getmessage) {
				if (getmessage.content == '') {
					instance.error('Can not use empty message', 1)
					return
				}

				message = getmessage.content
				user = getmessage.author
			} else {
				const messages = instance.channel!.messages.cache.array()
				const getmessage = messages[messages.length - num]

				if (getmessage) {
					if (getmessage.content == '') {
						instance.error('Can not use empty message', 1)
						return
					}

					message = getmessage.content
					user = getmessage.author
				} else {
					instance.error('I have no idea what you meant', 1)
					return
				}
			}
		} else {
			message = instance.raw
			user = instance.author!
		}

		if (!user) {
			throw new TypeError('User was never defined')
		}

		const hash = crypto.createHash('sha256').update(message).digest('hex')
		const result = await this.sql.query(`SELECT "answer" FROM "8ball" WHERE "user" = '${user.id}' AND "message" = '\\x${hash}'`)

		if (result.rowCount != 0) {
			instance.reply(listing[result.rows[0].answer])
			return
		}

		const cscore = Math.random() * _maxscore
		let myscore = 0

		for (const i in replies) {
			const member = replies[i]
			myscore += member.weight

			if (myscore >= cscore) {
				const rand = member.replies[Math.floor(Math.random() * (member.replies.length - 1))]
				const indexof = listing.indexOf(rand)

				if (!member.nosave) {
					await this.sql.query(`INSERT INTO "8ball" VALUES ('${user.id}', '\\x${hash}', ${indexof})`)
				}

				instance.reply(rand)
				return
			}
		}

		throw new Error('None of messages scored desired weight')
	}
}

export {MagicBallCommand}
