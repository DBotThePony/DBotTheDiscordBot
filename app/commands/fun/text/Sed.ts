

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../../CommandBase'

class SedCommand extends CommandBase {
	allow_piping = false
	display_help = false
	custom_prefix = '/'
	command_help = '/sed/find/replace'

	constructor() {
		super('sed')
	}

	is(input: string) {
		return input.startsWith('sed/')
	}

	executed(instance: CommandExecutionInstance) {
		const raw = instance.context.raw.substr(4).replace(/\/$/, '').split('/')

		if (raw.length == 1) {
			return 'Only replacement pattern were provided'
		} else if (raw.length != 2) {
			return 'Ambiguous amount of slashes'
		}

		raw[0] = raw[0].toLowerCase()
		const regexp = new RegExp(raw[0], 'i')
		const list = instance.channel!.messages.cache.last(15)
		list.sort((a, b) => a.createdTimestamp > b.createdTimestamp ? 1 : 0)

		for (const message of list) {
			if (message.author.id != this.bot.id && message.id != instance.context.msg!.id && message.content.match(regexp)) {
				return `**${(message.guild && message.member && message.member.nickname || message.author.username).trim().replace(/_/g, '\\_').replace(/`/g, '\\`').replace(/\*/g, '\\*')}**: ${message.content.replace(regexp, raw[1]).replace(/@everyone/g, '@ everyone').replace(/@here/, '@ here')}`
			}
		}

		return 'Unable to find matching message'
	}
}

export {SedCommand}
