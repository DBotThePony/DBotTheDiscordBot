

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../../CommandBase'
import {CommandHolder} from '../../CommandHolder'
import Discord = require('discord.js')

const charMap = [
	'q', 'w', 'e', 'r', 't', 'y', 'u',
	'i', 'o', 'p', 'a', 's', 'd', 'f',
	'g', 'h', 'j', 'k', 'l', 'z', 'x',
	'c', 'v', 'b', 'n', 'm', '1', '2',
	'3', '4', '5', '6', '7', '8', '9',
	'0', '[', ']', ';', '\'', '\\', ',',
	'.', '/', '?', '-', '=', '+', '!',
	'@', '#', '$', '%', '^', '&', '*',
	'(', ')', '~', '"', '{', '}', '<',
	'>', 'Q', 'W', 'E', 'R', 'T', 'Y',
	'U', 'U', 'I', 'O', 'P', 'A', 'S',
	'D', 'F', 'G', 'H', 'J', 'K', 'K',
	'L', 'Z', 'X', 'C', 'V', 'B', 'N',
	'M'
]

const charMapExp = [
	'q', 'w', 'e', 'r', 't', 'y', 'u',
	'i', 'o', 'p', 'a', 's', 'd', 'f',
	'g', 'h', 'j', 'k', 'l', 'z', 'x',
	'c', 'v', 'b', 'n', 'm', '1', '2',
	'3', '4', '5', '6', '7', '8', '9',
	'0', '\\[', '\\]', ';', '\'', '\\\\', ',',
	'\\.', '/', '\\?', '\\-', '=', '\\+', '!',
	'@', '\\#', '\\$', '%', '\\^', '\\&', '\\*',
	'\\(', '\\)', '~', '"', '\\{', '\\}', '\\﻿<',
	'﻿\\>', 'Q', 'W', 'E', 'R', 'T', 'Y',
	'U', 'U', 'I', 'O', 'P', 'A', 'S',
	'D', 'F', 'G', 'H', 'J', 'K', 'K',
	'L', 'Z', 'X', 'C', 'V', 'B', 'N',
	'M'
]

const charFullSpace = ['ｑ', 'ｗ', 'ｅ', 'ｒ', 'ｔ', 'ｙ', 'ｕ', 'ｉ', 'ｏ', 'ｐ', 'ａ', 'ｓ', 'ｄ', 'ｆ', 'ｇ', 'ｈ', 'ｊ', 'ｋ', 'ｌ', 'ｚ', 'ｘ', 'ｃ', 'ｖ', 'ｂ', 'ｎ', 'ｍ', '１', '２', '３', '４', '５', '６', '７', '８', '９', '０', '［', '］', '；', '＇', '＼', '，', '．', '／', '？', '－', '＝', '＋', '！', '＠', '＃', '＄', '％', '＾', '＆', '＊', '（', '）', '～', '｀', '﻿｛', '﻿｝', '﻿＜', '﻿＞', 'Ｑ', 'Ｗ', 'Ｅ', 'Ｒ', 'Ｔ', 'Ｙ', 'Ｕ', 'Ｕ', 'Ｉ', 'Ｏ', 'Ｐ', 'Ａ', 'Ｓ', 'Ｄ', 'Ｆ', 'Ｇ', 'Ｈ', 'Ｊ', 'Ｋ', 'Ｋ', 'Ｌ', 'Ｚ', 'Ｘ', 'Ｃ', 'Ｖ', 'Ｂ', 'Ｎ', 'Ｍ']

const flipMap = ['b', 'ʍ', 'ǝ', 'ɹ', 'ʇ', 'ʎ', 'n', 'ı', 'o', 'd', 'ɐ', 's', 'p', 'ɟ', 'ƃ', 'ɥ', 'ɾ', 'ʞ', 'ן', 'z', 'x', 'ɔ', 'ʌ', 'q', 'u', 'ɯ', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '[', ']', ';', ',', '\\', '‘', '.', '/', '¿', '-', '=', '+', '¡', '@', '#', '$', '%', '^', '⅋', '*', '(', ')', '~', '"', '{', '}', '<', '>', 'b', 'ʍ', 'ǝ', 'ɹ', 'ʇ', 'ʎ', 'n', 'n', 'ı', 'o', 'd', 'ɐ', 's', 'p', 'ɟ', 'ƃ', 'ɥ', 'ɾ', 'ʞ', 'ʞ', 'ן', 'z', 'x', 'ɔ', '𧐌', '', 'q', 'u', 'ɯ']
const flopMap = ['p', 'w', 'ɘ', 'ᴙ', 'T', 'Y', 'U', 'i', 'o', 'q', 'A', 'ꙅ', 'b', 'ꟻ', 'g', 'H', 'j', 'k', 'l', 'z', 'x', 'ↄ', 'v', 'd', 'ᴎ', 'm', '߁', '2', '3', '4', '5', '6', '7', '8', '9', '0', '[', ']', '⁏', '\'', '\\', ',', '.', '/', '⸮', '-', '=', '+', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '∽', '"', '{', '}', '<', '>', 'p', 'W', 'Ǝ', 'ᴙ', 'T', 'Y', 'U', 'U', 'I', 'O', 'ꟼ', 'A', 'Ꙅ', 'b', 'ꟻ', 'G', 'H', 'J', 'K', 'K', '⅃', 'Z', 'X', 'Ↄ', 'V', 'd', 'ᴎ', 'M']

interface StringMap {
	[key: string]: string
}

const mappedFull: StringMap = {}
const mappedFullInv: StringMap = {}
const mappedFlip: StringMap = {}
const mappedFlipInv: StringMap = {}
const mappedFlop: StringMap = {}
const mappedFlopInv: StringMap = {}

const matchMap = '(' + charMapExp.join('|') + ')'
const matchMapInv = '(' + charFullSpace.join('|') + ')'
const matchMapFlip = '(' + flipMap.join('|') + ')'
const matchMapFlop = '(' + flopMap.join('|') + ')'
const matchMapExp = new RegExp(matchMap, 'gi')
const matchMapInvExp = new RegExp(matchMapInv, 'gi')
const matchMapFlipExp = new RegExp(matchMapFlip.replace(/\*/g, '\\*').replace(/\?/g, '\\?').replace(/\+/g, '\\+'), 'gi')
const matchMapFlopExp = new RegExp(matchMapFlop.replace(/\*/g, '\\*').replace(/\?/g, '\\?').replace(/\+/g, '\\+'), 'gi')

for (let i in charMap) {
	mappedFull[charMap[i]] = charFullSpace[i]
	mappedFullInv[charFullSpace[i]] = charMap[i]
	mappedFlip[charMap[i]] = flipMap[i]
	mappedFlipInv[flipMap[i]] = charMap[i]
	mappedFlop[charMap[i]] = flopMap[i]
	mappedFlopInv[flopMap[i]] = charMap[i]
}

class Aesthetics extends CommandBase {
	command_help = 'A e s t h e t i c s'
	help_arguments = '<string>'

	constructor() {
		super('aesthetics', 'aes')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.get(1)) {
			instance.error('At least one word is required', 1)
			return
		}

		instance.reply(instance.raw.replace(matchMapExp, (m) => mappedFull[m] || m))
	}

	static unmap(input: string) {
		return input.replace(matchMapInvExp, (m) => mappedFullInv[m] || m)
	}
}

class TextFlip extends CommandBase {
	command_help = 'Flips?'
	help_arguments = '<string>'

	constructor() {
		super('tflip')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.get(1)) {
			instance.error('At least one word is required', 1)
			return
		}

		instance.reply(instance.raw.replace(matchMapExp, (m) => mappedFlip[m] || m))
	}

	static unmap(input: string) {
		return input.replace(matchMapFlipExp, (m) => {
			const getchar = mappedFlipInv[m]

			if (getchar == undefined || m.charCodeAt(0) < 128) {
				return m
			}

			return getchar
		})
	}
}

class TextFlop extends CommandBase {
	command_help = 'Flops?'
	help_arguments = '<string>'

	constructor() {
		super('tflop')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.get(1)) {
			instance.error('At least one word is required', 1)
			return
		}

		instance.reply(instance.raw.replace(matchMapExp, (m) => mappedFlop[m] || m))
	}

	static unmap(input: string) {
		return input.replace(matchMapFlopExp, (m) => {
			const getchar = mappedFlopInv[m]

			if (getchar == undefined || m.charCodeAt(0) < 128) {
				return m
			}

			return getchar
		})
	}
}

export {Aesthetics, TextFlip, TextFlop}
