

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance, ImageCommandBase} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')

class ShippingCommand extends ImageCommandBase {
	command_help = 'ship'
	parse_user_argument = true
	help_arguments = '<user1> <user2>'

	constructor() {
		super('ship')
	}

	executed(instance: CommandExecutionInstance) {
		if (!instance.getUser(1)) {
			return
		}

		if (!instance.getUser(2)) {
			return
		}

		if (instance.getUser(1) == instance.getUser(2)) {
			instance.error('forever alone', 2)
			return
		}

		let nickname1: string, nickname2: string
		const user1 = <Discord.User> instance.getUser(1)
		const user2 = <Discord.User> instance.getUser(2)

		if (instance.server) {
			nickname1 = instance.server.member(user1) && instance.server.member(user1)!.nickname || user1.username
			nickname2 = instance.server.member(user2) && instance.server.member(user2)!.nickname || user2.username
		} else {
			nickname1 = user1.username
			nickname2 = user2.username
		}

		const nick1Sub = Math.max(Math.floor(nickname1.length / 2), 4);
		const nick2Sub = Math.min(Math.floor(nickname2.length / 2), nick1Sub);
		const nick1Piece = nickname1.substr(0, nick1Sub);
		const nick2Piece = nickname2.substr(nick2Sub);

		const postShip = (imageBuffer?: Buffer | null) => {
			instance.query(`INSERT INTO "shipping" ("first", "second") VALUES (${user1.id}, ${user2.id}) ON CONFLICT ("first", "second") DO UPDATE SET "times" = "shipping"."times" + 1 RETURNING "times"`)
			.then((value) => {
				const shipText = `${instance.author} ships it (${value.rows[0].times} times now)\nShip name: **${nick1Piece}${nick2Piece}**`

				if (imageBuffer) {
					instance.send(shipText, new Discord.MessageAttachment(imageBuffer, 'ship.png'))
				} else {
					instance.send(shipText)
				}
			})
		}

		if (instance.hasPermissionBoth('ATTACH_FILES') && user1.avatarURL() && user2.avatarURL()) {
			instance.loadImage(user1.avatarURL()!)
			.then((avatar1) => {
				instance.loadImage(user2.avatarURL()!)
				.then((avatar2) => {
					this.convert(instance,
						'-background', 'transparent',
						'(', avatar1, '-resize', '256x256!', ')',
						'-density', '2048',
						'(', process.cwd() + '/resource/emoji/2665.svg', '-resize', '256x256!', ')',
						'(', avatar2, '-resize', '256x256!', ')',
						'+append', 'png:-')
					.then(imageBuffer => {
						postShip(imageBuffer)
					})
				})
			})

			return
		}

		postShip()
	}
}

export {ShippingCommand}
