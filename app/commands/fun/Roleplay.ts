

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase, CommandExecutionInstance} from '../CommandBase'
import {CommandHolder} from '../CommandHolder'
import Discord = require('discord.js')
import {sprintf} from 'sprintf-js'
import { CommandHelper } from '../../lib/CommandHelper';
import { BotInstance } from '../../BotInstance';

class UserWithUserCommand extends CommandBase {
	fstring: string
	parse_user_argument = true
	actionID: number | null = null
	help_arguments = '[target user = bot->you]'

	constructor(fstring: string, help?: string, ...commandNames: string[]) {
		super(...commandNames)
		this.fstring = fstring
		this.command_help = help || commandNames[0]
	}

	setHolder(holder: CommandHolder) {
		this.command_holder = holder
		if (!this.bot) {
			throw new Error('Invalid bot initialization')
		}

		this.bot.storage.nextActionID = (this.bot.storage.nextActionID || 0) + 1
		this.actionID = this.bot.storage.nextActionID
		return this
	}

	executed(instance: CommandExecutionInstance) {
		let actor = <Discord.User> instance.user
		let targetUser = <Discord.User> instance.next()

		if (!targetUser) {
			targetUser = actor
			actor = (<BotInstance> this.bot).client.user!
		}

		if (typeof targetUser != 'object') {
			if (!instance.getUser(1)) {
				return
			}

			targetUser = instance.getUser(1)!
		}

		if (targetUser.id == actor.id) {
			instance.error('what', 1)
			return
		}

		instance.query(`INSERT INTO "roleplay" VALUES ('${actor.id}', '${targetUser.id}', '${this.actionID}', '1') ON CONFLICT
		("actor", "target", "action") DO UPDATE SET "times" = "roleplay"."times" + 1 RETURNING "times"`).then((data) => {
			instance.query(`INSERT INTO "roleplay_generic" VALUES ('${actor.id}', '${this.actionID}', '1') ON CONFLICT
			("actor", "action") DO UPDATE SET "times" = "roleplay_generic"."times" + 1 RETURNING "times"`).then((data2) => {
				instance.say('_' + sprintf(this.fstring, `<@${actor.id}>`, `<@${targetUser.id}>`) + ` (${data && data.rows[0] && data.rows[0].times} times with <@${targetUser.id}>/${data2 && data2.rows[0] && data2.rows[0].times} times in total)_`)
			})
		})
	}
}

class UserWithEnvCommand extends CommandBase {
	fstringAlone: string
	fstringWith: string
	parse_user_argument = true
	actionID: number | null = null
	help_arguments = '[target user = none]'

	constructor(fstringAlone: string, fstringWith: string, help?: string, ...commandNames: string[]) {
		super(...commandNames)
		this.fstringAlone = fstringAlone
		this.fstringWith = fstringWith
		this.command_help = help || commandNames[0]
	}

	setHolder(holder: CommandHolder) {
		this.command_holder = holder
		if (!this.bot) {
			throw new Error('Invalid bot initialization')
		}

		this.bot.storage.nextActionID = (this.bot.storage.nextActionID || 0) + 1
		this.actionID = this.bot.storage.nextActionID
		return this
	}

	executed(instance: CommandExecutionInstance) {
		let actor = <Discord.User> instance.user
		let alone = false
		let targetUser = <Discord.User> instance.next()

		if (!targetUser) {
			alone = true
		}

		if (!alone && typeof targetUser != 'object') {
			if (!instance.getUser(1)) {
				return
			}

			targetUser = instance.getUser(1)!
		}

		if (!alone && targetUser.id == actor.id) {
			instance.error('what', 1)
			return
		}

		if (!alone) {
			instance.query(`INSERT INTO "roleplay" VALUES ('${actor.id}', '${targetUser.id}', '${this.actionID}', '1') ON CONFLICT
			("actor", "target", "action") DO UPDATE SET "times" = "roleplay"."times" + 1 RETURNING "times"`).then((data) => {
				instance.say('_' + sprintf(this.fstringWith, `<@${actor.id}>`, `<@${targetUser.id}>`) + ` (${data && data.rows[0] && data.rows[0].times} times with <@${targetUser.id}>)_`)
			})
		} else {
			instance.query(`INSERT INTO "roleplay_generic" VALUES ('${actor.id}', '${this.actionID}', '1') ON CONFLICT
			("actor", "action") DO UPDATE SET "times" = "roleplay_generic"."times" + 1 RETURNING "times"`).then((data) => {
				instance.say('_' + sprintf(this.fstringAlone, `<@${actor.id}>`) + ` (${data && data.rows[0] && data.rows[0].times} times alone)_`)
			})
		}
	}
}

const registerRoleplayCommands = function(holder: CommandHolder) {
	holder.registerCommand(new UserWithUserCommand('%s hugs %s', 'hugs? ^w^', 'hug'))
	holder.registerCommand(new UserWithUserCommand('%s pokes %s', 'Pokes', 'poke'))
	holder.registerCommand(new UserWithUserCommand('%s punches %s', 'PUNCH', 'punch'))
	holder.registerCommand(new UserWithUserCommand('%s hugs tight %s', 'huggy', 'squeeze'))
	holder.registerCommand(new UserWithUserCommand('%s cuddles %s', 'Cuddly', 'cuddle'))
	holder.registerCommand(new UserWithUserCommand('%s snuggles %s', 'snuggly pillow', 'snuggle'))
	holder.registerCommand(new UserWithUserCommand('%s rubs %s', 'rubby', 'rub'))
	holder.registerCommand(new UserWithUserCommand('%s slowly strokes %s', 'heartstrokes', 'stroke'))
	holder.registerCommand(new UserWithUserCommand('%s slaps %s', 'ouch', 'slap'))
	holder.registerCommand(new UserWithUserCommand('%s boops nosey of %s', 'got your nosey', 'boop'))
	holder.registerCommand(new UserWithUserCommand('%s nose licks %s', 'mmmm', 'lick'))
	holder.registerCommand(new UserWithUserCommand('%s brushes mane of %s', 'oooooh', 'brush'))
	holder.registerCommand(new UserWithUserCommand('%s softly bites ears of %s', 'mmmmm!', 'earnom'))
	holder.registerCommand(new UserWithUserCommand('%s CHOKESLAMS %s', 'ded', 'chokeslam'))
	holder.registerCommand(new UserWithUserCommand('%s sniffs %s', 'sniff', 'sniffs', 'why would you do this'))
	holder.registerCommand(new UserWithUserCommand('%s mane noms %s', 'manenom', 'manebite', 'heh nommy'))
	holder.registerCommand(new UserWithUserCommand('%s nose noms %s', 'nom', 'nosenom', 'not what you think'))
	// holder.registerCommand(new UserWithUserCommand('r00d', '%s fukks %s', 'fuck'))
	holder.bot.storage.nextActionID++

	holder.registerCommand(new UserWithUserCommand('%s pets %s', 'oww', 'pet'))
	holder.registerCommand(new UserWithUserCommand('%s makes fluffesh %s', 'more fluff!', 'fluff'))
	holder.registerCommand(new UserWithUserCommand('%s boxes %s', 'pone boxing', 'box'))
	holder.registerCommand(new UserWithUserCommand('%s hoof nom of %s', 'hmmm?', 'hoofnom'))
	holder.registerCommand(new UserWithUserCommand('%s softly chews tail of %s', 'that tickles!', 'tailnom'))
	holder.registerCommand(new UserWithUserCommand('%s makes lewdish %s', '>w<', 'lewd'))
	holder.registerCommand(new UserWithUserCommand('%s gives bread to %s', 'baguette', 'bread'))

	holder.registerCommand(new UserWithEnvCommand('%s sits', '%s sits on %s', 'sitty', 'sit'))
	holder.registerCommand(new UserWithEnvCommand('%s jumps around', '%s jumps on %s', 'Jumps', 'jump'))
	holder.registerCommand(new UserWithEnvCommand('%s sleeps on grass', '%s sleep on %s', 'Sleeeeeepy', 'sleep'))
	holder.registerCommand(new UserWithEnvCommand('%s lays on pillows', '%s lay on %s', 'not sleepy', 'lay'))

	holder.registerCommand(new UserWithUserCommand('%s pats %s', 'oww', 'pat'))
}

export {registerRoleplayCommands}
