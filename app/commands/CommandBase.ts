

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandContext, CommandFlags, parseRole, parseChannel, parseUser} from './CommandContext'
import {CommandHolder} from './CommandHolder'
import {InvalidStateException} from '../../lib/Error'
import Discord = require('discord.js')

const strcmp = (strIn: string, strWith: string): [number, number, number] => {
	let chars = 0
	let start = -1
	let ends = -1

	if (strIn.length > strWith.length) {
		return [chars, start, ends]
	}

	for (let i = 0; i < strWith.length; i++) {
		if (strIn[0] == strWith[i]) {
			start = i
			break
		}
	}

	if (start == -1) {
		return [chars, start, ends]
	}

	for (let i = 0; i < strIn.length; i++) {
		if (strIn[i] == strWith[i + start]) {
			chars++
		} else if (start == 0 && strIn[i] != ' ') {
			// non floating space inside comprasion word mismatch is not
			// allowed if compared-to expression literally started with this word
			// e.g. @dbot should not match @dbotthepone
			// but @eledery should match @(•._.• eledery •._.•)
			return [0, -1, -1]
		} else {
			ends = i + start
			break
		}
	}

	// guess we got no match here if match is too short
	if (chars < strIn.length / 2) {
		return [0, -1, -1]
	}

	return [chars, start, start + strIn.length]
}

class CommandExecutionInstance {
	typingStatus = false
	wasTyping = false
	isMessageSent = false
	isFlushed = false
	hasErrors = false
	isValidPipe = false
	isPiped = false

	current_argument_index = 0
	pipe_arguments: any[] | null = null
	pipe_command: CommandBase | null = null

	get uid() { return this.context.bot.uid }
	get id() { return this.context.author && this.context.author.id }
	get bot() { return this.context.bot }
	get sql() { return this.context.bot.sql }
	get commands() { return this.context.bot.command_holder }
	get isInServer() { return this.context.isInServer }
	get isOwner() { return this.context.isOwner }
	get author() { return this.context.author }
	get user() { return this.context.author }
	get sender() { return this.context.author }
	get member() { return this.context.member }
	get helper() { return this.bot.command_helper }
	get channel() { return this.context.channel }
	get server() { return this.context.server }
	get length() { return this.context.args.length - 1 }
	get raw() { return this.context.rawArgs }
	get isDM() { return this.context.msg && this.context.msg.channel.type == 'dm' }
	get areMessagesAllowed() { return this.context.areMessagesAllowed }
	get tag() { return this.context.user?.tag }

	get parsedArguments() { return this.pipe_arguments || this.context.parsedArgs }
	get isPipingAllowed() { return this.context.allow_piping }

	hasArguments() { return this.context.hasArguments() }

	constructor(public command: CommandBase, public context: CommandContext, public pipeid = 0) {
		if (this.isPipingAllowed) {
			const getPipe = context.getPipe(pipeid)

			if (getPipe) {
				this.pipe_command = this.bot.command_holder.findCommand(getPipe.toLowerCase())
			}
		}

		this.thinking(true)
	}

	static noChannelPerms = [
		'CREATE_INSTANT_INVITE',
		'KICK_MEMBERS',
		'BAN_MEMBERS',
		'MANAGE_CHANNELS',
		'MANAGE_GUILD',
		'CONNECT',
		'SPEAK',
		'CHANGE_NICKNAME',
		'MANAGE_NICKNAMES',
		'MANAGE_ROLES',
		'MANAGE_ROLES_OR_PERMISSIONS',
		'MANAGE_EMOJIS',
	]

	hasPermission(permission: Discord.PermissionResolvable | Discord.PermissionResolvable[]) {
		if (this.isDM) {
			return true // maybe
		}

		if (this.server && this.server.me?.hasPermission('ADMINISTRATOR')) {
			return true
		}

		if (this.server && this.server.me && typeof permission == 'string' && CommandExecutionInstance.noChannelPerms.includes(permission)) {
			return this.server.me.hasPermission(permission)
		}

		if (this.channel && this.server) {
			const perms = (<Discord.TextChannel> this.channel).permissionsFor(this.server.me!)

			if (perms) {
				return perms.has(permission)
			} else {
				return this.server.me!.hasPermission(permission)
			}
		}

		return false
	}

	hasPermissionExecutor(permission: Discord.PermissionResolvable | Discord.PermissionResolvable[]) {
		if (this.isDM) {
			return true // maybe
		}

		if (this.isOwner) {
			return true
		}

		if (this.member && this.member.hasPermission('ADMINISTRATOR')) {
			return true
		}

		if (this.member && typeof permission == 'string' && CommandExecutionInstance.noChannelPerms.includes(permission)) {
			return this.member.hasPermission(permission)
		}

		if (this.channel && this.server && this.member) {
			const perms = (<Discord.TextChannel> this.channel).permissionsFor(this.member)

			if (perms) {
				return perms.has(permission)
			} else {
				return this.member.hasPermission(permission)
			}
		}

		return false
	}

	hasPermissionBoth(permission: Discord.PermissionResolvable | Discord.PermissionResolvable[]) {
		if (this.isDM) {
			return true // maybe
		}

		if (this.server && this.server.me!.hasPermission('ADMINISTRATOR') && (this.isOwner || this.member && this.member.hasPermission('ADMINISTRATOR'))) {
			return true
		}

		if (this.member && this.server && this.server.me && typeof permission == 'string' && CommandExecutionInstance.noChannelPerms.includes(permission)) {
			return this.member.hasPermission(permission) && this.server.me.hasPermission(permission)
		}

		if (this.channel && this.server && this.member) {
			const perms = (<Discord.TextChannel> this.channel).permissionsFor(this.server.me!)
			const perms2 = (<Discord.TextChannel> this.channel).permissionsFor(this.member)

			if (perms) {
				return perms.has(permission) && perms2!.has(permission)
			} else {
				return this.server.me!.hasPermission(permission) && this.member.hasPermission(permission)
			}
		}

		return false
	}

	buildError(message: string, argNum: number) {
		let buildString = 'Error - ' + message + '\n```' + this.command.id + ' '
		let spacesLen = this.command.id.length
		let uppersLen = 0

		for (let i = 1; i <= Math.max(argNum, this.parsedArguments.length - 1); i++) {
			const arg = String(this.parsedArguments[i] != undefined ? this.parsedArguments[i] : '<missing>')
			buildString += arg + ' '

			if (i < argNum) {
				spacesLen += arg.length + 1
			} else if (i == argNum) {
				uppersLen = arg.length
			}
		}

		buildString += '\n' + ' '.repeat(spacesLen) + ' ' + '^'.repeat(uppersLen) + '```\n' +
			'Help:\n```' + this.command.id + ' ' + this.command.help_arguments + (this.command.hasHelp() && ('\n' + this.command.command_help) || '') + '```'

		return buildString
	}

	rawInput(from = 0) {
		return this.context.rawArgsFrom(from + 1)
	}

	error(message: string, argNum: number) {
		this.reply(this.buildError(message, argNum))
	}

	thinking(status = false) {
		if (this.typingStatus != status) {
			if (this.isFlushed) { this.context.typing(status) }
			this.wasTyping = true
		}

		this.typingStatus = status
	}

	findImage(arg: any): string | null {
		if (typeof arg == 'object' && arg instanceof Discord.User) {
			return arg.avatarURL()
		}

		if (typeof arg == 'string' && arg.trim() != '') {
			return this.bot.command_helper.findImageString(arg)
		}

		if (this.channel) {
			return this.bot.command_helper.findImage(this.channel, arg) || null
		}

		return null
	}

	loadImage(urlIn: string) {
		const promise = this.bot.command_helper.getImagePath(urlIn)

		promise.catch((err: string) => {
			this.reply('Image `' + urlIn + '` download failed: ```\n' + err + '\n```')
		})

		return promise
	}

	loadBufferImage(urlIn: string) {
		const promise = this.bot.command_helper.getImageContents(urlIn)

		promise.catch((err: string) => {
			this.send('Image download failed: ' + err)
		})

		return promise
	}

	argument(num: number) {
		return this.parsedArguments[num]
	}

	destroy() {
		this.flush()

		if (this.typingStatus) {
			this.thinking(false)
		}
	}

	flush() {
		this.isFlushed = true

		if (this.isMessageSent) {
			if (this.typingStatus) {
				this.context.typing(false)
			}

			return
		}

		if (this.typingStatus) {
			this.context.typing(true)
			return
		}

		if (!this.wasTyping) {
			this.thinking(true)
		}
	}

	send(content: string, attach?: Discord.MessageOptions | Discord.MessageAttachment): Promise<Discord.Message | Discord.Message[]> | null {
		if (this.hasErrors) {
			return null
		}

		if (this.isPipingAllowed && this.pipe_command && !attach) {
			const argsPrev = []
			content = content.replace(/```/g, '')

			if (this.id) {
				content = content.replace('<@' + this.id + '>, ', '')
			}

			const argsNextRaw = ParseString(content)

			for (const level of argsNextRaw) {
				for (const argument of level) {
					argsPrev.push(argument)
				}
			}

			this.isMessageSent = true
			this.context.pipe(this.pipeid, argsPrev, content)

			if (this.typingStatus) {
				this.thinking(false)
			}

			this.pipe_command.execute(this.context)

			return null
		}

		if (content.length > 1900 && !attach) {
			if (this.hasPermission('ATTACH_FILES')) {
				const promise = this.context.send('', new Discord.MessageAttachment(Buffer.from(content), this.command.id + '.txt'))

				if (this.typingStatus) {
					this.thinking(false)
				}

				this.isMessageSent = true

				return promise
			} else if (this.author) {
				const promise = this.author.send('', new Discord.MessageAttachment(Buffer.from(content), this.command.id + '.txt'))

				if (this.typingStatus) {
					this.thinking(false)
				}

				this.isMessageSent = true

				return promise
			} else {
				if (this.typingStatus) {
					this.thinking(false)
				}

				return null
			}
		}

		if (attach && !this.hasPermissionBoth('ATTACH_FILES')) {
			if (!this.author) {
				if (this.typingStatus) {
					this.thinking(false)
				}

				return null
			}

			const promise = this.author.send(content, attach)

			if (!promise) {
				return null
			}

			if (this.typingStatus) {
				this.thinking(false)
			}

			this.isMessageSent = true

			return promise
		}

		const promise = this.context.send(content, attach)

		if (!promise) {
			return null
		}

		if (this.typingStatus) {
			this.thinking(false)
		}

		this.isMessageSent = true

		return promise
	}

	say(content: string, attach?: Discord.MessageAttachment | Discord.MessageOptions) {
		return this.send(content, attach)
	}

	reply(content: string, attach?: Discord.MessageAttachment | Discord.MessageOptions) {
		if (!this.areMessagesAllowed || attach && !this.hasPermissionBoth('ATTACH_FILES')) {
			return this.send(content, attach)
		}

		return this.isDM ? this.send(content, attach) : this.send('<@' + this.id + '>, ' + content, attach)
	}

	sqlError(err: Error) {
		console.error(`User ${this.context.author && this.context.author.username || '???'}<${this.context.author && this.context.author.id || '???'}> executed command ${this.command.id} and it errored:`)
		console.error(err)
		this.reply('SQL Execution error: ' + err)
	}

	sendPM(content: string) {
		if (this.hasErrors) {
			return false
		}

		if (!this.context.user) {
			return false
		}

		this.context.user.send(content)

		if (this.typingStatus) {
			this.thinking(false)
		}

		return true
	}

	next() {
		this.current_argument_index++
		return this.parsedArguments[this.current_argument_index]
	}

	get(argNum: number) {
		return this.parsedArguments[argNum]
	}

	getString(argNum: number) {
		return this.parsedArguments[argNum] != undefined ? String(this.parsedArguments[argNum]) : undefined;
	}

	memberGetCache: (Discord.GuildMember | null)[] = []

	getMember(argNum: number, reportErrors = true) {
		if (this.memberGetCache[argNum] != undefined) {
			return this.memberGetCache[argNum]
		}

		const member = this._getMember(argNum, reportErrors)
		this.memberGetCache[argNum] = member
		return member
	}

	private _getMember(argNum: number, reportErrors = true) {
		if (this.parsedArguments[argNum] instanceof Discord.GuildMember) {
			return <Discord.GuildMember> this.parsedArguments[argNum]
		}

		if (!this.server) {
			return null
		}

		if (this.parsedArguments[argNum] instanceof Discord.User) {
			const user = <Discord.User> this.parsedArguments[argNum]
			const member = this.server.members.cache.get(user.id)

			if (!member) {
				if (reportErrors) {
					this.error('User exists but is not present on this server', argNum)
				}

				return null
			}

			return member
		}

		if (typeof this.parsedArguments[argNum] != 'string') {
			if (reportErrors) {
				this.error('Invalid object for member', argNum)
			}

			return null
		}

		const nickname = <string> this.parsedArguments[argNum].toLowerCase()

		if (nickname.length < 3) {
			if (reportErrors) {
				this.error('Nickname is too short', argNum)
			}

			return null
		}

		let max = -1
		let best = null
		let matchesInRow = 0

		for (const member of this.server.members.cache.values()) {
			if (member.nickname) {
				const [chars, start, ends] = strcmp(nickname, member.nickname.toLowerCase())

				if (chars != 0 && max < chars) {
					best = member
					max = chars
					matchesInRow = 0
					continue
				} else if (chars != 0 && max == chars && member.id != best!.id) {
					matchesInRow++
				}
			}

			const [chars, start, ends] = strcmp(nickname, member.user.username.toLowerCase())

			if (chars != 0 && max < chars) {
				best = member
				max = chars
				matchesInRow = 0
			} else if (chars != 0 && max == chars && member.id != best!.id) {
				matchesInRow++
			}
		}

		if (matchesInRow > 0) {
			if (reportErrors) {
				this.error(`Ambiguous username reference, please use longer nickname or @ping user directly`, argNum)
			}

			return null
		}

		if (!best && reportErrors) {
			this.error('Unable to find specified member', argNum)
		}

		return best
	}

	userMatchCache: (Discord.User | null)[] = []

	getUser(argNum: number, reportErrors = true) {
		if (this.userMatchCache[argNum] != undefined) {
			if (this.userMatchCache[argNum] == null && reportErrors) {
				this.error('Cached: Unable to find specified user', argNum)
			}

			return this.userMatchCache[argNum]
		}

		const user = this._getUser(argNum, reportErrors)
		this.userMatchCache[argNum] = user
		return user
	}

	getUserID(argNum: number, reportErrors = true) {
		const user = this.getUser(argNum, false)

		if (user == null) {
			const arg = this.parsedArguments[argNum]

			if (arg == undefined) {
				if (reportErrors) {
					this.error('Provided argument is not resolvable to an user', argNum)
				}

				return null
			}

			const parse = arg.match(parseUser)

			if (parse && parse[1]) {
				return <string> parse[1]
			}

			if (arg.match(/^[0-9]+$/)) {
				return <string> arg
			}

			if (reportErrors) {
				this.error('Provided argument is not resolvable to an user', argNum)
			}

			return null
		}

		return user.id
	}

	private _getUser(argNum: number, reportErrors = true) {
		if (this.parsedArguments[argNum] instanceof Discord.GuildMember) {
			return <Discord.User> this.parsedArguments[argNum].user
		}

		if (this.parsedArguments[argNum] instanceof Discord.User) {
			return <Discord.User> this.parsedArguments[argNum]
		}

		if (this.server) {
			const member = this.getMember(argNum, false)

			if (member) {
				return member.user
			}
		}

		if (typeof this.parsedArguments[argNum] != 'string') {
			if (reportErrors) {
				this.error('Invalid object for user', argNum)
			}

			return null
		}

		const nickname = <string> this.parsedArguments[argNum].toLowerCase()

		if (nickname.length < 3) {
			if (reportErrors) {
				this.error('Nickname is too short', argNum)
			}

			return null
		}

		let max = -1
		let best = null
		let matchesInRow = 0
		let matchesCache: string[] = []

		if (this.server) {
			for (const member of this.server.members.cache.values()) {
				const user = member.user

				if (member.nickname) {
					const [chars, start, ends] = strcmp(nickname, member.nickname.toLowerCase())

					if (chars != 0 && max < chars) {
						best = user
						max = chars

						if (matchesInRow != 0)
							matchesCache = []

						matchesInRow = 0
						continue
					} else if (chars != 0 && max == chars && user.id != best!.id) {
						if (!matchesCache.includes(user.id)) {
							matchesCache.push(user.id)
							matchesInRow++
						}

						continue
					}
				}

				{
					const [chars, start, ends] = strcmp(nickname, user.username.toLowerCase())

					if (chars != 0 && max < chars) {
						best = user
						max = chars

						if (matchesInRow != 0)
							matchesCache = []

						matchesInRow = 0
						continue
					} else if (chars != 0 && max == chars && user.id != best!.id) {
						if (!matchesCache.includes(user.id)) {
							matchesCache.push(user.id)
							matchesInRow++
						}

						continue
					}
				}
			}
		}

		if (!best) {
			max = -1
			best = null
			matchesInRow = 0
			matchesCache = []

			for (const user of this.bot.client.users.cache.values()) {
				const [chars, start, ends] = strcmp(nickname, user.username.toLowerCase())

				if (chars != 0 && max < chars) {
					best = user
					max = chars

					if (matchesInRow != 0)
						matchesCache = []

					matchesInRow = 0
				} else if (chars != 0 && max == chars && user.id != best!.id) {
					if (!matchesCache.includes(user.id)) {
						matchesCache.push(user.id)
						matchesInRow++
					}
				}
			}
		}

		if (matchesInRow > 0) {
			if (reportErrors) {
				this.error(`Ambiguous username reference, please use longer nicknames or @ping user directly (${matchesInRow} matches)`, argNum)
			}

			return null
		}

		if (!best && reportErrors) {
			this.error('Unable to find specified user', argNum)
		}

		return best
	}

	from(argNum: number): any[] {
		if (!this.has(argNum)) {
			return []
		}

		const output: string[] = []

		for (let i = argNum; i < this.parsedArguments.length; i++) {
			output.push(this.parsedArguments[i])
		}

		return output
	}

	has(argNum: number) {
		return this.parsedArguments[argNum] != undefined && this.parsedArguments[argNum] != null
	}

	selectUser(slotIn = 1, ifNone: Discord.User | null = this.author, strict = true): Discord.User | null {
		if (!this.command.parse_user_argument) {
			throw new InvalidStateException('Command do not accept users', 'allowUsers', true, this.command.parse_user_argument)
		}

		if (!this.has(slotIn)) {
			return ifNone
		}

		if (strict && !this.getUser(slotIn)) {
			return null
		}

		return this.getUser(slotIn)
	}

	assert(argNum: number, reason?: string) {
		if (!this.has(argNum)) {
			this.error(reason || 'Missing argument at ' + argNum, argNum)
			return false
		}

		return true
	}

	reset() {
		this.current_argument_index = 0
		return this
	}

	findRole(name: string) {
		if (!this.server) {
			return null
		}

		let role

		if (role = name.match(parseRole)) {
			return this.server.roles.cache.get(role[1]) || null
		}

		name = name.toLowerCase()

		for (const role of this.server.roles.cache.values()) {
			if (role.name.toLowerCase() == name) {
				return role
			}
		}

		for (const role of this.server.roles.cache.values()) {
			const lc = role.name.toLowerCase()

			if (lc.startsWith(name)) {
				return role
			}

			if (lc.match(name)) {
				return role
			}
		}

		return null
	}

	findChannel(name: any, type: 'dm' | 'group' | 'text' | 'voice' | 'category' | null = null) {
		if (!this.server) {
			return null
		}

		if (typeof name !== 'string') {
			if (name instanceof Discord.GuildChannel) {
				return name
			}

			return null
		}

		let channel

		if (channel = name.match(parseChannel)) {
			return this.server.channels.cache.get(channel[1]) || null
		}

		name = name.toLowerCase()

		if (name.startsWith('#')) {
			name = name.substr(1)
		}

		for (const channel of this.server.channels.cache.values()) {
			if (channel.name.toLowerCase() == name && (type == null || channel.type == type)) {
				return channel
			}
		}

		for (const channel of this.server.channels.cache.values()) {
			const lc = channel.name.toLowerCase()

			if (lc.match(name) && (type == null || channel.type == type)) {
				return channel
			}

			if (lc.startsWith(name) && (type == null || channel.type == type)) {
				return channel
			}
		}

		return null
	}

	checkTargeting(memberTarget: Discord.GuildMember, checkSelf = true) {
		const immunityself = this.bot.command_helper.getImmunityLevel(this.member!)
		const immunityme = this.bot.command_helper.getImmunityLevel(this.server!.me!)
		const immunitythem = this.bot.command_helper.getImmunityLevel(memberTarget)

		if (this.isOwner && memberTarget.id == this.bot.client.user!.id) {
			return true
		}

		if (this.member!.id == this.bot.id && this.bot.config.owners.includes(memberTarget.id)) {
			return false
		}

		if (!this.isOwner && memberTarget.id == this.bot.id) {
			return false
		}

		if (this.isOwner) {
			return immunityme > immunitythem
		}

		if (checkSelf) {
			return immunityself > immunitythem && immunityme > immunitythem
		}

		return immunityself > immunitythem
	}

	*[Symbol.iterator] () {
		for (let i = 1; i < this.parsedArguments.length; i++) {
			yield [i, this.parsedArguments[i]]
		}
	}

	query(query: string) {
		if (!this.bot || !this.bot.sql) {
			throw new Error('No bot or no database avaliable')
		}

		const promise = this.bot.sql.query(query)

		promise.catch((err) => {
			this.send('```sql\nSQL Execution error - ' + err + '```')
			console.error(this.command.id + ' sql query errored: ' + err)
			this.hasErrors = true
		})

		return promise
	}
}

class CommandBase implements CommandFlags {
	command_names: string[]
	command_help = ''
	help_arguments = ''
	execute_incremental = 0
	command_holder!: CommandHolder
	display_help = true

	get id() { return this.command_names[0] }

	parse_user_argument = false
	parse_member_argument = false
	parse_role_argument = false
	parse_channel_argument = false
	allow_piping = true
	is_pipe_command = false
	allow_in_dm = true
	only_in_dm = false
	can_be_banned = true
	remember_command_context = true

	custom_prefix: null | string = null

	get bot() { return this.command_holder.bot }
	get sql() { return this.command_holder.bot.sql }
	get client() { return this.command_holder.bot.client }

	toString() {
		return '[object CommandBase[' + this.command_names.join(', ') + ']]'
	}

	constructor(...id: string[]) {
		this.command_names = id
	}

	is(input: string) {
		return this.id == input || this.command_names.includes(input)
	}

	addName(...aliases: string[]) {
		for (const obj of aliases) {
			this.command_names.push(obj)
		}
	}

	setHolder(holder: CommandHolder) {
		this.command_holder = holder
		this.setupBot(holder.bot)
		return this
	}

	setupBot(bot: BotInstance) {
		// override
	}

	getAntispamWeight(user: Discord.User, msg: Discord.Message) {
		return 1
	}

	hasHelp() {
		return this.command_help != ''
	}

	getArgumentsString() {
		if (this.hasHelpArguments()) {
			return this.help_arguments
		} else {
			return ''
		}
	}

	hasHelpArguments() {
		return this.help_arguments != ''
	}

	hasAlias() {
		return this.command_names && this.command_names.length > 0
	}

	async execute(context: CommandContext) {
		const instance = new CommandExecutionInstance(this, context, context.nextpipeid)

		if (this.only_in_dm && !instance.isDM) {
			instance.reply('This command can be only executed in Direct messaging channel (PM/Private Messaging)')
			return false
		}

		if (!this.allow_in_dm && instance.isDM) {
			instance.reply('This command can not be executed in Direct messaging channel (PM/Private Messaging)')
			return false
		}

		try {
			if (context.guild != null && context.member == null) {
				if (!context.userid) {
					// whut
					throw new TypeError('User were never provided for passed message inside CommandContext')
				}

				await context.guild.members.fetch(context.userid)
				context.grabFieldsFrom(context.msg!)
				context.member = context.guild.member(context.userid)

				if (!context.member) {
					// вы чо там совсем поехали
					throw new TypeError('Member was never fetched by Discord.JS')
				}
			}
		} catch(err) {
			if (err instanceof Error) {
				instance.send('Discord.JS error:\n```js\n' + err.stack + '```')
			} else {
				instance.send('Discord.JS error:\n```\n' + String(err) + '```')
			}
		}

		this.execute_incremental++

		try {
			this.bot.triggerActivity()
			const status = this.executed(instance)

			if (typeof status == 'string') {
				if (status == '') {
					throw new TypeError('Can not send empty message')
				}

				instance.send(status)
			} else if (typeof status == 'boolean') {
				return status
			} else if (typeof status == 'object' && status instanceof Promise) {
				try {
					const status2 = await status

					if (typeof status2 == 'string') {
						if (status2 == '') {
							throw new TypeError('Can not send empty message')
						}

						instance.send(status2)
					} else if (typeof status2 == 'boolean') {
						return status2
					}
				} catch(err) {
					instance.flush()
					throw err
				}

				instance.flush()
			} else {
				instance.flush()
			}
		} catch(err) {
			if (err instanceof Error) {
				instance.send('```js\n' + err.stack + '```')
			} else {
				instance.send('```\n' + String(err) + '```')
			}

			this.bot.error('User ' + context.author + ' executed ' + context.getCommand() + ' and it errored:')
			this.bot.error(err)
			instance.destroy()
		}

		return true
	}

	// override
	executed(instance: CommandExecutionInstance): boolean | string | Promise<string | null | void> | void {
		throw new Error('Not implemented')
	}
}

import child_process = require('child_process')
import { BotInstance } from '../BotInstance';
import { ImageIdentify } from '../../lib/imagemagick/Identify';
import { ParseString } from '../../lib/StringUtil';
const spawn = child_process.spawn

const reconstructBuffer = (buffers: Buffer[]) => {
	let bufferSize = 0

	for (const buffer of buffers) {
		bufferSize += buffer.byteLength
	}

	const newBuffer = Buffer.alloc(bufferSize)
	let offset = 0

	// the fuck node? I want .extend(Buffer) or .append(Buffer | BytesArray) or even better .write(Buffer, offset, length)
	for (const buffer of buffers) {
		for (let i = 0; i < buffer.byteLength; i++) {
			newBuffer[offset + i] = buffer[i]
		}

		offset += buffer.byteLength
	}

	return newBuffer
}

class ImageCommandBase extends CommandBase {
	allow_piping = false

	convertInternal(...args: string[]): Promise<Buffer> {
		return new Promise((resolve, reject) => {
			const magick = spawn('magick', ['convert', ...args])

			let buffers: Buffer[] = []
			let buffersErr: Buffer[] = []

			magick.on('close', (code: number, signal: string) => {
				if (code != 0) {
					if (buffersErr.length == 0) {
						reject('Image magick exited with non zero code! (' + code + ')')
					} else {
						reject(reconstructBuffer(buffersErr).toString('utf8'))
					}

					return
				}

				if (buffers.length == 0) {
					reject('Image magick did not gave the picture output')
					return
				}

				resolve(reconstructBuffer(buffers))
			})

			magick.stdout.on('data', (chunk: Buffer) => {
				buffers.push(chunk)
			})

			magick.stderr.on('data', (chunk: Buffer) => {
				buffersErr.push(chunk)
			})

			magick.stderr.pipe(process.stderr)
		})
	}

	convertInOutInternal(bufferIn: Buffer, ...args: string[]): Promise<Buffer> {
		return new Promise((resolve, reject) => {
			const magick = spawn('magick', ['convert', ...args])

			let buffers: Buffer[] = []

			magick.on('close', (code: number, signal: string) => {
				if (code != 0) {
					reject('Image magick exited with non zero code! (' + code + ')')
					return
				}

				if (buffers.length == 0) {
					reject('Image magick did not gave the picture output')
					return
				}

				resolve(reconstructBuffer(buffers))
			})

			magick.stdout.on('data', (chunk: Buffer) => {
				buffers.push(chunk)
			})

			magick.stderr.pipe(process.stderr)
			magick.stdin.end(bufferIn)
		})
	}

	convert(instance: CommandExecutionInstance, ...args: string[]): Promise<Buffer> {
		const promise = this.convertInternal(...args)

		promise.catch((err) => {
			instance.reply(err)
		})

		return promise
	}

	identify(instance: CommandExecutionInstance, pathToFile: string): Promise<ImageIdentify> {
		const promise = new ImageIdentify(pathToFile).identify()

		promise.catch((err) => {
			instance.reply('```\n' + err + '\n```')
		})

		return promise
	}

	convertInOut(instance: CommandExecutionInstance, bufferIn: Buffer, ...args: string[]): Promise<Buffer | null> {
		const promise = this.convertInOutInternal(bufferIn, ...args)

		promise.catch((err) => {
			instance.reply(err)
		})

		return promise
	}

	escapeLiterals(textIn: string) {
		return textIn.replace(/\\/gi, '\\\\').replace(/"/, '\\"').replace(/%/g, '%%').replace(/@/g, '\\@')
	}

	escapeText(textIn: string) {
		return `"${this.escapeLiterals(textIn)}"`
	}

	formatDrawText(lines: string) {
		const split = lines.split(/\r?\n/)
		const magikArgs: string[] = ['(']

		for (const line of split) {
			magikArgs.push(
				'(',
				'canvas:transparent',
				'caption:' + line,
				')'
			)
		}

		magikArgs.push('-append', ')')
		return magikArgs
	}
}

// todo: RegularMultiImageCommandBase (when needed)
class RegularImageCommandBase extends ImageCommandBase {
	forceClamp = true
	clampMinWidth = 512
	clampMinHeight = 512
	clampMaxWidth = 2048
	clampMaxHeight = 2048
	allowWildRatio = false
	onlyStatic = true

	doImage(instance: CommandExecutionInstance, identify: ImageIdentify, w?: number, h?: number) {

	}

	executed(instance: CommandExecutionInstance) {
		let image = instance.findImage(instance.from(1).join(' '))

		if (!image) {
			const user = instance.getUser(1)

			if (user) {
				image = user.avatarURL()

				if (!image) {
					instance.error('Invalid image provided', 1)
					return
				}
			} else {
				return
			}
		}

		instance.loadImage(image)
		.then((path) => {
			this.identify(instance, path)
			.then((value) => {
				if (this.onlyStatic && !value.isStatic) {
					instance.reply('Image is not a static image!')
					return
				}

				if (!this.allowWildRatio && value.wildAspectRatio) {
					instance.reply('Image has wild aspect ratio')
					return
				}

				if (this.forceClamp) {
					const [w, h] = value.clamp(this.clampMinWidth, this.clampMinHeight, this.clampMaxWidth, this.clampMaxHeight)

					if (!w && !h) {
						instance.reply('Image has wild aspect ratio')
						return
					}

					this.doImage(instance, value, w!, h!)
				} else {
					this.doImage(instance, value)
				}
			})
		})
	}
}

export {CommandBase, ImageCommandBase, CommandExecutionInstance, RegularImageCommandBase}
