

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {CommandBase} from './CommandBase'
import {CommandContext} from './CommandContext'
import {BotInstance} from '../BotInstance'
import Discord = require('discord.js')
import { ServerCommandsState } from './CommandBansController'

class CommandHolder {
	registeredCommands = new Map<string, CommandBase>()
	registeredCommandsUndup = new Map<string, CommandBase>()
	mappedCommands = new Map<string, CommandBase>()
	categories = new Map<string, CommandBase[]>()
	currentCategoryArray: string[] | null = null
	lastCommandContext = new Map<string, CommandContext>()
	lastCommandContextChannel = new Map<string, Map<string, CommandContext>>()
	commandContextes = new Map<string, CommandContext[]>()
	prefix = ['}', ')']
	customPrefix = new Map<string, CommandBase[]>()
	bot: BotInstance
	banStates = new Map<string, ServerCommandsState>()

	transform: ((input: string) => string)[] = []

	get size() { return this.registerCommand.length }
	get length() { return this.registerCommand.length }

	constructor(bot: BotInstance) {
		this.bot = bot
		this.bot.on('validMessageUpdate', (oldMessage: Discord.Message, newMessage: Discord.Message) => this.onEdit(oldMessage, newMessage))
		this.bot.on('validMessageDelete', (msg: Discord.Message) => this.onDelete(msg))
	}

	lastContext(user: string | Discord.User | Discord.GuildMember | null) {
		if (typeof user == 'string') {
			return this.lastCommandContext.get(user) || null
		} else if (user instanceof Discord.User) {
			return this.lastCommandContext.get(user.id) || null
		} else if (user instanceof Discord.GuildMember) {
			return this.lastCommandContext.get(user.id) || null // ???
		}

		return null
	}

	lastContextChannel(channel: Discord.Channel | null, user: string | Discord.User | Discord.GuildMember | null) {
		if (!channel) {
			return null
		}

		let userid

		if (typeof user == 'string') {
			userid = user
		} else if (user instanceof Discord.User) {
			userid = user.id
		} else if (user instanceof Discord.GuildMember) {
			userid = user.id
		} else {
			return null
		}

		if (this.lastCommandContextChannel.has(userid)) {
			return (<Map<string, CommandContext>> this.lastCommandContextChannel.get(userid)).get(channel.id)
		}

		return null
	}

	setCategory(...categories: string[]) {
		for (const category of categories) {
			if (!this.categories.has(category)) {
				this.categories.set(category, [])
			}
		}

		this.currentCategoryArray = categories

		return this
	}

	unsetCategory() {
		this.currentCategoryArray = null
		return this
	}

	registerCommand(command: CommandBase) {
		command.setHolder(this)

		if (this.currentCategoryArray) {
			for (const category of this.currentCategoryArray) {
				this.categories.get(category)!.push(command)
			}
		}

		this.registeredCommands.set(command.id, command)
		this.registeredCommandsUndup.set(command.id, command)

		for (const alias of command.command_names) {
			this.registeredCommands.set(alias, command)
		}

		if (!command.custom_prefix) {
			this.mappedCommands.set(command.id, command)

			for (const alias of command.command_names) {
				this.mappedCommands.set(alias, command)
			}
		} else {
			if (!this.customPrefix.has(command.custom_prefix)) {
				this.customPrefix.set(command.custom_prefix, [])
			}

			this.customPrefix.get(command.custom_prefix)!.push(command)
		}

		return this
	}

	registerTransformer(input: (input: string) => string) {
		this.transform.push(input)
		return this
	}

	getCategory(category: string) {
		return this.categories.get(category)
	}

	has(command: string) {
		return this.mappedCommands.has(command)
	}

	values() {
		return this.registeredCommandsUndup.values()
	}

	findCommand(command: string) {
		//return this.mappedCommands.get(command) || null

		for (const icommand of this.registeredCommands.values()) {
			if (icommand.is(command)) {
				return icommand
			}
		}

		return null
	}

	getCommand(command: string) {
		return this.get(command)
	}

	get(command: string) {
		return this.registeredCommands.get(command) || null
	}

	getServerBans(server: Discord.Guild): ServerCommandsState {
		if (this.banStates.has(server.id)) {
			return this.banStates.get(server.id)!
		}

		const state = new ServerCommandsState(this.bot, server.id)
		this.banStates.set(server.id, state)
		return state
	}

	pushContext(channelID: string, context: CommandContext) {
		if (!this.commandContextes.has(channelID)) {
			this.commandContextes.set(channelID, [])
		}

		this.commandContextes.get(channelID)!.push(context)
		return this
	}

	onDelete(msg: Discord.Message) {
		if (!this.commandContextes.has(msg.channel.id)) {
			return
		}

		const contextList = this.commandContextes.get(msg.channel.id)!
		let hitMessage = null

		for (let i = contextList.length - 1; i >= 0; i--) {
			const context = contextList[i]

			if (context.msg && (context.msg == msg || context.msg.id == msg.id)) {
				hitMessage = context
				break
			}
		}

		if (!hitMessage) {
			return
		}

		hitMessage.clearAll()
	}

	onEdit(oldMessage: Discord.Message, newMessage: Discord.Message) {
		if (oldMessage.content == newMessage.content) {
			return
		}

		const channel = oldMessage.channel || newMessage.channel

		if (!this.commandContextes.has(channel.id)) {
			this.commandContextes.set(channel.id, [])
		}

		const contextList = this.commandContextes.get(channel.id)!
		let hitMessage = null

		for (let i = contextList.length - 1; i >= 0; i--) {
			const context = contextList[i]

			if (context.msg && (context.msg == oldMessage || context.msg.id == oldMessage.id)) {
				hitMessage = context
				break
			}
		}

		if (!hitMessage) {
			const [parse, commandSet] = this.parseContent(newMessage)

			if (!this.parseContent(oldMessage)[0] && parse && oldMessage.createdTimestamp + 60000 > Date.now()) {
				const context = new CommandContext(this.bot, parse!, newMessage)
				this.proceed(context, commandSet)
				this.pushContext(channel.id, context)
			}

			return
		}

		contextList.splice(contextList.indexOf(hitMessage), 1)

		if (hitMessage.canEdit) {
			const [parsed, commandSet] = this.parseContent(newMessage)

			if (parsed) {
				const context = new CommandContext(this.bot, parsed, newMessage)
				context.setEdit(hitMessage)
				this.proceed(context, commandSet)
				this.pushContext(channel.id, context)
			} else {
				hitMessage.clearAll()
			}
		} else {
			hitMessage.clearAll()
			this.call(newMessage)
		}
	}

	proceed(context: CommandContext, commandSet: null | Iterable<CommandBase>): Promise<[CommandContext, CommandBase] | null> | null {
		context.parse()
		const getCommand = context.getCommand()

		if (!getCommand) {
			return null
		}

		let command: CommandBase | null = null

		if (commandSet == null) {
			command = this.findCommand(getCommand)
		} else {
			for (const icommand of commandSet) {
				if (icommand.is(getCommand)) {
					command = icommand
					break
				}
			}
		}

		if (!command) {
			return null
		}

		if (!this.bot.addAntispam(context.msg!.author, command.getAntispamWeight(context.msg!.author, context.msg!))) {
			return null
		}

		const executeCommand = async (): Promise<[CommandContext, CommandBase] | null> => {
			try {
				context.importFlags(command!)
				context.rebuildRaw()
				context.parseFull()

				const status = await command!.execute(context)

				if (status == false) {
					return null
				}

				if (command!.remember_command_context && context.author) {
					this.lastCommandContext.set(context.author.id, context)

					if (context.channel) {
						if (!this.lastCommandContextChannel.has(context.author.id)) {
							this.lastCommandContextChannel.set(context.author.id, new Map())
						}

						this.lastCommandContextChannel.get(context.author.id)!.set(context.channel.id, context)
					}

				}
			} catch(err) {
				console.error(err)
			}

			return [context, command!]
		}

		return new Promise(async (resolve, reject) => {
			if (!context.isInServer || context.member && context.member.hasPermission('ADMINISTRATOR') || context.isOwner) {
				return resolve(await executeCommand())
			}

			const state = this.getServerBans(context.server!)

			try {
				if (await state.resolveBanned(command!)) {
					return
				}
			} catch(err) {
				reject('Command is banned from the server')
			}

			if (context.channel) {
				try {
					if (await state.resolveChannelBanned(<Discord.TextChannel> context.channel, command!)) {
						return
					}

					resolve(await executeCommand())
				} catch(err) {
					reject('Command is banned from this channel')
				}

				return
			}

			resolve(await executeCommand())
		})
	}

	parseContent(msg: Discord.Message): [string | null, CommandBase[] | null] {
		let raw = msg.content.trim()

		if (raw == '') {
			return [null, null]
		}

		let hitPrefix = false
		let pm = msg.channel.type == 'dm'

		for (const prefix of this.prefix) {
			if (raw.substr(0, prefix.length) == prefix) {
				raw = raw.substr(prefix.length)
				return [raw, null]
			}
		}

		if (!pm) {
			for (const [prefix, commandSet] of this.customPrefix) {
				if (raw.substr(0, prefix.length) == prefix) {
					raw = raw.substr(prefix.length)
					return [raw, commandSet]
				}
			}

			if (!hitPrefix && !pm) {
				return [null, null]
			}
		}

		return [raw, null]
	}

	call(msg: Discord.Message, force = false): Promise<[CommandContext, CommandBase] | null> | null {
		if (msg.author.id == this.bot.uid && !force) {
			//console.log('Missing author id')
			return null
		}

		if (msg.webhookID) {
			return null
		}

		if (!this.bot.checkAntispam(msg.author)) {
			//console.log('Antispam hit!')
			return null
		}

		const [raw, commandSet] = this.parseContent(msg)

		if (!raw) {
			return null
		}

		const context = new CommandContext(this.bot, raw.trim(), msg)
		this.pushContext(msg.channel.id, context)
		return this.proceed(context, commandSet)
	}
}

export {CommandHolder}
