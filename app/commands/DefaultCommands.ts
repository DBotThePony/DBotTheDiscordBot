

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {Help} from './base/Help'
import {Eval} from './base/Eval'
import {HashCommand, ServerOwnerCommand, PermissionsList, AdminList, ServerInfo, NFCCommand, NFDCommand, NFKCCommand, NFKDCommand, UnZalgoCommand} from './base/Util'
import {CommandHolder} from './CommandHolder'

import {Invite, SetAvatar, GetAvatar, About, Ping, DateCommand, BotInfoCommand} from './base/MiscCommands'
import {CMDManip} from './base/CommandManip'
import {Retry} from './base/Retry'
import {XD, Leet} from './fun/MiscFun'
import {GuessAWordGame, GuessCommand, GuessCommandRussian, GuessAWordGameRussian, HangmanStats} from './games/GuessAWord'
import {RegisterCowsay} from './fun/Cowsay'
import {ShippingCommand} from './fun/Shipping'
import {WordWallCommand} from './fun/text/WordWall'
import {RollTheDice} from './fun/text/RollTheDice'
import {ASCIIPonyCommand, ASCIIPonyImageCommand} from './fun/text/ASCIIPony'
import {Fortune, VulgarFortune, CopyPasta, Intel, NoIntel, Pogreb, Zander, Ogon} from './fun/text/Fortune'
import {registerRoleplayCommands} from './fun/Roleplay'
import {WastedCommand, YouDied} from './image/Wasted'
import {NFS} from './fun/Misc'
import {Aesthetics, TextFlip, TextFlop} from './fun/text/TextManip'
import {SourceServerPing, SourceServerHackPing} from './util/SourceServerPing'
import {SteamIDCommand} from './util/SteamID'
import {ColorCommand, ColorsCommand, ReloadColors, RemoveColors} from './util/ColorCommand'
import { RatedByCommand } from './image/RatedBy';
import { GoldCommand } from './image/Gold';
import { GachiPls } from './image/GachiPls';
import { EmoteCommand } from './image/Emote';
import { KeywordsManip } from './base/KeywordsManip';
import { RoleGiverCommand, RoleRemoverCommand, ManageRolesCommand, ModRoleGiverCommand, ModRoleRemoverCommand } from './util/RoleGiver';
import { ManagePermaRoles, AddPermaNickname } from './util/ManagePermaStuff';
import { AddAutoReact, RemoveAutoReact, ListAutoReact } from './fun/AutoReact';
import { NowPlayingCommand } from './util/NowPlaying';
import { PurgeCommand, PurgeCommand2 } from './util/Purge';
import { LoggingCommand } from './util/Logging';
import { SedCommand } from './fun/text/Sed';
import { VoiceRoleCommand } from './util/VoiceRole';
import { SCPCommand } from './fun/text/SCP';
import { CheckNicknames } from './util/NormalizeNicknames';
import { MagicBallCommand } from './fun/text/8ball';
import { FreedomUnitsCommand } from './util/FreedomUnits';
import { IPLookupCommand } from './util/IPLookup';
import { SearchForAllPinsCommand } from './util/SearchForAllPins'
import { CoinFlipCommand, ChooseCommand } from './util/CoinFlip'
import { HackbanCommand } from './util/AdminTools'

const registerDefaultCommands = function(holder: CommandHolder) {
	holder.registerCommand(new Help())
	holder.registerCommand(new Eval())
	holder.registerCommand(new Invite())
	holder.registerCommand(new SetAvatar())
	holder.registerCommand(new About())
	holder.registerCommand(new Ping())
	holder.registerCommand(new DateCommand())
	holder.registerCommand(new Retry())
	holder.registerCommand(new BotInfoCommand())
	holder.registerCommand(new CMDManip(true))
	holder.registerCommand(new CMDManip(true, true))
	holder.registerCommand(new CMDManip(false))

	holder.setCategory('keymap', 'util')
	holder.registerCommand(new KeywordsManip(false, false))
	holder.registerCommand(new KeywordsManip(false, true))
	holder.registerCommand(new KeywordsManip(true, false))
	holder.registerCommand(new KeywordsManip(true, true))

	holder.setCategory('roles', 'util')
	holder.registerCommand(new RoleGiverCommand())
	holder.registerCommand(new RoleRemoverCommand())
	holder.registerCommand(new ManageRolesCommand())
	holder.registerCommand(new ModRoleGiverCommand())
	holder.registerCommand(new ModRoleRemoverCommand())
	holder.registerCommand(new ManagePermaRoles())
	holder.registerCommand(new AddPermaNickname())

	holder.setCategory('util', 'text_util', 'text_fun')
	holder.registerCommand(new AddAutoReact(true))
	holder.registerCommand(new RemoveAutoReact(true))
	holder.registerCommand(new ListAutoReact(true))
	holder.registerCommand(new AddAutoReact(false))
	holder.registerCommand(new RemoveAutoReact(false))
	holder.registerCommand(new ListAutoReact(false))

	holder.setCategory('util', 'text_fun')
	holder.registerCommand(new CoinFlipCommand())
	holder.registerCommand(new ChooseCommand())

	holder.setCategory('colors', 'util')
	holder.registerCommand(new ColorCommand())
	holder.registerCommand(new ColorsCommand())
	holder.registerCommand(new ReloadColors())
	holder.registerCommand(new RemoveColors())

	holder.setCategory('hash')
	holder.registerCommand(new HashCommand('MD5'))
	holder.registerCommand(new HashCommand('SHA256'))
	holder.registerCommand(new HashCommand('SHA1'))
	holder.registerCommand(new HashCommand('SHA512'))

	holder.setCategory('text', 'util')
	holder.registerCommand(new NFCCommand())
	holder.registerCommand(new NFDCommand())
	holder.registerCommand(new NFKCCommand())
	holder.registerCommand(new NFKDCommand())
	holder.registerCommand(new UnZalgoCommand())
	holder.registerCommand(new SearchForAllPinsCommand())
	holder.setCategory('users', 'util')
	holder.registerCommand(new CheckNicknames())

	holder.setCategory('games', 'fun_text', 'fun')
	holder.registerCommand(new GuessAWordGame())
	holder.registerCommand(new GuessCommand())
	holder.registerCommand(new GuessAWordGameRussian())
	holder.registerCommand(new GuessCommandRussian())
	holder.registerCommand(new HangmanStats())
	holder.registerCommand(new MagicBallCommand())

	holder.setCategory('fun', 'images')
	holder.registerCommand(new NFS())
	holder.registerCommand(new YouDied())
	holder.registerCommand(new RatedByCommand())
	holder.registerCommand(new GoldCommand())
	holder.registerCommand(new GachiPls())
	holder.registerCommand(new EmoteCommand())
	holder.registerCommand(new WastedCommand())
	holder.registerCommand(new WastedCommand('cactus', 'you got cocky, mate'))

	holder.setCategory('fun_text', 'fun')
	holder.registerCommand(new SedCommand())
	holder.registerCommand(new WordWallCommand())
	holder.registerCommand(new XD())
	holder.registerCommand(new Leet())

	holder.registerCommand(new Aesthetics())
	holder.registerCommand(new TextFlip())
	holder.registerCommand(new TextFlop())

	holder.registerCommand(new RollTheDice())
	holder.registerCommand(new SCPCommand())

	RegisterCowsay(holder)

	holder.setCategory('util')
	holder.registerCommand(new SourceServerPing())
	holder.registerCommand(new SourceServerHackPing())
	holder.registerCommand(new ServerOwnerCommand())
	holder.registerCommand(new PermissionsList())
	holder.registerCommand(new AdminList())
	holder.registerCommand(new GetAvatar())
	holder.registerCommand(new ServerInfo())
	holder.registerCommand(new NowPlayingCommand())
	holder.registerCommand(new PurgeCommand())
	holder.registerCommand(new PurgeCommand2())
	holder.registerCommand(new LoggingCommand())
	holder.registerCommand(new VoiceRoleCommand())
	holder.registerCommand(new FreedomUnitsCommand())
	holder.registerCommand(new IPLookupCommand())

	if (holder.bot.config.steam_enable) {
		holder.registerCommand(new SteamIDCommand())
	}

	holder.setCategory('util', 'admin')
	holder.registerCommand(new HackbanCommand())

	holder.setCategory('roleplay', 'ponystuff')
	registerRoleplayCommands(holder)

	holder.setCategory('ponystuff')
	holder.registerCommand(new ShippingCommand())
	holder.registerCommand(new ASCIIPonyCommand())
	holder.registerCommand(new ASCIIPonyImageCommand())

	holder.setCategory('quotes')
	holder.registerCommand(new Fortune())
	holder.registerCommand(new VulgarFortune())
	holder.registerCommand(new CopyPasta())

	holder.registerCommand(new Intel())
	holder.registerCommand(new NoIntel())
	holder.registerCommand(new Pogreb())
	holder.registerCommand(new Zander())
	holder.registerCommand(new Ogon())
}

export {registerDefaultCommands, Help, Eval, Invite}
