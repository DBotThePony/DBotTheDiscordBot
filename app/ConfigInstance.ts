

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


interface SQLData {
	sql_hostname: string
	sql_port?: number
	sql_user: string
	sql_password: string
	sql_database: string
	sql_workers?: number
}

interface ConfigData {
	token: string
	google: string
	google_enable: boolean
	steam: string
	steam_enable: boolean

	sql_hostname?: string
	sql_port?: number
	sql_user?: string
	sql_password?: string
	sql_database?: string
	sql_workers?: number
	webroot: string
	webpath: string
	protocol: string
	owners: string[]
}

class SQLConfig implements SQLData {
	sql_hostname: string
	sql_port: number
	sql_user: string
	sql_password: string
	sql_database: string
	sql_workers: number
	host: string
	port: number
	user: string
	password: string
	database: string
	workers: number

	constructor(inputData: SQLData) {
		this.sql_hostname = inputData.sql_hostname
		this.sql_port = inputData.sql_port || 5432
		this.sql_user = inputData.sql_user
		this.sql_password = inputData.sql_password
		this.sql_database = inputData.sql_database
		this.sql_workers = inputData.sql_workers || 1

		this.host = inputData.sql_hostname
		this.port = inputData.sql_port || 5432
		this.user = inputData.sql_user
		this.password = inputData.sql_password
		this.database = inputData.sql_database
		this.workers = inputData.sql_workers || 1
	}

	getSQL() {
		const {
			host, port, user, password, database
		} = this
		return {
			host, port, user, password, database
		}
	}
}

class ConfigInstance implements ConfigData {
	data: ConfigData
	token: string
	google: string
	google_enable: boolean
	steam: string
	steam_enable: boolean
	sql_hostname?: string
	sql_port?: number
	sql_user?: string
	sql_password?: string
	sql_database?: string
	sql_workers?: number
	webroot: string
	webpath: string
	protocol: string
	owners: string[]
	sql_config: SQLConfig | null = null

	constructor(inputData: ConfigData) {
		this.data = inputData
		this.token = inputData.token
		this.google = inputData.google
		this.google_enable = inputData.google_enable
		this.steam = inputData.steam
		this.steam_enable = inputData.steam_enable
		this.sql_hostname = inputData.sql_hostname
		this.sql_port = inputData.sql_port
		this.sql_user = inputData.sql_user
		this.sql_password = inputData.sql_password
		this.sql_database = inputData.sql_database
		this.sql_workers = inputData.sql_workers
		this.webroot = inputData.webroot
		this.webpath = inputData.webpath
		this.protocol = inputData.protocol
		this.owners = inputData.owners

		if (this.isValidSQL()) {
			this.sql_config = new SQLConfig(<any> inputData)
		}
	}

	isValidSQL() {
		return this.sql_user != undefined && this.sql_password != undefined && this.sql_database != undefined
	}

	getSQL() {
		return this.sql_config && this.sql_config.getSQL()
	}
}

export {ConfigInstance, SQLConfig, ConfigData, SQLData}
