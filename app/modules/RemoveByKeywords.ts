

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import Discord = require('discord.js')
import { BotInstance } from "../BotInstance";
import { AsyncDBArrayHolder } from "../../lib/AsyncDBArrayHolder";

class RemoveByKeywords {
	perChannel = new AsyncDBArrayHolder(this.bot, 'keywords_ban_channel', 'server', 'channel', 'keywords')
	perServer = new AsyncDBArrayHolder(this.bot, 'keywords_ban_server', 'server', 'keywords')
	perChannelUser = new AsyncDBArrayHolder(this.bot, 'keywords_ban_user_channel', 'server', 'channel', 'user', 'keywords')
	perServerUser = new AsyncDBArrayHolder(this.bot, 'keywords_ban_user_server', 'server', 'user', 'keywords')
	perServerWhitelist = new Map<string, string[]>();

	constructor(public bot: BotInstance) {

	}

	turnIntoRegexp(strIn: string) {
		let isRegExp = false

		for (let char of strIn) {
			if (char == '*') {
				isRegExp = true
				break
			} else if (char == '\\') {
				isRegExp = true
				break
			} else if (char == '[') {
				isRegExp = true
				break
			} else if (char == ']') {
				isRegExp = true
				break
			} else if (char == '{') {
				isRegExp = true
				break
			} else if (char == '+') {
				isRegExp = true
				break
			} else if (char == '-') {
				isRegExp = true
				break
			} else if (char == '.') {
				isRegExp = true
				break
			} else if (char == '?') {
				isRegExp = true
				break
			}
		}

		if (isRegExp) {
			return new RegExp(strIn, 'i')
		}

		return new RegExp('(^| )' + strIn.split('').join(' *') + '($| )', 'i')
	}

	check(msg: Discord.Message | string, arr: string[], dryrun = false) {
		if (arr.length == 0) {
			return true
		}

		const content = typeof msg == 'string' ? msg : msg.content

		let dryresult: string[]

		if (dryrun) {
			dryresult = []
		}

		for (const str of arr) {
			if (content.match(this.turnIntoRegexp(str))) {
				if (dryrun) {
					dryresult!.push(str)
				} else if (typeof msg == 'object') {
					this.messageInLog(msg, str)
					msg.delete()
					return false
				} else {
					throw new TypeError('keywords.check called with non-object when dryrun = false')
				}
			}
		}

		if (dryrun) {
			return dryresult!
		}

		return true
	}

	async messageInLog(msg: Discord.Message, filter: string) {
		const server = msg.guild
		const author = msg.author

		const result = await this.bot.sql.query(`SELECT "channel" FROM "keywords_log" WHERE "server" = ${server!.id}`)

		if (result.rowCount == 0) {
			return
		}

		const channel = server!.channels.cache.get(result.rows[0].channel)

		if (!channel) {
			return
		}

		(<Discord.TextChannel> channel).send(`Message sent by ${author.username}<${author.id}> in <#${msg.channel.id}> got removed by filter: \`${filter}\`:` + '```\n' +
			msg.content.replace(/@/g, '@ ').replace(/```/, '\`\`\`')
		+ '\n```')
	}

	async getWhitelistedList(server: Discord.Guild) {
		if (!this.perServerWhitelist.has(server.id)) {
			const result = await this.bot.sql.query(`SELECT "channels" FROM "keywords_whitelist" WHERE "server" = ${server.id}`)

			if (result.rowCount == 0) {
				this.perServerWhitelist.set(server.id, [])
			} else {
				this.perServerWhitelist.set(server.id, result.rows[0].channels)
			}
		}

		return this.perServerWhitelist.get(server.id)!
	}

	async isWhitelisted(channel: Discord.TextChannel) {
		if (!channel.guild) {
			throw new TypeError('Channel must belong to server')
		}

		const arr = await this.getWhitelistedList(channel.guild)
		return arr.includes(channel.id)
	}

	async addToWhitelist(channel: Discord.TextChannel) {
		if (!channel.guild) {
			throw new TypeError('Channel must belong to server')
		}

		if (await this.isWhitelisted(channel)) {
			return false
		}

		await this.bot.sql.query(`INSERT INTO "keywords_whitelist" ("server", "channels") VALUES (${channel.guild.id}, ARRAY[${channel.id}])
			ON CONFLICT ("server") DO UPDATE SET "channels" = "keywords_whitelist"."channels" || excluded."channels"`);

		const arr = await this.getWhitelistedList(channel.guild)
		arr.push(channel.id)
		return true
	}

	async removeFromWhitelist(channel: Discord.TextChannel) {
		if (!channel.guild) {
			throw new TypeError('Channel must belong to server')
		}

		if (!await this.isWhitelisted(channel)) {
			return false
		}

		await this.bot.sql.query(`UPDATE "keywords_whitelist" SET "channels" = array_remove("channels", ${channel.id}) WHERE "server" = ${channel.guild.id}`)

		const map = await this.getWhitelistedList(channel.guild)
		map.splice(map.indexOf(channel.id), 1)
		return true
	}

	async switchWhitelistState(channel: Discord.TextChannel) {
		if (await this.isWhitelisted(channel)) {
			await this.removeFromWhitelist(channel)
			return false
		}

		await this.addToWhitelist(channel)
		return true
	}

	async onMessage(msg: Discord.Message): Promise<boolean> {
		if (msg.author.id == this.bot.client.user!.id) {
			return true
		}

		if (msg.channel.type == 'dm') {
			return true
		}

		if (!msg.guild!.me!.hasPermission('ADMINISTRATOR')) {
			const perms = (<Discord.TextChannel> msg.channel).permissionsFor(msg.guild!.me!)

			if (!perms) {
				if (!msg.guild!.me!.hasPermission('MANAGE_MESSAGES')) {
					return true
				}
			} else {
				if (!perms.has('MANAGE_MESSAGES')) {
					return true
				}
			}
		}

		if (this.bot.config.owners.includes(msg.author.id)) {
			return true
		}

		if (!msg.member) {
			return true
		}

		if (msg.member.hasPermission('MANAGE_GUILD')) {
			return true
		}

		if (await this.isWhitelisted(<Discord.TextChannel> msg.channel)) {
			return true
		}

		let arr = await this.perServer.load(msg.guild!.id)

		if (!this.check(msg, arr)) {
			return false
		}

		arr = await this.perChannel.load(msg.guild!.id, msg.channel.id)

		if (!this.check(msg, arr)) {
			return false
		}

		arr = await this.perServerUser.load(msg.guild!.id, msg.author.id)

		if (!this.check(msg, arr)) {
			return false
		}

		arr = await this.perChannelUser.load(msg.guild!.id, msg.channel.id, msg.author.id)

		if (!this.check(msg, arr)) {
			return false
		}

		return true
	}
}

export {RemoveByKeywords}