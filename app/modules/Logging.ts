
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import { AsyncDBHashMap } from "../../lib/AsyncDBHashMap";
import { BotInstance } from "../BotInstance";
import Discord = require('discord.js')
import humanizeDuration = require('humanize-duration')

interface ILoggingOptions {
	log_join?: boolean
	log_leave?: boolean
	log_edits?: boolean
	log_deletions?: boolean
	log_nicknames?: boolean
	channel_log?: string
	channel_join?: string
}

export {ILoggingOptions as ILoggingManerOptions}

class LoggingManager {
	servers = new AsyncDBHashMap<ILoggingOptions>(this.bot, 'logging_servers', 'server', 'options')

	constructor(public bot: BotInstance) {
		bot.client.on('messageUpdate', (oldMsg, newMsg) => this.messageUpdate(oldMsg, newMsg))
		bot.client.on('messageDelete', (msg) => this.messageDelete(msg))
		bot.client.on('guildMemberAdd', (member) => this.guildMemberAdd(member))
		bot.client.on('guildMemberRemove', (member) => this.guildMemberRemove(member))
		bot.client.on('guildMemberUpdate', (member, member2) => this.guildMemberUpdate(member, member2))
	}

	getValidChannel(channel: string, me: Discord.GuildMember) {
		if (!channel) {
			return null
		}

		const getChannel = this.bot.client.channels.cache.get(channel)

		if (!getChannel) {
			return null
		}

		const overrides = (<Discord.TextChannel> getChannel).permissionsFor(me)

		if (overrides && !me.hasPermission('ADMINISTRATOR') && !overrides.has('SEND_MESSAGES')) {
			return null
		}

		return <Discord.TextChannel> getChannel
	}

	get logTime() {
		const now = new Date()
		const hours = now.getUTCHours(), minutes = now.getUTCMinutes(), seconds = now.getUTCSeconds()
		return `\`[${hours >= 10 ? hours : '0' + hours}:${minutes >= 10 ? minutes : '0' + minutes}:${seconds >= 10 ? seconds : '0' + seconds} UTC+0]\``
	}

	protected formatUser(member: Discord.GuildMember) {
		return `\`${member.nickname != undefined && member.nickname != member.user.username ? member.nickname + ' (' + member.user.username + ')' : member.user.username}\`<\`${member.id}\`>`
	}

	protected escapeMessage(msg: string, guild: Discord.Guild) {
		return msg.replace(/<@!?([0-9]+)>/g, (sub, arg1) => {
			const member = guild.members.cache.get(arg1)
			return member ? this.formatUser(member) : '@invalid-user'
		}).replace(/<&([0-9]+)>/g, (sub, arg1) => {
			const role = guild.roles.cache.get(arg1)
			return role != undefined ? role.name : '@invalid-role'
		})
	}

	async messageUpdate(oldmsg: Discord.Message, newmsg: Discord.Message) {
		if (!newmsg.guild || newmsg.content == oldmsg.content || newmsg.author.id == this.bot.id || newmsg.member?.user.bot) {
			return
		}

		const options = await this.servers.get(newmsg.guild.id)

		if (!options || !options.channel_log || !options.log_edits) {
			return
		}

		const validChannel = this.getValidChannel(options.channel_log, newmsg.guild.me!)

		if (!validChannel) {
			return
		}

		validChannel.send(`${this.logTime} :pencil: Message Edit:
User: ${this.formatUser(newmsg.member)}
Channel: <#${newmsg.channel.id}>
Before:
${this.escapeMessage(oldmsg.content, newmsg.guild)}
After:
${this.escapeMessage(newmsg.content, newmsg.guild)}
`)
	}

	async messageDelete(msg: Discord.Message) {
		if (!msg.guild || msg.author.id == this.bot.id || msg.member?.user.bot) {
			return
		}

		const options = await this.servers.get(msg.guild.id)

		if (!options || !options.channel_log || !options.log_deletions) {
			return
		}

		const validChannel = this.getValidChannel(options.channel_log, msg.guild.me!)

		if (!validChannel) {
			return
		}

		validChannel.send(`${this.logTime} :wastebasket: Message Delete:
User: ${this.formatUser(msg.member)}
Channel: <#${msg.channel.id}>
Was:
${this.escapeMessage(msg.content, msg.guild)}
`)
	}

	async guildMemberAdd(member: Discord.GuildMember) {
		const options = await this.servers.get(member.guild.id)

		if (!options || !options.channel_join) {
			return
		}

		const validChannel = this.getValidChannel(options.channel_join, member.guild.me!)

		if (!validChannel) {
			return
		}

		validChannel.send(`${this.logTime} :bell: Member joined:
User: ${this.formatUser(member)}
Created at: ${member.user.createdAt} (${humanizeDuration(Date.now() - member.user.createdTimestamp)} ago)
--- \`(${new Date()})\`
`)
	}

	async guildMemberRemove(member: Discord.GuildMember) {
		const options = await this.servers.get(member.guild.id)

		if (!options || !options.channel_join) {
			return
		}

		const validChannel = this.getValidChannel(options.channel_join, member.guild.me!)

		if (!validChannel) {
			return
		}

		validChannel.send(`${this.logTime} :door: Member left:
User: ${this.formatUser(member)}
Joined: ${member.joinedAt}
Was in server for: ${humanizeDuration(Date.now() - member.joinedTimestamp)}
--- \`(${new Date()})\`
`)
	}

	async guildMemberUpdate(member: Discord.GuildMember, member2: Discord.GuildMember) {
		if (member.nickname == member2.nickname) {
			return
		}

		const options = await this.servers.get(member.guild.id)

		if (!options || !options.channel_log) {
			return
		}

		const validChannel = this.getValidChannel(options.channel_log, member.guild.me!)

		if (!validChannel) {
			return
		}

		validChannel.send(`${this.logTime} :arrows_clockwise: Member changed nickname:
User: ${this.formatUser(member)}
Before: \`${member.nickname != undefined ? member.nickname : '<no nickname>'}\`
After: \`${member2.nickname != undefined ? member2.nickname : '<no nickname>'}\`
`)
	}
}

export {LoggingManager}
