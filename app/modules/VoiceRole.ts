
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Discord = require('discord.js')
import { BotInstance } from "../BotInstance"
import { AsyncDBArrayHolder } from "../../lib/AsyncDBArrayHolder"

class VoiceRole {
	voice = new AsyncDBArrayHolder(this.bot, 'voice_role', 'channel', 'role').disableCache(true).castToBigInt()

	constructor(public bot: BotInstance) {
		bot.client.on('voiceStateUpdate', (oldState, newState) => this.voiceStateUpdate(oldState, newState))
	}

	async voiceStateUpdate(oldVoice: Discord.VoiceState, newVoice: Discord.VoiceState) {
		if (!newVoice.guild.me || !newVoice.guild.me.hasPermission('MANAGE_ROLES')) {
			return
		}

		let roles, status

		if (newVoice.channel) {
			roles = await this.voice.asyncGet(newVoice.channel.id)
			status = true
		} else if (oldVoice.channel) {
			roles = await this.voice.asyncGet(oldVoice.channel.id)
			status = false
		}

		if (!roles || roles.length == 0) {
			return
		}

		const rolesobj = []

		for (const roleid of roles) {
			const role = newVoice.guild.roles.cache.get(roleid)

			if (role) {
				rolesobj.push(role)
			}
		}

		if (rolesobj.length == 0) {
			return
		}

		if (status) {
			newVoice.member?.roles.add(rolesobj)
		} else {
			newVoice.member?.roles.remove(rolesobj)
		}
	}
}

export {VoiceRole}
