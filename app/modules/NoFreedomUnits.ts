
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Discord = require('discord.js')
import { BotInstance } from '../BotInstance';
import numeral = require('numeral')

const _prefixes: [string, number][] = [
	['n', 1 / 1_000_000_000],
	['µ', 1 / 1_000_000],
	['m', 1 / 1_000],
	// ['c', 1 / 100],
	// ['d', 1 / 10],
	// ['da', 10],
	// ['h', 100],
	['k', 1_000],
	['M', 1_000_000],
	['G', 1_000_000_000],
	['T', 1_000_000_000_000],
]

const prefixes = _prefixes.sort((a, b) => a[1] == b[1] ? 0 : a[1] > b[1] ? 1 : -1)

function whut(input: [string, string][]): [RegExp, string][] {
	const output: [RegExp, string][] = []

	for (const [a, b] of input) {
		output.push([new RegExp(a, 'g'), b])
		output.push([new RegExp(a.toUpperCase(), 'g'), b.toUpperCase()])
	}

	return output
}

function getSiPrefix(numIn: number) {
	if (numIn < 1000 && numIn > 1) {
		return ['', numIn]
	}

	let selectName: string, selectValue: number

	for (const [name, value] of prefixes) {
		if (numIn > value) {
			selectName = name, selectValue = value
		}
	}

	if (!selectName!) {
		return ['', numIn]
	}

	return [selectName!, numIn / selectValue!]
}

function normalize(set: [string, number][]): [RegExp, number][] {
	for (const row of set) {
		(<any> row)[0] = new RegExp('(^|\\s)(-?[0-9]+[\\.,][0-9]+|-?[0-9]+|-?[\\.,][0-9]+|[0-9]+e[\\-\\+][0-9]+|a\\s+)\\s*' + row[0] + '(\\s|$)', 'gi')
	}

	// (-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)

	return <any> set
}

function replaceDistance(input: number) {
	if (Math.abs(input) < 1) {
		return Math.floor(input * 1000) / 10 + ' cm'
	}

	const [name, input2] = getSiPrefix(input)
	return `${numeral(input2).format('0,0.00')} ${name}m`
}

function replaceSpeed(input: number) {
	const [name, input2] = getSiPrefix(input)
	const [name_s, input_s] = getSiPrefix(input / 3600)
	return `${numeral(input2).format('0,0.00')} ${name}m/h / ${numeral(input_s).format('0,0.00')} ${name_s}m/s`
}

function replaceSurface(input: number) {
	if (input >= 1000000) {
		return numeral(input / 1000000).format('0.00') + ' km^2'
	}

	return numeral(input).format('0,0.0000') + ' m^2'
}

function replaceWeight(input: number) {
	if (input >= 1000000) {
		return numeral(input / 1000000).format('0.0') + ' metric tonne'
	}

	return numeral(input).format('0.00 a') + 'g'
}

function replaceVolume(input: number) {
	if (input >= 1000) {
		return numeral(input / 1000).format('0.0') + ' m^3'
	}

	const [name, input2] = getSiPrefix(input)
	return `${numeral(input2).format('0,0.000')} ${name}L`
}

function goFormatTemp(temp: number) {
	return numeral(temp).format('0,0.00') + '°C / ' + numeral(temp + 273.15).format('0,0.00') + '°K'
}

function replaceTemperature(input: number) {
	return goFormatTemp((input - 32) * 5 / 9)
}

function replaceTemperature2(input: number) {
	return `If Rankine: ${replaceTemperature(input - 459.67)}; If Réaumur: ${goFormatTemp(input * 1.25)}`
}

function replacePressure(input: number) {
	return numeral(input / 1000).format('0,0.00') + ' kPa / ' + numeral(input / 101325).format('0.000') + ' atm'
}

function replaceWork(input: number) {
	const [name, input2] = getSiPrefix(input)
	return `${numeral(input2).format('0,0.00')} ${name}J`
}

function replacePower(input: number) {
	const [name, input2] = getSiPrefix(input)
	return `${numeral(input2).format('0,0.00')} ${name}W`
}

function replaceForce(input: number) {
	const [name, input2] = getSiPrefix(input)
	return `${numeral(input2).format('0,0.00')} ${name}N`
}

function getWeekOfMonth(date: Date, exact: boolean) {
	var month = date.getUTCMonth()
		, year = date.getFullYear()
		, firstWeekday = new Date(year, month, 1).getUTCDay()
		, lastDateOfMonth = new Date(year, month + 1, 0).getUTCDate()
		, offsetDate = date.getUTCDate() + firstWeekday - 1
		, index = 1 // start index at 0 or 1, your choice
		, weeksInMonth = index + Math.ceil((lastDateOfMonth + firstWeekday - 7) / 7)
		, week = index + Math.floor(offsetDate / 7)

	if (exact || week < 2 + index) return week
	return week === weeksInMonth ? index + 5 : week
}

function twon(num: number) {
	if (num < 10) {
		return '0' + num
	}

	return String(num)
}

function _wraphour(num: number) {
	num %= 24

	if (num < 0) {
		num += 24
	}

	return num
}

function putNormalUTC(hours: number, minutes: string, utcShift: number) {
	return ` / ${_wraphour(hours - utcShift - 3)}:${minutes} UTC-3 / ${_wraphour(hours - utcShift - 1)}:${minutes} UTC-1 / ${_wraphour(hours - utcShift)}:${minutes} UTC+0 / ${_wraphour(hours - utcShift + 1)}:${minutes} UTC+1 / ${_wraphour(hours - utcShift + 3)}:${minutes} UTC+3`
}

class NoFreedomUnits {
	messages = new Map<string, Discord.Message>()
	pending: Discord.Message[] = []

	static distance = normalize([
		['nautical miles?', 1852],
		['statute miles?', 1609.344],
		['in', 0.0254],
		['in(ch)(es)?', 0.0254],
		['yards?', 0.9144],
		['yds?', 0.9144],
		['rods?', 0.9144 * 5.5],
		['logs?', 0.9144 * 5.5],
		['perchs?', 0.9144 * 5.5],
		['f(e|o){2}ts?', 0.3048],
		['ft', 0.3048],
		['hammer units?', 0.0254],
		['hus?', 0.0254],
		['xbox controllers?', 0.3048 * 6],
		['miles?', 1609.34],
		['furlongs?', 1609.34 / 8],
	])

	static speed = normalize([
		['knots?', 1852],
		['miles? per hour', 1609.34],
		['mph', 1609.34],
		['mps', 1609.34 * 3600],
		['fth', 0.3048],
		['ft per hour', 0.3048],
		['foots? per hour', 0.3048],
		['f(e|o){2}ts? per hour', 0.3048],
		['fts', 0.3048 * 3600],
	])

	static surface = normalize([
		['square miles?', 2589988.110336],
		['acres?', 4840 * 0.83613],
		['oxgangs?', 4840 * 0.83613 * 15],
		['virgates?', 4840 * 0.83613 * 30],
		['carucates?', 4840 * 0.83613 * 120],
		['square inch(es)?', 0.00064516],
		['sq in?', 0.00064516],
		['square yards?', 0.83613],
		['square f(e|o){2}ts?', 0.09290304],
		['sq ft', 0.09290304],
	])

	static pressure = normalize([
		['inhg', 3386.389],
		['psi', 6894.76],
	])

	static force = normalize([
		['pdl', 0.1383],
		['poundal', 0.1383],
		['lbf', 4.448],
		['pound force', 4.448],
	])

	static power = normalize([
		['hp', 745.7],
		['horsepowers?', 745.7],
		['calories/s', 4.1868],
		['cal/s', 4.1868],
		['calories per second', 4.1868],
		['kcalories/s', 4.1868 * 1000],
		['kcal/s', 4.1868 * 1000],
		['kcal per second', 4.1868 * 1000],
		['kilocal per second', 4.1868 * 1000],
		['kilocalories per second', 4.1868 * 1000],
	])

	static work = normalize([
		['btu', 1055.87],
		['cal', 4.1868],
		['calories?', 4.1868],
		['kcal', 4.1868 * 1000],
		['kilocalories?', 4.1868 * 1000],
		['kcalories?', 4.1868 * 1000],
		['kilocal', 4.1868 * 1000],
	])

	/*
	Grain   gr      64.80 mg
	Dram    dr  ​27 11⁄32 gr    1.772 g
	Ounce   oz  16 dr   28.35 g
	Pound   lb  16 oz    453.6 g[n 1]
	Stone UK     st  14 lb   6.350 kg
	Quarter UK       28 lb   12.70 kg
	Cental UK
	hundredweight US     cwtUS   100 lb  45.36 kg
	Hundredweight UK     cwtUK   112 lb  50.80 kg
	Ton US       2000 lb     907.2 kg
	Ton UK       2240 lb     1016 kg
	*/

	static weight = normalize([
		['grains?', 64.80 / 1000],
		['drams?', 1.7772],
		['ounces?', 28.35],
		['ounce troys?', 31.1],
		['scruples?', 1.296],
		['pounds?', 453.6],
		['short hundredweights?', 453.6 * 100],
		['long hundredweights?', 453.6 * 112],
		['centals?', 453.6 * 100],
		['lbs?', 453.6],
		['hundredweights?', 50.80 * 1000],
		['pennyweights?', 1.555],
	])

	/*
	Gallon  gal     231 cu in[n 1]  3.785 L     0.8327 imp gal
	Quart   qt  ​1⁄4 gal    946.4 mL    0.8327 imp qt
	Pint    pt  ​1⁄2 qt     473.2 mL    0.8327 imp pt
	Fluid ounce     fl oz   ​1⁄16 pt    29.57 mL    1.041 imp fl oz
	Fluid dram  fl dr   ​1⁄8 fl oz  3.6967 mL   1.041 imp fl dr
	Minim       ​1⁄60 fl dr     61.61 μL    1.041 imp fl minim
	*/

	static volume = normalize([
		['gallons?', 3.785],
		['gal', 3.785],
		['barrels?', 115.6],
		['bbls', 158.987],
		['quarts?', 946.4 / 1000],
		['pints?', 473.2 / 1000],
		['fluid ounces?', 28.413063 / 1000],
		['fl oz', 28.413063 / 1000],
		['oz', 29.57 / 1000],
		['fluid drams?', 3.6967 / 1000],
		['minims?', 61.61 / 1000000],
		['hogsheads?', 245.49],
		['table\\s?spoons?', 14.2 / 1000],
		['tea\\s?spoons?', 4.7 / 1000],
		['coffee\\s?spoons?', 2.45 / 1000],
	])

	static temperature = normalize([
		['F', 1],
		['°F', 1],
		['Fahrenheit', 1],
	])

	static temperature2 = normalize([
		['R[eé]?', 1],
		['°R[eé]?', 1],
		['Rankine', 1],
		['R[ée]aumur', 1],
	])

	constructor(public bot: BotInstance) {
		bot.on('validMessage', msg => this.onMessage(msg).catch(console.error))

		bot.client.on('messageDelete', (msg) => this.onMessageDelete(msg))
		bot.client.on('messageUpdate', (msg, msg2) => this.onMessageEdit(msg, msg2))
	}

	onMessageDelete(msg: Discord.Message) {
		if (this.pending.includes(msg)) {
			this.pending.splice(this.pending.indexOf(msg), 1)
		} else if (this.messages.has(msg.id)) {
			this.messages.get(msg.id)!.delete()
			this.messages.delete(msg.id)
		}
	}

	onMessageEdit(oldmsg: Discord.Message, newmsg: Discord.Message) {
		if (oldmsg.content == newmsg.content) {
			return
		}

		if (this.pending.includes(oldmsg)) {
			this.pending.splice(this.pending.indexOf(oldmsg), 1)
			this.onMessage(newmsg).catch(console.error)
		} else if (this.messages.has(oldmsg.id)) {
			const msg = this.messages.get(oldmsg.id)!
			this.messages.delete(oldmsg.id)
			this.onMessage(newmsg, msg).catch(console.error)
		} else if (oldmsg.createdTimestamp + 60000 > Date.now()) {
			this.onMessage(newmsg).catch(console.error)
		}
	}

	get sql() { return this.bot.sql }

	replaceUnits(content: string, set: [RegExp, number][], callback: (input: number) => string): [string, number] {
		let matches = 0

		for (const row of set) {
			content = content.replace(row[0], (matched, _, num) => {
				if (num.charAt(0) == 'a') {
					num = '1'
				}

				matches++
				return ' **' + callback(Number(num.replace(/,/g, '.')) * row[1]) + '**' + (matched[matched.length - 1] == ' ' ? ' ' : '')
			})
		}

		return [content, matches]
	}

	private async updateStats(table: string, object: string | undefined, index: string, count = 1): Promise<number | null> {
		if (!object) {
			return null
		}

		return (await this.sql.query(`INSERT INTO "${table}" ("object", "${index}") VALUES ('${object}', ${count}) ON CONFLICT ("object") DO
			UPDATE SET "${index}" = excluded."${index}" + "${table}"."${index}" RETURNING "${table}"."${index}"`)).rows[0][index]
	}

	async replaceDistance(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let matches = 0, content2 = content

		content2 = content2.replace(/(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)('|′)\s?(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)($| |,|\.)/gi, (substr, num1, _, num2, _space) => {
			matches++
			return '**' + replaceDistance(Number(num1.replace(/,/g, '.')) * 0.3048 + Number(num2.replace(/,/g, '.')) * 0.0254) + '**' + _space
		})

		content2 = content2.replace(/(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)('|′)\s?(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)(''|″|")(\s|$|,|\.)/gi, (substr, num1, _, num2, _2, _space) => {
			matches++
			return '**' + replaceDistance(Number(num1.replace(/,/g, '.')) * 0.3048 + Number(num2.replace(/,/g, '.')) * 0.0254) + '**' + _space
		})

		content2 = content2.replace(/(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)('|′)\s?(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)\s+(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)\/(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)(''|″|")(\s|$|,|\.)/gi, (substr, num1, _, num2, num3, num4, _2, _space) => {
			matches++
			return '**' + replaceDistance(Number(num1.replace(/,/g, '.')) * 0.3048 +
				(Number(num2.replace(/,/g, '.')) + Number(num3.replace(/,/g, '.') / Number(num4.replace(/,/g, '.')))) * 0.0254) + '**' + _space
		})

		content2 = content2.replace(/^([^']+)(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)(''|″|")(\s|$|,|\.)/gi, (substr, _, num1, _2, _space) => {
			matches++

			if (_.match(/(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)/)) {
				num1 = _ + num1
				_ = ''
			}

			return _ + '**' + replaceDistance(Number(num1.replace(/,/g, '.')) * 0.0254) + '**' + _space
		})

		content2 = content2.replace(/^(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)(''|″|")(\s|$|,|\.)/gi, (substr, num1, _, _space) => {
			matches++
			return '**' + replaceDistance(Number(num1.replace(/,/g, '.')) * 0.0254) + '**' + _space
		})

		content2 = content2.replace(/^([^']+)(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)('|′)(\s|$|,|\.)/gi, (substr, _, num1, _2, _space) => {
			matches++

			if (_.match(/(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)/)) {
				num1 = _ + num1
				_ = ''
			}

			return _ + '**' + replaceDistance(Number(num1.replace(/,/g, '.')) * 0.3048) + '**' + _space
		})

		content2 = content2.replace(/^(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)('|′)(\s|$|,|\.)/gi, (substr, num1, _, _space) => {
			matches++
			return '**' + replaceDistance(Number(num1.replace(/,/g, '.')) * 0.3048) + '**' + _space
		})

		let [content3, matches2] = this.replaceUnits(content2, NoFreedomUnits.distance, replaceDistance)
		matches += matches2
		content2 = content3

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'distance', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'distance', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replaceSurface(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.surface, replaceSurface)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'surface', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'surface', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replacePressure(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.pressure, replacePressure)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'pressure', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'pressure', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replaceWeight(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.weight, replaceWeight)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'weight', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'weight', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replaceVolume(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.volume, replaceVolume)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'volume', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'volume', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replaceForce(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.force, replaceForce)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'force', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'force', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replacePower(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.power, replacePower)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'power', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'power', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replaceWork(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.work, replaceWork)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'work', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'work', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replaceSpeed(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.speed, replaceSpeed)

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'speed', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'speed', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	async replaceTemperature(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let [content2, matches] = this.replaceUnits(content, NoFreedomUnits.temperature, replaceTemperature)
		let [content3, matches2] = this.replaceUnits(content2, NoFreedomUnits.temperature2, replaceTemperature2)

		matches += matches2

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'temperature', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'temperature', matches)

			return [content3, matches, server, user, true]
		}

		return [content, matches2, null, null, wasTyping]
	}

	async replaceTime(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		let matches = 0, content2 = content

		// 9 AM
		content2 = content2.replace(/(^|\s)([0-9]+)\s*AM(\s|$|,|\.)/gi, (substr, prep, num1, space) => {
			const num = Number(num1)

			if (num > 0 && num < 13) {
				matches++

				if (num == 12) {
					return prep + '**00:00**' + space
				}

				return prep + '**' + twon(num) + ':00**' + space
			}

			return substr
		})

		// 9:05 AM
		content2 = content2.replace(/(^|\s)([0-9]+):([0-9]+)\s*AM(\s|$|,|\.)/gi, (substr, prep, num1, num2, space) => {
			const num = Number(num1)
			const num_ = Number(num2)

			if (num > 0 && num < 13 && num_ >= 0 && num_ <= 60) {
				matches++

				if (num == 12) {
					return prep + '**00:' + twon(num_) + '**' + space
				}

				return prep + '**' + twon(num) + ':' + twon(num_) + '**' + space
			}

			return substr
		})

		// 9 PM
		content2 = content2.replace(/(^|\s)([0-9]+)\s*PM(\s|$|,|\.)/gi, (substr, prep, num1, space) => {
			const num = Number(num1)

			if (num > 0 && num < 12) {
				matches++

				if (num == 12) {
					return prep + '**12:00**' + space
				}

				return prep + '**' + twon(num + 12) + ':00**' + space
			}

			return substr
		})

		// 9:05 PM
		content2 = content2.replace(/(^|\s)([0-9]+):([0-9]+)\s*PM(\s|$|,|\.)/gi, (substr, prep, num1, num2, space) => {
			const num = Number(num1)
			const num_ = Number(num2)

			if (num > 0 && num < 12 && num_ >= 0 && num_ <= 60) {
				matches++

				if (num == 12) {
					return prep + '**12:' + twon(num_) + '**' + space
				}

				return prep + '**' + twon(num + 12) + ':' + twon(num_) + '**' + space
			}

			return substr
		})

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = msg.guild == null ? null : await this.updateStats('metric_server', msg.guild.id, 'time', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'time', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	static timezoneInsanity: [RegExp, string][] = [
		[/(^|\s)PST(\s|$)/g, '8'],
		[/(^|\s)Pacific Standard Time(\s|$)/ig, '8'],
		[/(^|\s)PDT(\s|$)/g, '7'],
		[/(^|\s)Pacific Daylight Time(\s|$)/ig, '7'],

		[/(^|\s)MST(\s|$)/g, '7'],
		[/(^|\s)Mountain Standard Time(\s|$)/ig, '7'],
		[/(^|\s)MDT(\s|$)/g, '6'],
		[/(^|\s)Mountain Daylight Time(\s|$)/ig, '6'],

		[/(^|\s)CST(\s|$)/g, '6'],
		[/(^|\s)Central Standard Time(\s|$)/ig, '6'],
		[/(^|\s)CDT(\s|$)/g, '5'],
		[/(^|\s)Central Daylight Time(\s|$)/ig, '5'],

		[/(^|\s)EST(\s|$)/g, '5'],
		[/(^|\s)Eastern Standard Time(\s|$)/ig, '5'],
		[/(^|\s)EDT(\s|$)/g, '4'],
		[/(^|\s)Eastern Daylight Time(\s|$)/ig, '4'],
	]

	async replaceTimezone(content: string, msg: Discord.Message, wasTyping = false): Promise<[string, number, number | null, number | null, boolean]> {
		const date = new Date()
		let doAdvance = date.getUTCMonth() < 7 && date.getUTCMonth() > 1

		if (doAdvance) {
			if (date.getUTCMonth() == 2) { // march
				const week = getWeekOfMonth(date, true)

				if (week == 1 || week < 3 && date.getUTCDay() == 6) {
					doAdvance = false
				}
			} else if (date.getUTCMonth() == 6) { // september
				const week = getWeekOfMonth(date, true)

				if (week > 1 || date.getUTCDay() == 6) {
					doAdvance = false
				}
			}
		}

		let matches = 0, content2 = content

		// time
		// 20:00 PT/PST/PDT
		content2 = content2.replace(/\*\*([0-9]+):([0-9]+)\*\*\s+P[SD]?T(\s|$|,|\.)/gi, (substr, num1, num2, space) => {
			matches++
			return `**${num1}:${num2} UTC-${doAdvance ? '7' : '8'}${putNormalUTC(Number(num1), num2, doAdvance ? -7 : -8)}**${space}`
		})

		// 20:00 MT/MNT/MST/MDT
		content2 = content2.replace(/\*\*([0-9]+):([0-9]+)\*\*\s+M[NSD]?T(\s|$|,|\.)/gi, (substr, num1, num2, space) => {
			matches++
			return `**${num1}:${num2} UTC-${doAdvance ? '6' : '7'}${putNormalUTC(Number(num1), num2, doAdvance ? -6 : -7)}**${space}`
		})

		// 20:00 CT/CST/CDT
		content2 = content2.replace(/\*\*([0-9]+):([0-9]+)\*\*\s+C[SD]?T(\s|$|,|\.)/gi, (substr, num1, num2, space) => {
			matches++
			return `**${num1}:${num2} UTC-${doAdvance ? '5' : '6'}${putNormalUTC(Number(num1), num2, doAdvance ? -5 : -6)}**${space}`
		})

		// 20:00 ET/EST/EDT
		content2 = content2.replace(/\*\*([0-9]+):([0-9]+)\*\*\s+E[SD]?T(\s|$|,|\.)/gi, (substr, num1, num2, space) => {
			matches++
			return `**${num1}:${num2} UTC-${doAdvance ? '4' : '5'}${putNormalUTC(Number(num1), num2, doAdvance ? -4 : -5)}**${space}`
		})

		// regular names
		for (const [regexp, replace] of NoFreedomUnits.timezoneInsanity) {
			content2 = content2.replace(regexp, (substr, space1, space2) => {
				matches++
				return `${space1}**UTC-${replace}**${space2}`
			})
		}

		if (matches > 0) {
			if (!wasTyping) {
				msg.channel.startTyping()
			}

			const server = await this.updateStats('metric_server', msg.guild?.id, 'timezone', matches)
			const user = await this.updateStats('metric_user', msg.author.id, 'timezone', matches)

			return [content2, matches, server, user, true]
		}

		return [content, matches, null, null, wasTyping]
	}

	static normalizeNumbers = (function(input: string[]) {
		const output = []

		for (const i in input) {
			const name = input[i]
			const num = String(Number(i) + 1)
			const rep1 = name.replace('-', '')
			const rep2 = name.replace('-', ' ')

			output.push({
				replace: new RegExp('^' + name, 'gi'),
				with: num
			})

			output.push({
				replace: new RegExp('\\s' + name, 'gi'),
				with: ' ' + num
			})

			if (rep1 != name) {
				output.push({
					replace: new RegExp('^' + rep1, 'gi'),
					with: num
				})

				output.push({
					replace: new RegExp('\\s' + rep1, 'gi'),
					with: ' ' + num
				})
			}

			if (rep2 != name) {
				output.push({
					replace: new RegExp('^' + rep2, 'gi'),
					with: num
				})

				output.push({
					replace: new RegExp('\\s' + rep2, 'gi'),
					with: ' ' + num
				})
			}
		}

		return output
	})([
		'one',
		'two',
		'three',
		'four',
		'five',
		'six',
		'seven',
		'eight',
		'nine',
		'ten',
		'eleven',
		'twelve',
		'thirteen',
		'fourteen',
		'fifteen',
		'sixteen',
		'seventeen',
		'eighteen',
		'nineteen',
		'twenty',
		'twenty-one',
		'twenty-two',
		'twenty-three',
		'twenty-four',
		'twenty-five',
		'twenty-six',
		'twenty-seven',
		'twenty-eight',
		'twenty-nine',
		'thirty',
		'thirty-one',
		'thirty-two',
		'thirty-three',
		'thirty-four',
		'thirty-five',
		'thirty-six',
		'thirty-seven',
		'thirty-eight',
		'thirty-nine',
		'forty',
		'forty-one',
		'forty-two',
		'forty-three',
		'forty-four',
		'forty-five',
		'forty-six',
		'forty-seven',
		'forty-eight',
		'forty-nine',
		'fifty',
		'fifty-one',
		'fifty-two',
		'fifty-three',
		'fifty-four',
		'fifty-five',
		'fifty-six',
		'fifty-seven',
		'fifty-eight',
		'fifty-nine',
		'sixty',
		'sixty-one',
		'sixty-two',
		'sixty-three',
		'sixty-four',
		'sixty-five',
		'sixty-six',
		'sixty-seven',
		'sixty-eight',
		'sixty-nine',
		'seventy',
		'seventy-one',
		'seventy-two',
		'seventy-three',
		'seventy-four',
		'seventy-five',
		'seventy-six',
		'seventy-seven',
		'seventy-eight',
		'seventy-nine',
		'eighty',
		'eighty-one',
		'eighty-two',
		'eighty-three',
		'eighty-four',
		'eighty-five',
		'eighty-six',
		'eighty-seven',
		'eighty-eight',
		'eighty-nine',
		'ninety',
		'ninety-one',
		'ninety-two',
		'ninety-three',
		'ninety-four',
		'ninety-five',
		'ninety-six',
		'ninety-seven',
		'ninety-eight',
		'ninety-nine',
		'one hundred',
	]);

	protected _replace_chain: [(content: string, msg: Discord.Message, wasTyping?: boolean) => Promise<[string, number, number | null, number | null, boolean]>, string, string?][] = [
		[this.replaceForce, 'force'],
		[this.replaceSpeed, 'speed'],
		[this.replaceDistance, 'distance'],
		[this.replaceSurface, 'surface', 'area'],
		[this.replaceWeight, 'weight'],
		[this.replaceVolume, 'volume'],
		[this.replaceTemperature, 'temperature'],
		[this.replacePressure, 'pressure'],
		[this.replaceWork, 'work'],
		[this.replacePower, 'power'],
		[this.replaceTime, 'time'],
		[this.replaceTimezone, 'timezone'],
	]

	static remap_ = whut([
		['м', 'm'],
		['с', 'c'],
		['г', 'r'],
		['у', 'y'],
		['к', 'k'],
		['х', 'x'],
		['а', 'a'],
		['е', 'e'],
		['Μ', 'm'],
		['і', 'i'],
		['Ι', 'l'],
		['о', 'o'],
		['ο', 'o'],
		['р', 'p'],
	])

	static generic_normalizations = [
		[/[\u2060-\u2063]/gi, ''],
		[/\u180E/gi, ''],
		[/\uFEFF/gi, ''],
		[/[\u200B-\u200D]/gi, ''],
		[/one\s+in\s/gi, 'one i​n'],
		[/two\s+in\s/gi, 'two i​n'],
		[/three\s+in\s/gi, 'three i​n'],
		[/(-?[0-9]+[\.,][0-9]+|-?[0-9]+|-?[\.,][0-9]+|[0-9]+e[\-\+][0-9]+)\s+in\s+([a-z]+)/gi, '$1 i​n $2'],
		[/"([^"]+?)"/g, '"​$1​"'],
		[/'([^']+?)'/g, (substr: any, args: any) => {
			if (args.match(/^[0-9]+$/)) {
				return substr
			}

			return "'" + args + "'"
		}]
	]

	async isOptedIn(serverID: string) {
		return (await this.sql.query(`SELECT * FROM "metric_optin" WHERE "server" = '${serverID}'`)).rowCount != 0
	}

	async isOptedInChannel(channelID: string) {
		return (await this.sql.query(`SELECT * FROM "metric_optin_channel" WHERE "channel" = '${channelID}'`)).rowCount != 0
	}

	async onMessage(msg: Discord.Message, edit?: Discord.Message) {
		if (msg.content == '' || msg.author.bot || msg.author.id == this.bot.id
			|| (msg.guild && !(<Discord.TextChannel> msg.channel).permissionsFor(msg.guild.me!)?.has('SEND_MESSAGES'))) {
			return 'No permissions or nothing to replace or message is from a bot'
		}

		if (msg.guild && !(await this.isOptedIn(msg.guild.id)) && !(await this.isOptedInChannel(msg.channel.id))) {
			return 'Not opted in'
		}

		if (msg.content.match(/```[\s\S]+```/)) {
			return 'Message contain code block'
		}

		let normalizedContent = msg.content

		for (const [a, b] of NoFreedomUnits.generic_normalizations) {
			normalizedContent = normalizedContent.replace(<any> a, <any> b)
		}

		for (const data of NoFreedomUnits.normalizeNumbers) {
			normalizedContent = normalizedContent.replace(data.replace, data.with)
		}

		for (const [a, b] of NoFreedomUnits.remap_) {
			normalizedContent = normalizedContent.replace(a, b)
		}

		this.pending.push(msg)

		let contentChanged = normalizedContent
		let counts_user = <any> {}, counts_server = <any> {}, count_current = <any> {}, remap = <any> {}
		let is_typing = false

		for (const [fn, fn_name, fn_name2] of this._replace_chain) {
			let [fn_out, fn_count, fn_server, fn_user, fn_typing] = await fn.call(this, contentChanged, msg, is_typing)
			is_typing = <boolean> fn_typing
			contentChanged = fn_out
			count_current[fn_name] = fn_count
			counts_user[fn_name] = fn_user
			counts_server[fn_name] = fn_server
			remap[fn_name] = fn_name2 ? fn_name2 : fn_name
		}

		if (contentChanged == normalizedContent) {
			return 'contentChanged == normalizedContent'
		}

		if (!this.pending.includes(msg)) {
			if (is_typing) {
				msg.channel.stopTyping()
			}

			return 'message was removed before bot could answer'
		}

		this.bot.triggerActivity()

		if (contentChanged.length >= 1900) {
			if (is_typing) {
				msg.channel.stopTyping()
			}

			this.pending.splice(this.pending.indexOf(msg), 1)
			return 'Too long message'
		}

		const corrections: string[] = []

		for (const key in count_current) {
			if (count_current[key] != 0) {
				corrections.push(`${remap[key]} ${counts_user[key]} times`)
			}
		}

		let build_corrections

		if (corrections.length == 1) {
			build_corrections = corrections[0]
		} else {
			const pop = corrections.pop()!
			build_corrections = corrections.join(', at ') + ' and at ' + pop
		}

		let build = `${contentChanged.replace(/@everyone/g, '@ everyone').replace(/@here/, '@ here')}
*Freedom units: ${build_corrections}*`

		if (!msg.guild) {
			build = contentChanged
		}

		if (build.length >= 1999) {
			if (is_typing) {
				msg.channel.stopTyping()
			}

			this.pending.splice(this.pending.indexOf(msg), 1)

			return 'Too long message'
		}

		let message

		try {
			if (!edit) {
				message = <Discord.Message> await msg.channel.send(build)
			} else {
				message = edit
				await edit.edit(build)
			}
		} catch(err) {
			if (is_typing) {
				msg.channel.stopTyping()
			}

			this.pending.splice(this.pending.indexOf(msg), 1)
			console.error(err)
			return 'Failed to post a message'
		}

		if (!this.pending.includes(msg)) {
			message.delete()

			if (is_typing) {
				msg.channel.stopTyping()
			}

			return 'Message was removed after bot answered'
		}

		this.pending.splice(this.pending.indexOf(msg), 1)
		this.messages.set(msg.id, message)

		if (is_typing) {
			msg.channel.stopTyping()
		}

		return true
	}
}

export {NoFreedomUnits}
