
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Discord = require('discord.js')
import { BotInstance } from "../BotInstance";

interface PatternHolder {
	[key: string]: string
}

class AutoReactManager {
	perServer = new Map<string, PatternHolder>()
	perChannel = new Map<string, PatternHolder>()

	get sql() {
		return this.bot.sql
	}

	constructor(public bot: BotInstance) {
		bot.on('validMessage', (msg: Discord.Message) => {
			this.onMessage(msg)
		})

		bot.on('validMessageUpdate', (msg: Discord.Message, msg2: Discord.Message) => {
			this.onMessageEdit(msg, msg2)
		})

		bot.client.on('messageDelete', msg => {
			this.onMessageDelete(msg)
		})
	}

	async reactInChannel(channel: Discord.TextChannel, contents: string, msg?: Discord.Message) {
		const perms = channel.permissionsFor(channel.guild.me)

		if (!channel.guild.me.hasPermission('ADMINISTRATOR')) {
			if (perms && !perms.has('SEND_MESSAGES')) {
				return false
			}

			if (!channel.guild.me.hasPermission('SEND_MESSAGES')) {
				return false
			}
		}

		const iterate = this.perChannel.get(channel.id)!

		for (const pattern in iterate) {
			const reaction = iterate[pattern]

			if (contents.match(new RegExp(pattern, 'i'))) {
				if (!msg) {
					this.bot.triggerActivity()
					return await channel.send(reaction)
				} else {
					this.bot.triggerActivity()
					msg.edit(reaction)
				}

				return true
			}
		}

		return false
	}

	async reactInServer(channel: Discord.TextChannel, contents: string, msg?: Discord.Message) {
		const perms = channel.permissionsFor(channel.guild.me)

		if (!channel.guild.me.hasPermission('ADMINISTRATOR')) {
			if (perms && !perms.has('SEND_MESSAGES')) {
				return false
			}

			if (!channel.guild.me.hasPermission('SEND_MESSAGES')) {
				return false
			}
		}

		const iterate = this.perServer.get(channel.guild.id)!

		for (const pattern in iterate) {
			const reaction = iterate[pattern]

			if (contents.match(new RegExp(pattern, 'i'))) {
				if (!msg) {
					this.bot.triggerActivity()
					return await channel.send(reaction)
				} else {
					this.bot.triggerActivity()
					msg.edit(reaction)
				}

				return true
			}
		}

		return false
	}

	addPattern(server: Discord.Guild, pattern: string, react: string) {
		return new Promise((resolve, reject) => {
			try {
				new RegExp(pattern)
			} catch(err) {
				reject('Malformed regexp: ' + err)
				return
			}

			this.sql.query(`INSERT INTO "autoreact_text" VALUES (${server.id}, ${this.sql.escapeLiteral(JSON.stringify({[pattern]: react}))})
			ON CONFLICT ("server") DO UPDATE SET "patterns" = "autoreact_text"."patterns" || excluded."patterns"`)
			.then(values => {
				const obj = this.perServer.has(server.id) ? this.perServer.get(server.id)! : {}

				obj[pattern] = react
				this.perServer.set(server.id, obj)
				resolve('Pattern added successfully')
			})
		})
	}

	removePattern(server: Discord.Guild, pattern: string) {
		return new Promise((resolve, reject) => {
			this.sql.query(`UPDATE "autoreact_text" SET "patterns" = "patterns" - ${this.sql.escapeLiteral(pattern)} WHERE "server" = ${server.id}`)
			.then(values => {
				const obj = this.perServer.has(server.id) ? this.perServer.get(server.id)! : {}
				delete obj[pattern]
				this.perServer.set(server.id, obj)
				resolve('Pattern removed successfully')
			})
		})
	}

	addChannelPattern(channel: Discord.TextChannel, pattern: string, react: string) {
		return new Promise((resolve, reject) => {
			try {
				new RegExp(pattern)
			} catch(err) {
				reject('Malformed regexp: ' + err)
				return
			}

			this.sql.query(`INSERT INTO "autoreact_text_channel" VALUES (${channel.id}, ${this.sql.escapeLiteral(JSON.stringify({[pattern]: react}))})
			ON CONFLICT ("channel") DO UPDATE SET "patterns" = "autoreact_text_channel"."patterns" || excluded."patterns"`)
			.then(values => {
				const obj = this.perChannel.has(channel.id) ? this.perChannel.get(channel.id)! : {}
				obj[pattern] = react
				this.perChannel.set(channel.id, obj)
				resolve('Pattern added successfully')
			})
		})
	}

	removeChannelPattern(channel: Discord.TextChannel, pattern: string) {
		return new Promise((resolve, reject) => {
			this.sql.query(`UPDATE "autoreact_text_channel" SET "patterns" = "patterns" - ${this.sql.escapeLiteral(pattern)} WHERE "channel" = ${channel.id}`)
			.then(values => {
				const obj = this.perChannel.has(channel.id) ? this.perChannel.get(channel.id)! : {}
				delete obj[pattern]
				this.perChannel.set(channel.id, obj)
				resolve('Pattern removed successfully')
			})
		})
	}

	resolveServer(server: Discord.Guild): Promise<PatternHolder> {
		return new Promise((resolve, reject) => {
			if (!this.perServer.has(server.id)) {
				this.sql.query(`SELECT "patterns" FROM "autoreact_text" WHERE "server" = ${server.id}`)
				.then(values => {
					this.perServer.set(server.id, values.rows[0] && values.rows[0].patterns || {})
					resolve(this.perServer.get(server.id)!)
				}).catch(reject)

				this.perServer.set(server.id, {})
			} else {
				resolve(this.perServer.get(server.id)!)
			}
		})
	}

	resolveChannel(channel: Discord.TextChannel): Promise<PatternHolder> {
		return new Promise((resolve, reject) => {
			if (!this.perChannel.has(channel.id)) {
				this.sql.query(`SELECT "patterns" FROM "autoreact_text_channel" WHERE "channel" = ${channel.id}`)
				.then(values => {
					this.perChannel.set(channel.id, values.rows[0] && values.rows[0].patterns || {})
					resolve(this.perChannel.get(channel.id)!)
				}).catch(reject)

				this.perChannel.set(channel.id, {})
			} else {
				resolve(this.perChannel.get(channel.id)!)
			}
		})
	}

	reacts = new Map<string, Discord.Message>()

	async onMessage(msg: Discord.Message) {
		if (!msg.guild) {
			return
		}

		await this.resolveChannel(<Discord.TextChannel> msg.channel)
		await this.resolveServer(msg.guild)

		let reply = await this.reactInServer(<Discord.TextChannel> msg.channel, msg.content)
		let reply2 = await this.reactInChannel(<Discord.TextChannel> msg.channel, msg.content)

		if (reply) {
			this.reacts.set(msg.id, <Discord.Message> reply)
		}

		if (reply2) {
			this.reacts.set(msg.id, <Discord.Message> reply2)
		}
	}

	async onMessageEdit(oldmsg: Discord.Message, msg: Discord.Message) {
		if (!this.reacts.has(msg.id)) {
			if (msg.createdTimestamp + 60000 > Date.now()) {
				this.onMessage(msg)
			}

			return
		}

		if (oldmsg.content == msg.content) {
			return
		}

		await this.resolveChannel(<Discord.TextChannel> msg.channel)
		await this.resolveServer(msg.guild)

		let status = await this.reactInServer(<Discord.TextChannel> msg.channel, msg.content, this.reacts.get(msg.id))
		status = await this.reactInChannel(<Discord.TextChannel> msg.channel, msg.content, this.reacts.get(msg.id)) || status

		if (!status) {
			this.reacts.get(msg.id)!.delete()
			this.reacts.delete(msg.id)
		}
	}

	onMessageDelete(msg: Discord.Message) {
		if (!this.reacts.has(msg.id)) {
			return
		}

		this.reacts.get(msg.id)!.delete().catch(console.error)
		this.reacts.delete(msg.id)
	}
}

export {AutoReactManager}
