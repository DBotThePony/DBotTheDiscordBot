

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import { BotInstance } from "../BotInstance"

const Statuses = [
	'Team Fortress 2',
	'Garry\'s Mod',
	'Space Engineers',
	'World of Goo',
	'FlatOut 2',
	'Node.JS',
	'Minecraft',
	'Rome: Total War I',
	'Settlers II: Vikings',
	'Star Wars: Empire At War',
	'Worms: Ultimate Mayhem',
	'SpellForce 2: Gold Edition',
	'Nuclear Dawn',
	'Infinifactory',
	'Killing Floor',
	'Killing Floor 2',
	'Portal 2',
	'Distance',
	'Terraria',
	'Starbound',
	'Fallout: New Vegas',
	'Torchlight',
	'Torchlight II',
	'Cinematic Mod 2013',
	'BeamNG.drive',
	'Counter-Strike: Source',
	'Counter-Strike: Global Offensive',
	'Left 4 Dead 2',
	'King\'s Bounty: Crossworlds',
	'Castle Crashers',
	'The Bridge',
	'Plague Inc: Evolved',
	'Medieval Engineers',
	'Super Hexagon',
	'Borderlands 2',
	'King\'s Bounty: The Legend',
	'Crashday: Redline Edition',
	'Factorio',
	'7 Days to Die',
]

function round(num: number) {
	const half = num % 1

	if (half < 0.5) {
		return num - half
	}

	return num + 1 - half
}

const RegisterStatusWatchdog = (bot: BotInstance) => {
	const UpdateStatus = () => {
		if (Math.random() < 0.3) {
			const status = Statuses[Math.floor(Math.random() * (Statuses.length - 1))]
			bot.client.user?.setActivity(status, {type: 'PLAYING'})
			setTimeout(UpdateStatus, (Math.random() * 1600 + 300) * 1000)
			return
		}

		const rand = round(Math.random() * 2)

		switch (rand) {
			case 0:
				bot.client.user?.setActivity(`${bot.client.users.cache.size} users`, {type: 'WATCHING'})
				break
			case 1:
				bot.client.user?.setActivity(`${bot.client.guilds.cache.size} servers`, {type: 'WATCHING'})
				break
			case 2:
				let num = 0

				for (const guild of bot.client.guilds.cache.values()) {
					num += guild.members.cache.size
				}

				bot.client.user?.setActivity(`${num} total members in all servers`, {type: 'WATCHING'})
				break
		}

		setTimeout(UpdateStatus, (Math.random() * 120 + 120) * 1000)
	}

	UpdateStatus()
}

export {RegisterStatusWatchdog}
