
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Discord = require('discord.js')
import { BotInstance } from "../BotInstance";
import { AsyncDBArrayHolder } from "../../lib/AsyncDBArrayHolder";
import { AsyncDBHashMap } from '../../lib/AsyncDBHashMap';

class PermanentStuff {
	permaRoles = new AsyncDBArrayHolder(this.bot, 'perma_roles', 'server', 'user', 'roles').disableCache(true).castToBigInt()
	permaNicknames = new AsyncDBHashMap<string>(this.bot, 'perma_nicknames', 'server', 'user', 'nickname')

	constructor(public bot: BotInstance) {
		bot.client.on('guildMemberAdd', (member) => {
			this.guildMemberAdd(member)
		})

		bot.client.on('guildMemberUpdate', (oldMember, newMember) => {
			this.prcoessNickname(newMember)
		})
	}

	processRoles(member: Discord.GuildMember) {
		if (!member.guild.me || !member.guild.me.hasPermission('MANAGE_ROLES')) {
			return
		}

		this.permaRoles.load(member.guild.id, member.id).then((result) => {
			for (const role of result) {
				const getrole = member.guild.roles.cache.get(role)

				if (getrole && !member.roles.cache.has(role)) {
					member.roles.add(getrole, 'Perma role from database.')
				}
			}
		})
	}

	prcoessNickname(member: Discord.GuildMember) {
		if (!member.guild.me || !member.guild.me.hasPermission('MANAGE_NICKNAMES')) {
			return
		}

		this.permaNicknames.get(member.guild.id, member.id).then((result) => {
			if (!result) {
				return
			}

			if (member.nickname == result) {
				return
			}

			member.setNickname(result, 'Perma nickname from database.')
		})
	}

	guildMemberAdd(member: Discord.GuildMember) {
		this.processRoles(member)
		this.prcoessNickname(member)
	}

	setNickname(guild: Discord.Guild, user: Discord.User | string, nickname: string): Promise<string> {
		nickname = nickname.trim()

		if (nickname == '') {
			return this.removeNickname(guild, user)
		}

		return new Promise((resolve, reject) => {
			if (!guild.me || !guild.me.hasPermission('MANAGE_NICKNAMES')) {
				reject('Bot lack MANAGE_NICKNAMES permission')
				return
			}

			const member = guild.member(user)

			if (!member) {
				this.permaNicknames.set(nickname, nickname, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Nickname written to database successfully')
				}).catch(reject)

				return
			}

			this.permaNicknames.set(nickname, nickname, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
				member.setNickname(nickname, 'Perma nickname addition').then(() => {
					resolve('Nickname set and written to database successfully')
				}).catch(reject)
			}).catch(() => {
				member.setNickname('', 'Perma nickname removal').then(() => {
					resolve('Nickname added to database successfully, but failed to user')
				}).catch(reject)
			})
		})
	}

	removeNickname(guild: Discord.Guild, user: Discord.User | string): Promise<string> {
		return new Promise((resolve, reject) => {
			if (!guild.me || !guild.me.hasPermission('MANAGE_NICKNAMES')) {
				reject('Bot lack MANAGE_NICKNAMES permission')
				return
			}

			const member = guild.member(user)

			if (!member) {
				this.permaNicknames.remove(guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Nickname removed from database successfully')
				}).catch(reject)

				return
			}

			this.permaNicknames.remove(guild.id, typeof user == 'string' ? user : user.id).then((result) => {
				member.setNickname('', 'Perma nickname removal').then(() => {
					resolve('Nickname removed from user and database successfully')
				}).catch(reject)
			}).catch(() => {
				member.setNickname('', 'Perma nickname removal').then(() => {
					resolve('Nickname removed from database successfully, but failed from user')
				}).catch(reject)
			})
		})
	}

	addRole(guild: Discord.Guild, user: Discord.User | string, role: Discord.Role) {
		return new Promise((resolve, reject) => {
			if (!guild.me || !guild.me.hasPermission('MANAGE_ROLES')) {
				reject('Bot lack MANAGE_ROLES permission')
				return
			}

			const member = guild.member(user)

			if (!member) {
				this.permaRoles.add(role.id, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Role written to database successfully')
				}).catch(reject)

				return
			}

			if (member.roles.cache.has(role.id)) {
				this.permaRoles.add(role.id, guild.id, typeof user == 'string' ? user : user.id).then(resolve).catch(reject)
				return
			}

			member.roles.add(role, 'Perma role addition').then(() => {
				this.permaRoles.add(role.id, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Role added and written to database successfully')
				}).catch(reject)
			}).catch(() => {
				this.permaRoles.add(role.id, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Role written to database successfully, but addition on user failed')
				}).catch(reject)
			})
		})
	}

	removeRole(guild: Discord.Guild, user: Discord.User | string, role: Discord.Role) {
		return new Promise((resolve, reject) => {
			if (!guild.me || !guild.me.hasPermission('MANAGE_ROLES')) {
				reject('Bot lack MANAGE_ROLES permission')
				return
			}

			const member = guild.member(user)

			if (!member) {
				this.permaRoles.remove(role.id, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Role removed from database successfully')
				}).catch(reject)

				return
			}

			if (!member.roles.cache.has(role.id)) {
				this.permaRoles.remove(role.id, guild.id, typeof user == 'string' ? user : user.id).then(resolve).catch(reject)
				return
			}

			member.roles.remove(role, 'Perma role removal').then(() => {
				this.permaRoles.remove(role.id, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Role removed from user and database successfully')
				}).catch(reject)
			}).catch(() => {
				this.permaRoles.add(role.id, guild.id, typeof user == 'string' ? user : user.id).then((result) => {
					resolve('Role removed from database successfully, but removal on user failed')
				}).catch(reject)
			})
		})
	}

	listRole(guild: Discord.Guild, user: Discord.User | string) {
		return this.permaRoles.load(guild.id, typeof user == 'string' ? user : user.id)
	}
}

export {PermanentStuff}
