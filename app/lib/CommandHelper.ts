

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import {BotInstance} from '../BotInstance'
import Discord = require('discord.js')
import _fs = require('fs')
import fs = _fs.promises
import { EMOJI_FUNCS } from './Emoji';

const imgExt = /.(jpe?g|png|bpg|tiff|bmp)/
const imgExtGif = /.(jpe?g|png|bpg|tiff|bmp|gif)/
const urlMatch = /(https?:[^ ]+)($| )/

class CommandHelper {
	url_history = new Map<string, string>()
	image_history = new Map<string, string>()
	image_history2 = new Map<string, string>()

	constructor(public bot: BotInstance) {}

	static unicodeRanges = [
		/[\u0300-\u036F]/g,
		/[\u1AB0-\u1ABE]/g,
		/[\u1DC0-\u1DFF]/g,
		/[\u20D0-\u20F0]/g,
		/[\uFE20-\uFE2F]/g,
	]

	onMessage(msg: Discord.Message) {
		const raw = msg.content
		const findURL = raw.match(urlMatch)

		if (findURL) {
			const url = findURL[0]
			this.url_history.set(msg.channel.id, url)
			const findIMG = url.match(imgExt)

			if (findIMG) {
				this.image_history.set(msg.channel.id, url)
			}

			const findIMG2 = url.match(imgExtGif)

			if (findIMG2) {
				this.image_history2.set(msg.channel.id, url)
			}
		} else {
			for (const attach of msg.attachments.values()) {
				const url = attach.url

				this.url_history.set(msg.channel.id, url)
				const findIMG = url.match(imgExt)

				if (findIMG) {
					this.image_history.set(msg.channel.id, url)
				}

				const findIMG2 = url.match(imgExtGif)

				if (findIMG2) {
					this.image_history2.set(msg.channel.id, url)
				}
			}
		}
	}

	lastImage(inputArg: Discord.Message | Discord.TextBasedChannelFields) {
		if (inputArg instanceof Discord.Message) {
			return this.image_history.get(inputArg.channel.id) || null
		} else if (inputArg instanceof Discord.DMChannel) {
			return this.image_history.get(inputArg.id) || null
		} else if (inputArg instanceof Discord.TextChannel) {
			return this.image_history.get(inputArg.id) || null
		}

		return null
	}

	static matchCustomImage = /^<:[^:]+:([0-9]+)>$/

	findImageString(arg: string) {
		const matchurl = arg.match(urlMatch)

		//if (matchurl && matchurl[0].match(imgExt)) {
		if (matchurl) {
			return matchurl[0]
		}

		const matchSmile = arg.match(CommandHelper.matchCustomImage)

		if (matchSmile) {
			return `https://cdn.discordapp.com/emojis/${matchSmile[1]}.png?v=1`
		}

		const matchEmoji = EMOJI_FUNCS.match(arg)

		if (matchEmoji) {
			return matchEmoji
		}

		return null
	}

	findImage(inputArg: Discord.Message | Discord.TextBasedChannelFields, arg: any): string | null {
		if (typeof arg == 'string' && arg.trim() != '') {
			return this.findImageString(arg.trim())
		}

		if (typeof arg == 'object') {
			if (arg instanceof Discord.User) {
				return arg.avatarURL()
			}

			if (arg instanceof Discord.GuildMember) {
				return arg.user.avatarURL()
			}

		}

		return this.lastImage(inputArg)
	}

	async getImagePath(urlIn: string) {
		return await this.bot.downloadIntoCache(urlIn)
	}

	async getImageContents(urlIn: string): Promise<Buffer> {
		return await fs.readFile(await this.bot.downloadIntoCache(urlIn))
	}

	getImmunityLevel(member: Discord.GuildMember) {
		// :) i made this
		return this.bot.config.owners.includes(member.id) ? 99999 : member.roles.highest.position
	}

	fuckOffZalgoText(input: string) {
		return CommandHelper.fuckOffZalgoText(input)
	}

	static fuckOffZalgoText(input: string) {
		for (const exp of this.unicodeRanges) {
			input = input.replace(exp, '')
		}

		return input
	}
}

export {CommandHelper, imgExt, imgExtGif, urlMatch}
