
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import {ConfigInstance} from '../app/ConfigInstance'

let config: ConfigInstance

try {
	config = new ConfigInstance(require('../config.js'))
} catch(err) {
	console.error('---------------------------------------')
	console.error('FATAL: Unable to load config file')
	console.error('---------------------------------------')
	console.error(err)
	process.exit(1)
}

import pg = require('pg')

const sql = `
ALTER TABLE "metric_server"
ADD COLUMN
	"power" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN
	"force" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN
	"work" INTEGER NOT NULL DEFAULT 0;

ALTER TABLE "metric_user"
ADD COLUMN
	"power" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN
	"force" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN
	"work" INTEGER NOT NULL DEFAULT 0;
`

const nosql = `
ALTER TABLE "metric_server" DROP COLUMN "power" DROP COLUMN "work" DROP COLUMN "force";
ALTER TABLE "metric_user" DROP COLUMN "power" DROP COLUMN "work" DROP COLUMN "force";
`

async function getdb() {
	if (!config.isValidSQL()) {
		throw new Error('Config instance has no valid sql config')
	}

	const sqlConfig = config.getSQL()

	if (!sqlConfig) {
		throw new Error('Config instance has no valid sql config')
	}

	const db = await new pg.Client(sqlConfig)
	await db.connect()

	return db
}

module.exports.up = async function(next: () => void) {
	try {
		await (await getdb()).query(sql)
	} catch(err) {
		console.error(err)
		console.error(sql)
		throw err
	}
}

module.exports.down = async function(next: () => void) {
	try {
		await (await getdb()).query(nosql)
	} catch(err) {
		console.error(err)
		console.error(nosql)
		throw err
	}
}
