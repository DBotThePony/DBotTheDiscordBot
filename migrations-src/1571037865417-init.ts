
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import {ConfigInstance} from '../app/ConfigInstance'

let config: ConfigInstance

try {
	config = new ConfigInstance(require('../config.js'))
} catch(err) {
	console.error('---------------------------------------')
	console.error('FATAL: Unable to load config file')
	console.error('---------------------------------------')
	console.error(err)
	process.exit(1)
}

import pg = require('pg')

const sql = `
CREATE TABLE "roleplay" (
	"actor" BIGINT NOT NULL,
	"target" BIGINT NOT NULL,
	"action" SMALLINT NOT NULL,
	"times" INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY ("actor", "target", "action")
);

CREATE TABLE "roleplay_generic" (
	"actor" BIGINT NOT NULL,
	"action" SMALLINT NOT NULL,
	"times" INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY ("actor", "action")
);

CREATE TABLE "server_colors" (
	"server" BIGINT NOT NULL,
	"colors" BIGINT[] NOT NULL,
	PRIMARY KEY ("server")
);

CREATE TABLE "shipping" (
	"first" BIGINT NOT NULL,
	"second" BIGINT NOT NULL,
	"times" INTEGER NOT NULL DEFAULT 1,
	PRIMARY KEY ("first", "second")
);

CREATE TABLE "command_ban_channel" (
	"server" BIGINT NOT NULL,
	"channel" BIGINT NOT NULL,
	"commands" VARCHAR(255)[] DEFAULT ARRAY[]::VARCHAR(255)[],
	PRIMARY KEY ("server", "channel")
);

CREATE TABLE "keywords_log" (
	"server" BIGINT NOT NULL,
	"channel" BIGINT NOT NULL,
	PRIMARY KEY ("server")
);

CREATE TABLE "keywords_whitelist" (
	"server" BIGINT NOT NULL,
	"channels" BIGINT[] NOT NULL DEFAULT ARRAY[]::BIGINT[],
	PRIMARY KEY ("server")
);

CREATE TABLE "keywords_ban_channel" (
	"server" BIGINT NOT NULL,
	"channel" BIGINT NOT NULL,
	"keywords" VARCHAR(255)[] DEFAULT ARRAY[]::VARCHAR(255)[],
	PRIMARY KEY ("server", "channel")
);

CREATE TABLE "keywords_ban_user_channel" (
	"server" BIGINT NOT NULL,
	"channel" BIGINT NOT NULL,
	"user" BIGINT NOT NULL,
	"keywords" VARCHAR(255)[] DEFAULT ARRAY[]::VARCHAR(255)[],
	PRIMARY KEY ("server", "channel", "user")
);

CREATE TABLE "command_ban_server" (
	"server" BIGINT NOT NULL,
	"commands" VARCHAR(255)[] DEFAULT ARRAY[]::VARCHAR(255)[],
	PRIMARY KEY ("server")
);

CREATE TABLE "keywords_ban_server" (
	"server" BIGINT NOT NULL,
	"keywords" VARCHAR(255)[] DEFAULT ARRAY[]::VARCHAR(255)[],
	PRIMARY KEY ("server")
);

CREATE TABLE "keywords_ban_user_server" (
	"server" BIGINT NOT NULL,
	"user" BIGINT NOT NULL,
	"keywords" VARCHAR(255)[] DEFAULT ARRAY[]::VARCHAR(255)[],
	PRIMARY KEY ("server", "user")
);

CREATE TABLE "role_giver" (
	"server" BIGINT NOT NULL,
	"roles" BIGINT[] DEFAULT ARRAY[]::BIGINT[],
	PRIMARY KEY ("server")
);

CREATE TABLE "perma_roles" (
	"server" BIGINT NOT NULL,
	"user" BIGINT NOT NULL,
	"roles" BIGINT[] DEFAULT ARRAY[]::BIGINT[],
	PRIMARY KEY ("server", "user")
);

CREATE TABLE "perma_nicknames" (
	"server" BIGINT NOT NULL,
	"user" BIGINT NOT NULL,
	"nickname" VARCHAR(255) NOT NULL,
	PRIMARY KEY ("server", "user")
);

CREATE TABLE "autoreact_text" (
	"server" BIGINT NOT NULL,
	"patterns" JSONB NOT NULL,
	PRIMARY KEY ("server")
);

CREATE TABLE "autoreact_text_channel" (
	"channel" BIGINT NOT NULL,
	"patterns" JSONB NOT NULL,
	PRIMARY KEY ("channel")
);

CREATE TABLE "autoreact_edited" (
	"channel" BIGINT NOT NULL,
	"message" BIGINT NOT NULL,
	"count" SMALLINT NOT NULL DEFAULT -32767,
	PRIMARY KEY ("channel", "message")
);

CREATE TABLE "hangman_stats" (
	"user" BIGINT NOT NULL PRIMARY KEY,
	"games" INTEGER NOT NULL DEFAULT 0,
	"started" INTEGER NOT NULL DEFAULT 0,
	"stopped" INTEGER NOT NULL DEFAULT 0,
	"victories" INTEGER NOT NULL DEFAULT 0,
	"defeats" INTEGER NOT NULL DEFAULT 0,

	"guesses" INTEGER NOT NULL DEFAULT 0,
	"guesses_hits" INTEGER NOT NULL DEFAULT 0,
	"guesses_miss" INTEGER NOT NULL DEFAULT 0,

	"full_guesses" INTEGER NOT NULL DEFAULT 0,
	"full_guesses_hits" INTEGER NOT NULL DEFAULT 0,
	"full_guesses_miss" INTEGER NOT NULL DEFAULT 0,

	"length" INTEGER NOT NULL DEFAULT 0,
	"length_guess" INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE "steamid_cache" (
	"steamid" BIGINT NOT NULL PRIMARY KEY,
	"communityvisibilitystate" INTEGER NOT NULL DEFAULT 0,
	"profilestate" INTEGER NOT NULL DEFAULT 0,
	"personaname" VARCHAR(255) NOT NULL DEFAULT '<missing>',
	"lastlogoff" INTEGER NOT NULL DEFAULT 0,
	"profileurl" VARCHAR(255) NOT NULL DEFAULT '<missing>',
	"avatar" VARCHAR(255) NOT NULL DEFAULT '<missing>',
	"avatarmedium" VARCHAR(255) NOT NULL DEFAULT '<missing>',
	"avatarfull" VARCHAR(255) NOT NULL DEFAULT '<missing>',
	"personastate" INTEGER NOT NULL DEFAULT 0,
	"realname" VARCHAR(255) NOT NULL DEFAULT '<missing>',
	"primaryclanid" VARCHAR(255) NOT NULL DEFAULT '<missing>',
	"timecreated" BIGINT NOT NULL DEFAULT 0,
	"personastateflags" INTEGER NOT NULL DEFAULT 0,
	"loccountrycode" VARCHAR(15) NOT NULL DEFAULT '',
	"locstatecode" VARCHAR(15) NOT NULL DEFAULT '',
	"loccityid" INTEGER NOT NULL DEFAULT 0,

	"expires" BIGINT NOT NULL DEFAULT 0
);

CREATE TABLE "logging_servers" (
	"server" BIGINT NOT NULL,
	"options" JSONB NOT NULL DEFAULT '[]',
	PRIMARY KEY ("server")
);

CREATE TABLE "voice_role" (
	"channel" BIGINT NOT NULL,
	"role" BIGINT[] NOT NULL DEFAULT '{}'::BIGINT[],
	PRIMARY KEY ("channel")
);`

const nosql = `
DROP TABLE "roleplay";
DROP TABLE "roleplay_generic";
DROP TABLE "server_colors";
DROP TABLE "shipping";
DROP TABLE "command_ban_channel";
DROP TABLE "keywords_log";
DROP TABLE "keywords_whitelist";
DROP TABLE "keywords_ban_channel";
DROP TABLE "keywords_ban_user_channel";
DROP TABLE "command_ban_server";
DROP TABLE "keywords_ban_server";
DROP TABLE "keywords_ban_user_server";
DROP TABLE "role_giver";
DROP TABLE "perma_roles";
DROP TABLE "perma_nicknames";
DROP TABLE "autoreact_text";
DROP TABLE "autoreact_text_channel";
DROP TABLE "autoreact_edited";
DROP TABLE "hangman_stats";
DROP TABLE "steamid_cache";
DROP TABLE "logging_servers";
DROP TABLE "voice_role";
`

async function getdb() {
	if (!config.isValidSQL()) {
		throw new Error('Config instance has no valid sql config')
	}

	const sqlConfig = config.getSQL()

	if (!sqlConfig) {
		throw new Error('Config instance has no valid sql config')
	}

	const db = await new pg.Client(sqlConfig)
	await db.connect()

	return db
}

module.exports.up = async function(next: () => void) {
	try {
		await (await getdb()).query(sql)
	} catch(err) {
		console.error(err)
		console.error(sql)
		throw err
	}
}

module.exports.down = async function(next: () => void) {
	try {
		await (await getdb()).query(nosql)
	} catch(err) {
		console.error(err)
		console.error(nosql)
		throw err
	}
}
