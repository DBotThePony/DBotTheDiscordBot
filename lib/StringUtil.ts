

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


const ParseString = function(strIn: string) {
	const output: string[][] = []
	let currentLevel: string[] = []
	output.push(currentLevel)
	let currentString = ''
	let inSingle = false
	let inDouble = false
	let nextEscaped = false
	let prevChar = ''

	for (const val of strIn.trim()) {
		if (val == ' ' && !inSingle && !inDouble && !nextEscaped) {
			if (prevChar != ' ' && prevChar != '') {
				currentLevel.push(currentString)
				currentString = ''
				prevChar = val
			}
		} else if (val == '"') {
			if (inSingle || nextEscaped) {
				currentString += val
				nextEscaped = false
			} else if (inDouble) {
				inDouble = false
				currentLevel.push(currentString)
				currentString = ''
				prevChar = ''
			} else if (!inDouble && (prevChar == ' ' || prevChar == '')) {
				inDouble = true
				currentString = ''
				prevChar = ''
			} else { // ???
				currentString += val
			}
		} else if (val == '\'') {
			if (inDouble || nextEscaped) {
				currentString += val
				nextEscaped = false
			} else if (inSingle) {
				inSingle = false
				currentLevel.push(currentString)
				currentString = ''
				prevChar = ''
			} else if (!inSingle && (prevChar == ' ' || prevChar == '')) {
				inSingle = true
				currentString = ''
				prevChar = ''
			} else { // ???
				currentString += val
			}
		} else if (val == '\\') {
			nextEscaped = true
		} else if (val == '|' && !inDouble && !inSingle && !nextEscaped && (prevChar == ' ' || prevChar == '')) {
			currentLevel = []
			prevChar = ''
			output.push(currentLevel)
		} else {
			nextEscaped = false
			prevChar = val
			currentString += val
		}
	}

	if (currentString != '') {
		currentLevel.push(currentString)
	}

	return output
}

export {ParseString}
