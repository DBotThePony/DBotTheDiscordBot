

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import pg = require('pg')
import { BotInstance } from '../app/BotInstance'

class AsyncDBHashMap<T> {
	lastfield: string
	fields: string[]
	sqlfields: string
	length: number

	get sql() { return this.bot.sql }

	// the last field is the array
	constructor(public bot: BotInstance, public tableName: string, ...fields: string[]) {
		if (fields.length == 0) {
			throw new Error('None fields were specified')
		}

		if (fields.length == 1) {
			throw new TypeError('Can not use only one field (which is basically the table array)')
		}

		this.lastfield = fields.pop()!
		this.length = fields.length
		this.fields = fields
		this.sqlfields = '"' + fields.join('", "') + '"'
	}

	constructFields(...fields: string[]) {
		const construct = []

		for (const field of fields) {
			construct.push(this.sql.escapeLiteral(field))
		}

		return construct.join(', ')
	}

	constructWhere(...fields: string[]) {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		const construct = []
		let i = 0

		for (const field of fields) {
			construct.push(`"${this.fields[i]}" = ${this.sql.escapeLiteral(field)}`)
			i++
		}

		return construct.join(' AND ')
	}

	get(...fields: string[]): Promise<T | null> {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		return new Promise((resolve, reject) => {
			this.sql.query(`SELECT "${this.lastfield}" FROM "${this.tableName}" WHERE ${this.constructWhere(...fields)}`)
			.then((value) => {
				if (value.rowCount == 0) {
					resolve(null)
					return
				}

				resolve(value.rows[0][this.lastfield])
			})
			.catch(reject)
		})
	}

	set(value: T, escapedValue: string, ...fields: string[]) {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		return new Promise((resolve, reject) => {
			this.sql.query(`INSERT INTO "${this.tableName}" VALUES (${this.constructFields(...fields)},
				${this.sql.escapeLiteral(escapedValue)}) ON CONFLICT (${this.sqlfields}) DO UPDATE
				SET "${this.lastfield}" = excluded."${this.lastfield}"`)
			.then(() => {
				resolve('Successfull add to database')
			})
			.catch(reject)
		})
	}

	remove(...fields: string[]) {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		return new Promise((resolve, reject) => {
			const construct = []
			let i = 0

			for (const field of fields) {
				construct.push(`"${this.fields[i]}" = ${this.sql.escapeLiteral(field)}`)
				i++
			}

			this.sql.query(`DELETE FROM "${this.tableName}" WHERE ${this.constructWhere(...fields)}`)
			.then(() => {
				resolve('Successfull removal from database')
			})
			.catch(reject)
		})
	}
}

export {AsyncDBHashMap}
