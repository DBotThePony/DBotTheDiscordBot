

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


import pg = require('pg')
import { BotInstance } from '../app/BotInstance'

class AsyncDBArrayHolder {
	array: string
	raw_fields: string[]
	sql_fields: string
	length: number
	top_level = new Map<string, any>()
	cast_to_int = false
	cast_to_big_int = false
	disabled_cache = false

	get sql() { return this.bot.sql }

	// the last field is the array
	constructor(public bot: BotInstance, public tableName: string, ...fields: string[]) {
		if (fields.length == 0) {
			throw new Error('None fields were specified')
		}

		if (fields.length == 1) {
			throw new TypeError('Can not use only one field (which is basically the table array)')
		}

		this.length = fields.length - 1
		const array = fields.pop()!
		this.array = array
		this.raw_fields = fields
		this.sql_fields = '"' + fields.join('", "') + '"'
	}

	disableCache(disable = true) {
		this.disabled_cache = disable
		return this
	}

	get caster() {
		if (this.cast_to_int) {
			return '::INTEGER[]'
		} else if (this.cast_to_big_int) {
			return '::BIGINT[]'
		}

		return '::VARCHAR[]'
	}

	castToBigInt(doCast = true) {
		this.cast_to_big_int = doCast
		return this
	}

	castToInt(doCast = true) {
		this.cast_to_int = doCast
		return this
	}

	has(...fields: string[]) {
		return this.get(...fields) != null
	}

	get(...fields: string[]): string[] | null {
		if (this.disabled_cache) {
			throw new Error('Caching is disabled, therefore asyncGet should be used')
		}

		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		if (!this.top_level.has(fields[0])) {
			return null
		}

		if (this.length == 1) {
			return this.top_level.get(fields[0])!
		}

		let level = this.top_level.get(fields[0])

		for (let i = 1; i < this.length; i++) {
			if (!level.has(fields[i])) {
				return null
			}

			level = level.get(fields[i])
		}

		return <string[]> level
	}

	async asyncGet(...fields: string[]) {
		return await this.load(...fields)
	}

	constructWhere(...fields: string[]) {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		const construct = []
		let i = 0

		for (const field of fields) {
			construct.push(`"${this.raw_fields[i]}" = ${this.sql.escapeLiteral(field)}`)
			i++
		}

		return construct.join(' AND ')
	}

	async add(value: string, ...fields: string[]): Promise<string> {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		if (!this.disabled_cache) {
			if (!this.has(...fields)) {
				await this.load(...fields)
				return await this.add(value, ...fields)
			}

			if (this.hasValue(value, ...fields)) {
				throw new Error('Item is already in array')
			}
		} else {
			const arr = await this.asyncGet(...fields)

			if (arr.includes(value)) {
				throw new Error('Item is already in array')
			}
		}

		const construct = []

		for (const field of fields) {
			construct.push(this.sql.escapeLiteral(field))
		}

		await this.sql.query(`INSERT INTO "${this.tableName}" VALUES (${construct.join(', ')}, ARRAY[${this.sql.escapeLiteral(value)}]${this.caster}) ON CONFLICT (${this.sql_fields}) DO UPDATE
			SET "${this.array}" = "${this.tableName}"."${this.array}" || excluded."${this.array}"`)

		if (!this.disabled_cache) {
			this.get(...fields)!.push(value)
		}

		return 'Successfull add to database'

	}

	async bulkAdd(...values: string[]): Promise<[string, boolean, string][]> {
		if (values.length <= this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + values.length + '. Also you must provide at least one value to add')
		}

		const fields = values.splice(0, this.length)

		if (!this.disabled_cache && !this.has(...fields)) {
			await this.load(...fields)
			values.unshift(...fields)
			return await this.bulkAdd(...values)
		}

		const status: any[] = []

		await this.sql.query(`BEGIN`)

		for (const value of values) {
			try {
				status.push([value, true, await this.add(value, ...fields)])
			} catch(err) {
				status.push([value, false, err])
			}
		}

		await this.sql.query(`COMMIT`)
		return status
	}

	async bulkRemove(...values: string[]): Promise<[string, boolean, string][]> {
		if (values.length <= this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + values.length + '. Also you must provide at least one value to add')
		}

		const fields = values.splice(0, this.length)

		if (!this.disabled_cache && !this.has(...fields)) {
			await this.load(...fields)
			values.unshift(...fields)
			return await this.bulkRemove(...values)
		}

		const status: any[] = []
		await this.sql.query(`BEGIN`)

		for (const value of values) {
			try {
				status.push([value, true, await this.remove(value, ...fields)])
			} catch(err) {
				status.push([value, false, err])
			}
		}

		await this.sql.query(`COMMIT`)
		return status
	}

	async remove(value: string, ...fields: string[]): Promise<string> {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		if (!this.disabled_cache) {
			if (!this.has(...fields)) {
				await this.load(...fields)
				return await this.remove(value, ...fields)
			}

			if (!this.hasValue(value, ...fields)) {
				throw new Error('Item is already not in array')
			}
		} else {
			const arr = await this.asyncGet(...fields)

			if (!arr.includes(value)) {
				throw new Error('Item is already not in array')
			}
		}

		const construct = []
		let i = 0

		for (const field of fields) {
			construct.push(`"${this.raw_fields[i]}" = ${this.sql.escapeLiteral(field)}`)
			i++
		}

		await this.sql.query(`UPDATE "${this.tableName}" SET "${this.array}" = array_remove("${this.array}", ${this.sql.escapeLiteral(value)}) WHERE ${this.constructWhere(...fields)}`)

		if (!this.disabled_cache) {
			const arr = this.get(...fields)!
			arr.splice(arr.indexOf(value), 1)
		}

		return 'Successfull removal from database'
	}

	hasValue(value: string, ...fields: string[]) {
		const arr = this.get(...fields)

		if (arr == null) {
			return false
		}

		return arr.includes(value)
	}

	async load(...fields: string[]): Promise<string[]> {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		if (!this.disabled_cache && this.has(...fields)) {
			return this.get(...fields)!
		}

		const values = await this.sql.query(`SELECT "${this.array}" AS "res" FROM "${this.tableName}" WHERE ${this.constructWhere(...fields)}`)
		const arr: string[] = []

		if (!this.disabled_cache) {
			let level = this.top_level.get(fields[0])

			if (this.length != 1) {
				if (!level) {
					this.top_level.set(fields[0], new Map())
					level = this.top_level.get(fields[0])
				}

				for (let i = 1; i < this.length - 1; i++) {
					if (!level.has(fields[i])) {
						level.set(fields[i], new Map())
					}

					level = level.get(fields[i])
				}

				level.set(fields[fields.length - 1], arr)
			} else {
				this.top_level.set(fields[0], arr)
			}
		}

		for (const row of values.rows) {
			arr.push(...row.res)
		}

		return arr
	}

	async asyncHasValue(value: string, ...fields: string[]): Promise<boolean> {
		if (fields.length != this.length) {
			throw new TypeError('Invalid fields amount. Required exact ' + this.length + ' for constructing a patch but got ' + fields.length)
		}

		if (!this.disabled_cache && this.has(...fields)) {
			return this.hasValue(value, ...fields)
		}

		return (await this.load(...fields)).includes(value)
	}
}

export {AsyncDBArrayHolder}
