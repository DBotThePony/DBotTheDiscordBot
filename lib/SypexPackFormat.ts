
// Half-Life StatsX: Community Edition v2
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

// t    tinyint signed  1   Целое число от -128 до 127
// T    tinyint unsigned    1   Целое число от 0 до 255
// s    smallint signed     2   Целое число от -32 768 до 32 767
// S    smallint unsigned   2   Целое число от 0 до 65 535
// m    mediumint signed    3   Целое число от -8 388 608 до 8 388 607
// M    mediumint unsigned  3   Целое число от 0 до 16 777 215
// i    integer signed  4   Целое число от -2 147 483 648 до 2 147 483 647
// I    integer unsigned    4   Целое число от 0 до 4 294 967 295
// f    float   4   4-байтовое число одиночной точности с плавающей запятой
// d    double  8   8-байтовое число двойной точности с плавающей запятой
// n#   number 16bit    2   Число (2 байта) с фиксированным количеством знаков после запятой. После n указывается количество цифр после запятой
// N#   number 32bit    4   Число (4 байта) с фиксированным количеством знаков после запятой. После N указывается количество цифр после запятой
// c#   char    N   Строка фиксированного размера. После с указывается количество символов
// b    blob    N   Строка завершающаяся нулевым символов

import { MemoryMappedFile, MemoryMappedFilePointer } from "./MemoryMappedFile";
import { SypexDB } from "./SypexDB";

enum SypexDBPackValueType {
	TINY_SIGNED,
	TINY_UNSIGNED,
	SMALLINT_SIGNED,
	SMALLINT_UNSIGNED,
	MEDIUMINT_SIGNED,
	MEDIUMINT_UNSIGNED,
	INT_SIGNED,
	INT_UNSIGNED,
	FLOAT,
	DOUBLE,
	NUMBER_16BIT,
	NUMBER_32BIT,
	CHAR,
	BLOB,
}

const getTypeLength = (rawtype: string): [SypexDBPackValueType, number | null] => {
	switch (rawtype) {
		case 't':
			return [SypexDBPackValueType.TINY_SIGNED, 1]
		case 'T':
			return [SypexDBPackValueType.TINY_UNSIGNED, 1]
		case 's':
			return [SypexDBPackValueType.SMALLINT_SIGNED, 2]
		case 'S':
			return [SypexDBPackValueType.SMALLINT_UNSIGNED, 2]
		case 'm':
			return [SypexDBPackValueType.MEDIUMINT_SIGNED, 3]
		case 'M':
			return [SypexDBPackValueType.MEDIUMINT_UNSIGNED, 3]
		case 'i':
			return [SypexDBPackValueType.INT_SIGNED, 4]
		case 'I':
			return [SypexDBPackValueType.INT_UNSIGNED, 4]
		case 'f':
			return [SypexDBPackValueType.FLOAT, 4]
		case 'd':
			return [SypexDBPackValueType.DOUBLE, 4]
		case 'b':
			return [SypexDBPackValueType.BLOB, null]
		default:
			if (rawtype[0] == 'c') {
				return [SypexDBPackValueType.CHAR, Number(rawtype.substr(1))]
			} else if (rawtype[0] == 'n') {
				return [SypexDBPackValueType.NUMBER_16BIT, 2]
			} else if (rawtype[0] == 'N') {
				return [SypexDBPackValueType.NUMBER_32BIT, 4]
			} else {
				throw new Error('Unknown value type: ' + rawtype)
			}
	}
}

class SypexDBPackValue {
	fixedDecimals: number | null = null

	constructor(public rawtype: string, public type: SypexDBPackValueType, public length: number | null, public name: string) {
		switch (type) {
			case SypexDBPackValueType.NUMBER_16BIT:
			case SypexDBPackValueType.NUMBER_32BIT:
				this.fixedDecimals = Number(rawtype.substr(1))
				break
		}
	}

	numeric: number | null = null
	string: string | null = null

	async readFile(reader: MemoryMappedFilePointer, offset: number) {
		// И ещё веселее тут - у нас машинно зависимый порядок байт
		// мда
		// А это значит, что либо PHP скрипт не прокатит на big-endian процессорах
		// или база данных соберётся в упаковщике в обратном порядке (big-endian)
		switch (this.type) {
			case SypexDBPackValueType.TINY_SIGNED:
				this.numeric = await reader.nextInt8()
				break
			case SypexDBPackValueType.TINY_UNSIGNED:
				this.numeric = await reader.nextUInt8()
				break
			case SypexDBPackValueType.SMALLINT_SIGNED:
				this.numeric = await reader.nextInt16LE()
				break
			case SypexDBPackValueType.SMALLINT_UNSIGNED:
				this.numeric = await reader.nextUInt16LE()
				break
			case SypexDBPackValueType.MEDIUMINT_SIGNED:
				this.numeric = await reader.nextInt24LE()
				break
			case SypexDBPackValueType.MEDIUMINT_UNSIGNED:
				this.numeric = await reader.nextUInt24LE()
				break
			case SypexDBPackValueType.INT_SIGNED:
				this.numeric = await reader.nextInt32LE()
				break
			case SypexDBPackValueType.INT_UNSIGNED:
				this.numeric = await reader.nextUInt32LE()
				break
			case SypexDBPackValueType.FLOAT:
				this.numeric = await reader.nextFloat()
				break
			case SypexDBPackValueType.DOUBLE:
				this.numeric = await reader.nextDouble()
				break
			case SypexDBPackValueType.CHAR:
				this.string = await reader.nextChars(this.length!)
				break
			case SypexDBPackValueType.NUMBER_16BIT:
				this.numeric = await reader.nextInt16LE() / Math.pow(10, this.fixedDecimals!)
				break
			case SypexDBPackValueType.NUMBER_32BIT:
				this.numeric = await reader.nextInt32LE() / Math.pow(10, this.fixedDecimals!)
				break
			case SypexDBPackValueType.BLOB:
				this.string = await reader.nextString()
				break
			default:
				throw new TypeError('a?')
		}
	}
}

interface ITokenStorage {
	[0]: SypexDBPackValueType
	[1]: string
	[2]: number | null
	[3]: string
}

class SypexPackEntry {
	mapped = new Map<string, SypexDBPackValue>()

	constructor(public values: SypexDBPackValue[]) {
		for (const value of values) {
			this.mapped.set(value.name, value)
		}
	}

	get keys() {
		return this.mapped.keys()
	}

	has(name: string) {
		return this.mapped.has(name)
	}

	get(name: string) {
		return this.mapped.get(name) || null
	}

	getNumber(name: string) {
		if (!this.has(name)) {
			return null
		}

		return this.get(name)!.numeric
	}

	getString(name: string) {
		if (!this.has(name)) {
			return null
		}

		return this.get(name)!.string
	}
}

class SypexDBPack {
	tokensCity = new Map<string, ITokenStorage>()
	tokensRegion = new Map<string, ITokenStorage>()
	tokensCountry = new Map<string, ITokenStorage>()

	tokensCityOrdered: ITokenStorage[] = []
	tokensRegionOrdered: ITokenStorage[] = []
	tokensCountryOrdered: ITokenStorage[] = []

	get raw() {
		return this.db.packingRegionData
	}

	rawCityFormat: string
	rawRegionFormat: string
	rawCountryFormat: string

	static parsePack(format: string) {
		const reply: ITokenStorage[] = []
		const entries = format.split('/')

		for (const entry of entries) {
			const [type, name] = entry.split(':')
			const gettype = getTypeLength(type)
			reply.push([gettype[0], name, gettype[1], type])
		}

		return reply
	}

	static parsePackInto(arrIn: ITokenStorage[], arr: ITokenStorage[], map: Map<string, ITokenStorage>) {
		arr.push(...arrIn)

		for (const val of arrIn) {
			if (map.has(val[1])) {
				throw new TypeError('Pack-format contains duplicate value: ' + val[1])
			}

			map.set(val[1], val)
		}
	}

	get mmap() {
		return this.db.mmap
	}

	constructor(public db: SypexDB) {
		if (!db.packingRegionData) {
			throw new TypeError('You must read header first')
		}

		// Описание формата
		//const [city, region, country] = db.packingRegionData!.split('\0')
		// Реальная расстановка
		const [country, region, city] = db.packingRegionData!.split('\0')
		this.rawCityFormat = city
		this.rawRegionFormat = region
		this.rawCountryFormat = country

		//console.log(this.rawCityFormat)
		//console.log(this.rawRegionFormat)
		//console.log(this.rawCountryFormat)

		SypexDBPack.parsePackInto(SypexDBPack.parsePack(city), this.tokensCityOrdered, this.tokensCity)
		SypexDBPack.parsePackInto(SypexDBPack.parsePack(region), this.tokensRegionOrdered, this.tokensRegion)
		SypexDBPack.parsePackInto(SypexDBPack.parsePack(country), this.tokensCountryOrdered, this.tokensCountry)
	}

	async readFrom(offset: number, tokenStorage: ITokenStorage[]) {
		const tokens = []
		const reader = this.mmap.getNewPointer()
		reader.seek(offset)

		for (const token of tokenStorage) {
			const readtoken = new SypexDBPackValue(token[3], token[0], token[2], token[1])
			await readtoken.readFile(reader, offset)
			tokens.push(readtoken)
		}

		return new SypexPackEntry(tokens)
	}

	async readCity(offset: number) {
		return await this.readFrom(offset, this.tokensCityOrdered)
	}

	async readCountry(offset: number) {
		return await this.readFrom(offset, this.tokensCountryOrdered)
	}

	async readRegion(offset: number) {
		return await this.readFrom(offset, this.tokensRegionOrdered)
	}
}

export {SypexDBPack, SypexDBPackValue, SypexDBPackValueType, SypexPackEntry}
