
// Half-Life StatsX: Community Edition v2
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

const assert = (val: number) => {
	if (val != val) {
		throw new TypeError('Missing octet')
	}

	if (val < 1 || val > 255) {
		throw new RangeError('Invalid IP address octet: ' + val)
	}

	return val
}

const stringIsValidIP = (str: string) => {
	if (typeof str != 'string') {
		return false
	}

	const match = str.match(SypexIPAddress.matcher)

	if (!match) {
		return false
	}

	try {
		assert(Number(match[1]))
		assert(Number(match[2]))
		assert(Number(match[3]))
		assert(Number(match[4]))
	} catch(err) {
		return false
	}

	return true
}

export {stringIsValidIP}

class SypexIPAddress {
	static matcher = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/
	strvalue: string
	numvalue: number

	first: number
	second: number
	third: number
	fourth: number

	valid = false

	constructor(value: string | number) {
		if (typeof value == 'string') {
			const match = value.match(SypexIPAddress.matcher)

			if (match) {
				this.strvalue = value
				this.first = assert(Number(match[1]))
				this.second = assert(Number(match[2]))
				this.third = assert(Number(match[3]))
				this.fourth = assert(Number(match[4]))

				this.numvalue = this.first * 0x1000000 + this.second * 0x10000 + this.third * 0x100 + this.fourth
				this.valid = true
			} else {
				throw new TypeError('Invalid IP address input: ' + value)
			}
		} else if (typeof value == 'number') {
			if (value < 0x1000001) {
				throw new RangeError('0.x.x.x? - ' + value)
			}

			if (value >= 0x100000000) {
				throw new RangeError('A.A.A.A - ' + value)
			}

			this.first = value >> 24 & 255
			this.second = value >> 16 & 255
			this.third = value >> 8 & 255
			this.fourth = value & 255

			this.numvalue = value
			this.strvalue = `${this.first}.${this.second}.${this.third}.${this.fourth}`
			this.valid = true
		} else {
			throw new TypeError('Invalid input: ' + typeof value)
		}
	}
}

export {SypexIPAddress}
