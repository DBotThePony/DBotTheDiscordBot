
// Half-Life StatsX: Community Edition v2
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import fs = require('fs')
import util = require('util')

const fs_stat = util.promisify(fs.stat)
const fs_open = util.promisify(fs.open)
const fs_read = util.promisify(fs.read)

class MemoryMappedFilePointer {
	pointer = 0

	constructor(public mmap: MemoryMappedFile) {

	}

	seek(pos: number) {
		if (pos < 0 || pos >= this.mmap.length!) {
			throw new RangeError('Invalid seek position: ' + pos)
		}

		this.pointer = pos
		return this
	}

	reset() {
		return this.seek(0)
	}

	async nextByte() {
		this.pointer++
		return await this.mmap.index(this.pointer - 1)
	}

	async nextChar() {
		this.pointer++
		return await this.mmap.char(this.pointer - 1)
	}

	async nextBytes(len: number) {
		this.pointer += len
		return await this.mmap.bytes(this.pointer - len, len)
	}

	async nextBuffer(len: number) {
		this.pointer += len
		return await this.mmap.buffer(this.pointer - len, len)
	}

	async nextChars(len: number) {
		this.pointer += len
		return await this.mmap.chars(this.pointer - len, len)
	}

	async nextUInt8() {
		return await this.nextByte()
	}

	async nextUInt16() {
		this.pointer += 2
		return await this.mmap.uint16(this.pointer - 2)
	}

	async nextUInt16LE() {
		this.pointer += 2
		return await this.mmap.uint16LE(this.pointer - 2)
	}

	async nextUInt24() {
		this.pointer += 3
		return await this.mmap.uint24(this.pointer - 3)
	}

	async nextUInt24LE() {
		this.pointer += 3
		return await this.mmap.uint24LE(this.pointer - 3)
	}

	async nextUInt32() {
		this.pointer += 4
		return await this.mmap.uint32(this.pointer - 4)
	}

	async nextUInt32LE() {
		this.pointer += 4
		return await this.mmap.uint32LE(this.pointer - 4)
	}

	async nextInt8() {
		this.pointer += 1
		return await this.mmap.int8(this.pointer - 1)
	}

	async nextInt16() {
		this.pointer += 2
		return await this.mmap.int16(this.pointer - 2)
	}

	async nextInt16LE() {
		this.pointer += 2
		return await this.mmap.int16LE(this.pointer - 2)
	}

	async nextInt24() {
		this.pointer += 3
		return await this.mmap.int24(this.pointer - 3)
	}

	async nextInt24LE() {
		this.pointer += 3
		return await this.mmap.int24LE(this.pointer - 3)
	}

	async nextInt32() {
		this.pointer += 4
		return await this.mmap.int32(this.pointer - 4)
	}

	async nextInt32LE() {
		this.pointer += 4
		return await this.mmap.int32LE(this.pointer - 4)
	}

	async nextFloat() {
		this.pointer += 4
		return await this.mmap.float(this.pointer - 4)
	}

	async nextDouble() {
		this.pointer += 8
		return await this.mmap.double(this.pointer - 8)
	}

	async nextBuffString() {
		const readstr = await this.mmap.readBuffString(this.pointer)
		this.pointer += readstr.length + 1
		return readstr
	}

	async nextString() {
		const readstr = await this.mmap.readBuffString(this.pointer)
		this.pointer += readstr.length + 1
		return readstr.toString('utf8')
	}
}

class MemoryMappedFile {
	static BLOCK_SIZE = 4 * 1024

	isopen = false
	stat: fs.Stats | null = null
	fd: number | null = null
	buff: (Buffer | null)[] | null = null
	POINTER = new MemoryMappedFilePointer(this)
	loadedSectors = 0

	get length() {
		return this.stat && this.stat.size
	}

	constructor(public path: string) {

	}

	getNewPointer() {
		return new MemoryMappedFilePointer(this)
	}

	async bytes(offset: number, len: number) {
		const bytes = []

		for (let i = 0; i < len; i++) {
			bytes.push(await this.index(i + offset))
		}

		return bytes
	}

	async buffer(offset: number, len: number) {
		const bytes = Buffer.alloc(len)

		for (let i = 0; i < len; i++) {
			bytes[i] = await this.index(i + offset)
		}

		return bytes
	}

	async char(offset: number) {
		return String.fromCharCode(await this.index(offset))
	}

	async chars(offset: number, len: number) {
		return String.fromCharCode(...await this.bytes(offset, len))
	}

	async uintCustom(offset: number, size: number) {
		switch (size) {
			case 1:
				return this.uint8(offset)
			case 2:
				return this.uint16(offset)
			case 3:
				return this.uint24(offset)
			case 4:
				return this.uint32(offset)
		}

		throw new RangeError('Can not read UInt with size ' + size + '!')
	}

	async intCustom(offset: number, size: number) {
		switch (size) {
			case 1:
				return this.int8(offset)
			case 2:
				return this.int16(offset)
			case 3:
				return this.int24(offset)
			case 4:
				return this.int32(offset)
		}

		throw new RangeError('Can not read UInt with size ' + size + '!')
	}

	async uint8(offset: number) {
		return await this.index(offset)
	}

	async uint16(offset: number) {
		return await this.index(offset) * 0x100
			+ await this.index(offset + 1)
	}

	async uint16LE(offset: number) {
		return await this.index(offset)
			+ await this.index(offset + 1) * 0x100
	}

	async uint24(offset: number) {
		return await this.index(offset) * 0x10000
			+ await this.index(offset + 1) * 0x100
			+ await this.index(offset + 2)
	}

	async uint24LE(offset: number) {
		return await this.index(offset)
			+ await this.index(offset + 1) * 0x100
			+ await this.index(offset + 2) * 0x10000
	}

	async uint32(offset: number) {
		return await this.index(offset) * 0x1000000
			+ await this.index(offset + 1) * 0x10000
			+ await this.index(offset + 2) * 0x100
			+ await this.index(offset + 3)
	}

	async uint32LE(offset: number) {
		return await this.index(offset)
			+ await this.index(offset + 1) * 0x100
			+ await this.index(offset + 2) * 0x10000
			+ await this.index(offset + 3) * 0x1000000
	}

	async float(offset: number) {
		return (await this.buffer(offset, 4)).readFloatBE(0)
	}

	async double(offset: number) {
		return (await this.buffer(offset, 4)).readDoubleBE(0)
	}

	async int8(offset: number) {
		return (await this.buffer(offset, 1)).readInt8(0)
	}

	async int16(offset: number) {
		return (await this.buffer(offset, 2)).readInt16BE(0)
	}

	async int16LE(offset: number) {
		return (await this.buffer(offset, 2)).readInt16LE(0)
	}

	async int24(offset: number) {
		const val = await this.int16(offset)

		if (val < 0) {
			return val - await this.uint8(offset + 3)
		}

		return val + await this.uint8(offset + 3)
	}

	async int24LE(offset: number) {
		const val = await this.int16LE(offset)

		if (val < 0) {
			return val - await this.uint8(offset + 3)
		}

		return val + await this.uint8(offset + 3)
	}

	async int32(offset: number) {
		return (await this.buffer(offset, 4)).readInt32BE(0)
	}

	async int32LE(offset: number) {
		return (await this.buffer(offset, 4)).readInt32LE(0)
	}

	async readString(offset: number) {
		const chars = []
		let readNext = await this.index(offset)

		while (readNext != 0) {
			chars.push(readNext)
			offset++
			readNext = await this.index(offset)
		}

		return String.fromCharCode(...chars)
	}

	async readBuffString(offset: number) {
		const chars = []
		let readNext = await this.index(offset)

		while (readNext != 0) {
			chars.push(readNext)
			offset++
			readNext = await this.index(offset)
		}

		return Buffer.from(chars)
	}

	async index(num: number) {
		if (!this.isopen) {
			//throw new Error('File is not already mapped!')
			await this.open()
		}

		if (num < 0) {
			throw new RangeError('Negative offset: ' + num)
		}

		if (num >= this.stat!.size) {
			throw new RangeError('EOF: ' + num + '; trying to read ' + (num - this.stat!.size) + ' extra bytes after end of file')
		}

		const sector = Math.floor(num / MemoryMappedFile.BLOCK_SIZE)

		//if (sector >= this.buff!.length) {
		//  throw new RangeError('Tried to read byte at ' + num + ' which is not present in file!')
		//}

		if (this.buff![sector] == null) {
			this.buff![sector] = Buffer.alloc(MemoryMappedFile.BLOCK_SIZE)
			//console.log('Load sector ' + sector)
			this.loadedSectors++
			await fs_read(this.fd!, this.buff![sector]!, 0, MemoryMappedFile.BLOCK_SIZE , sector * MemoryMappedFile.BLOCK_SIZE)
		}

		return this.buff![sector]![num - sector * MemoryMappedFile.BLOCK_SIZE]
	}

	async open() {
		if (this.isopen) {
			return this
		}

		this.stat = await fs_stat(this.path)
		this.fd = await fs_open(this.path, 'r')
		this.isopen = true
		this.buff = Array(Math.ceil(this.stat.size / MemoryMappedFile.BLOCK_SIZE) + 1)
		this.loadedSectors = 0
		for (let i = 0; i < this.buff.length; i++) { this.buff[i] = null }

		return this
	}
}

export {MemoryMappedFile, MemoryMappedFilePointer}
