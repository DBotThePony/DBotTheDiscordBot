
// Half-Life StatsX: Community Edition v2
// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import { MemoryMappedFile } from "./MemoryMappedFile";
import { SypexIPAddress } from "./IPAddress";
import { SypexDBPack, SypexPackEntry } from "./SypexPackFormat";

enum SypexRecordType {
	CITY,
	COUNTRY,
}

class SypexDBRecord {
	ipEnds: number | null = null
	entryRegion: SypexPackEntry | null = null
	entry: SypexPackEntry | null = null
	entryCountry: SypexPackEntry | null = null
	entryCity: SypexPackEntry | null = null
	rawData: Buffer | null = null

	constructor(public db: SypexDB, public ipStart: number, public recordID: number) {

	}

	async readEntry() {
		if (this.entry != null) {
			return this.entry
		}

		if (this.recordID == 0) {
			return null
		}

		this.rawData = await this.db.mmap.buffer(this.recordOffset, this.maximalLength!)

		if (this.recordType == SypexRecordType.CITY) {
			this.entry = await this.db.headerPack!.readCity(this.recordOffset)
			this.entryCity = this.entry

			if (this.regionOffset) {
				this.entryRegion = await this.db.headerPack!.readRegion(this.regionOffset)

				if (this.countryOffset) {
					this.entryCountry = await this.db.headerPack!.readCountry(this.countryOffset)
				}
			}
		} else {
			this.entry = await this.db.headerPack!.readCountry(this.recordOffset)
			this.entryCountry = this.entry
		}

		return this.entry
	}

	get maximalLength() {
		if (this.recordType == SypexRecordType.CITY) {
			return this.db.maxCitySize
		}

		return this.db.maxCountrySize
	}

	get regionOffset() {
		if (this.entry == null || !this.entry.get('region_seek')) {
			return null
		}

		return this.db.db_regions_offset! + this.entry.get('region_seek')!.numeric!
	}

	get countryOffset() {
		if (this.entryRegion == null || !this.entryRegion.get('country_seek')) {
			return null
		}

		return this.db.db_cities_offset! + this.entryRegion.get('country_seek')!.numeric!
	}

	get recordOffset() {
		if (this.recordType == SypexRecordType.CITY) {
			//return this.db.db_cities_offset! + this.db.maxCitySize! * this.recordID
			//return this.db.db_cities_offset!
			return this.db.db_cities_offset! + this.recordID
			//return this.db.db_cities_offset! + 453129
		}

		//return this.db.db_countries_offset! + this.recordID
		// щито
		return this.db.db_cities_offset! + this.recordID
	}

	get startIP() {
		return new SypexIPAddress(this.ipStart)
	}

	get ends() {
		return this.ipEnds != null
	}

	get valid() {
		return this.entry != null && this.entryCountry != null
	}

	// А вот тут все веселее
	// оригинальный кусок кода на PHP
	/*
	if($seek < $this->country_size){
		$country = $this->readData($seek, $this->max_country, 0);
		$city = $this->unpack($this->pack[2]);
		$city['lat'] = $country['lat'];
		$city['lon'] = $country['lon'];
		$only_country = true;
	}
	else {
		$city = $this->readData($seek, $this->max_city, 2);
		$country = array('id' => $city['country_id'], 'iso' => $this->id2iso[$city['country_id']]);
		unset($city['country_id']);
	}
	*/
	// Чо, тип записи надо гадать?
	get recordType() {
		if (this.recordID < this.db.countryRegistrySize!) {
			return SypexRecordType.COUNTRY
		}

		return this.db.rangeRecordSize == 3 ? SypexRecordType.CITY : SypexRecordType.COUNTRY
	}

	get endIP() {
		if (this.ipEnds == null) {
			return null
		}

		return new SypexIPAddress(this.ipEnds)
	}
}

class SypexDB {
	mmap = new MemoryMappedFile(this.path)
	get POINTER() { return this.mmap.POINTER }

	valid = false

	constructor(public path: string) {

	}

	version: number | null = null
	timestamp: number | null = null
	parser: number | null = null
	codepage: number | null = null
	indexSize: number | null = null
	primaryIndexSize: number | null = null
	blocksInsideIndexCell: number | null = null
	rangeRecords: number | null = null
	rangeRecordSize: number | null = null

	get realRecordSize() {
		if (this.rangeRecordSize == null) {
			return null
		}

		return this.rangeRecordSize + 3
	}

	maxRegionSize: number | null = null
	maxCitySize: number | null = null

	regionRegistrySize: number | null = null
	cityRegistrySize: number | null = null

	maxCountrySize: number | null = null
	countryRegistrySize: number | null = null

	packingRegionInfoSize: number | null = null
	packingRegionData: string | null = null

	header_ending: number | null = null

	get second_block_offset() {
		if (this.header_ending == null) {
			return null
		}

		return this.blocksInsideIndexCell! * this.realRecordSize!
	}

	get index_offset() {
		if (this.header_ending == null) {
			return null
		}

		return this.header_ending
	}

	get primary_index_offset() {
		if (this.header_ending == null) {
			return null
		}

		return this.header_ending + this.indexSize! * 4
	}

	get ranges_offset() {
		if (this.header_ending == null) {
			return null
		}

		return this.primary_index_offset! + this.primaryIndexSize! * 4
	}

	get db_offset() {
		return this.db_regions_offset
	}

	get db_regions_offset() {
		if (this.header_ending == null) {
			return null
		}

		return this.ranges_offset! + this.rangeRecords! * this.realRecordSize!
	}

	get db_cities_offset() {
		if (this.header_ending == null) {
			return null
		}

		return this.db_regions_offset! + this.regionRegistrySize!
	}

	get db_countries_offset() {
		if (this.header_ending == null) {
			return null
		}

		return this.db_cities_offset! + this.cityRegistrySize!
	}

	async findOffset(firstOctet: number) {
		if (!this.valid) {
			throw new Error('Database is not valid!')
		}

		if (firstOctet < 0 || firstOctet > 255) {
			throw new RangeError('Octet out of range: ' + firstOctet)
		}

		if (firstOctet > this.indexSize!) {
			return null
		}

		const val = await this.mmap.uint32(this.index_offset! + firstOctet * 4)

		if (val == 0) {
			return null
		}

		return val
	}

	async findRange(firstOctet: number) {
		if (!this.valid) {
			throw new Error('Database is not valid!')
		}

		if (firstOctet < 1 || firstOctet > 255) {
			throw new RangeError('Octet out of range: ' + firstOctet)
		}

		const minimalOffset = await this.findOffset(firstOctet - 1)
		const maximalOffset = await this.findOffset(firstOctet)

		if (minimalOffset == null || maximalOffset == null) {
			return null
		}

		if (maximalOffset - minimalOffset < this.blocksInsideIndexCell!) {
			return [minimalOffset, minimalOffset]
		}

		return [minimalOffset, maximalOffset]
	}

	async walkIndex(offset: number, length: number) {
		if (!this.valid) {
			throw new Error('Database is not valid!')
		}

		const blocks = []

		for (let i = 0; i <= length; i++) {
			blocks.push(await this.mmap.uint32(offset * 4 + i * 4))
		}

		return blocks
	}

	// Returns blocks range which can contain target IP
	async scanIndex(offset: number, max: number, firstOctet: number, secondOctet: number, thirdOctet: number, fourthOctet: number) {
		if (!this.valid) {
			throw new Error('Database is not valid!')
		}

		const maxsize = this.primaryIndexSize!

		// returns true whenever target ip is less than ours
		const calc = firstOctet * 0x1000000 + secondOctet * 0x10000 + thirdOctet * 0x100 + fourthOctet

		// Increasing minimal value
		while (offset >= 0 && offset <= maxsize) {
			const ip = await this.mmap.uint32(this.primary_index_offset! + offset * 4)

			if (ip < calc) {
				offset++
			} else {
				//offset--
				break
			}
		}

		let minP = offset
		offset = max

		// Decreasming maximal value
		while (offset >= 0 && offset <= maxsize) {
			const ip = await this.mmap.uint32(this.primary_index_offset! + offset * 4)
			//console.log(new SypexIPAddress(ip).strvalue, 'vs', new SypexIPAddress(calc).strvalue)

			if (ip > calc) {
				offset--
			} else {
				offset++
				break
			}
		}

		//console.log(minP, offset)

		if (minP > offset) {
			return offset
		} else if (minP == offset) {
			return offset
		} else {
			return minP
		}
	}

	// Scans primary index and find specific block inside it
	// then it returns in-file offset which can contain required IP
	async scanForBlock(firstOctet: number, secondOctet: number, thirdOctet: number, fourthOctet: number) {
		if (!this.valid) {
			throw new Error('Database is not valid!')
		}

		const range = await this.findRange(firstOctet)

		if (range == null) {
			return null
		}

		if (range[1] - range[0] < this.blocksInsideIndexCell!) {
			return range
		}

		//console.log(range[1] - range[0])

		const scan = await this.scanIndex(Math.floor(range[0] / this.blocksInsideIndexCell!), Math.floor(range[1] / this.blocksInsideIndexCell!) - 1, firstOctet, secondOctet, thirdOctet, fourthOctet)

		// я нхр не понял что они вообще крутят в своем API
		// и я после того как побился над спецификацией 4 дня решил скопировать некоторые куски логики
		// оффсеты наполовину относительные, наполовину абсолютные. что курили?
		let min = scan > 0 ? scan * this.blocksInsideIndexCell! : 0
		let max = scan > this.primaryIndexSize! ? this.rangeRecords! : (scan + 1) * this.blocksInsideIndexCell!

		if (min < range[0]) {
			min = range[0]
		}

		if (max > range[1]) {
			max = range[1]
		}

		return [min, max]
	}

	// Offset is a block offset
	// и тут я тоже нихера не понял
	/*
	async readRanges(offset: number, ip: SypexIPAddress) {
		if (!this.valid) {
			throw new Error('Database is not valid!')
		}

		if (this.rangeRecords! <= offset || offset < 0) {
			throw new RangeError('Attempt to read a record out of bounds: ' + offset)
		}

		//const initialOffset = this.ranges_offset! + offset * this.blocksInsideIndexCell! * this.realRecordSize!
		// щито
		const initialOffset = offset * this.blocksInsideIndexCell! * this.realRecordSize!
		console.log('Initial ' , initialOffset)

		let lastIP: number | null = null
		const calc = ip.fourth + ip.third * 0x100 + ip.second * 0x10000
		//console.log(this.blocksInsideIndexCell!)

		for (let i = 0; i < this.blocksInsideIndexCell!; i++) {
			const readIP = await this.mmap.uint24(initialOffset + i * this.realRecordSize!)
			//console.log(readIP, comparable, readIP >> 16 & 255, readIP >> 8 & 255, readIP & 255)
			//console.log(comparable >> 16 & 255, comparable >> 8 & 255, comparable & 255)

			console.log(i, new SypexIPAddress((ip.first * 0x1000000) + readIP).strvalue)

			//console.log(comparable, r)
			if (readIP < calc) {

				//lastIP = readIP
			} else {

				//console.log('READ ID ' + await this.mmap.uintCustom(initialOffset + i * this.realRecordSize! + 3, this.rangeRecordSize!))
				//console.log('AT ' + (initialOffset + i * this.realRecordSize! + 3))

				// console.log(new SypexIPAddress(calc + ip.first * 0x1000000).strvalue, 'in', new SypexIPAddress(readIP + ip.first * 0x1000000).strvalue, new SypexIPAddress(lastIP + ip.first * 0x1000000).strvalue)

				// const record = new SypexDBRecord(this,
				//  (ip.first * 0x1000000) + lastIP,
				//  await this.mmap.uintCustom(initialOffset + (i - 1) * this.realRecordSize! + 3, this.rangeRecordSize!))

				// record.ipEnds = (ip.first * 0x1000000) + readIP
				// return record
			}

			if (max - min < 2) {
				break
			}
		}

		if (max == 0) {
			return null
		}

		while (await this.mmap.uint24(initialOffset + min * this.realRecordSize!) <= calc && min < max) {
			min++
		}

		console.log(min, max, new SypexIPAddress((ip.first * 0x1000000) + await this.mmap.uint24(initialOffset + min * this.realRecordSize!)).strvalue)
		console.log('offset inside', initialOffset + min * this.realRecordSize!)

		const record = new SypexDBRecord(this,
			(ip.first * 0x1000000) + await this.mmap.uint24(initialOffset + min * this.realRecordSize!),
			await this.mmap.uintCustom(initialOffset + min * this.realRecordSize! + 3, this.rangeRecordSize!))

		return record
	}
	*/

	// под копирочку. только правда с изменениями
	// Интересно то, что читая индекс мы используем прямую адресацию
	// А ВОТ ЧИТАЯ БАЗУ ДАННЫХ - ВИРТУАЛЬНУЮ СО СДВИГОМ
	async readRanges(min: number, max: number, ip: SypexIPAddress) {
		if (!this.valid) {
			throw new Error('Database is not valid!')
		}

		const initialOffset = this.ranges_offset!

		if (min + 1 < max) {
			const calc = ip.fourth + ip.third * 0x100 + ip.second * 0x10000

			while (max - min > 8) {
				const offset = (max + min) >> 1
				const readIP = await this.mmap.uint24(initialOffset + offset * this.realRecordSize!)

				//console.log('SEARCH DB: ', new SypexIPAddress(ip.first * 0x1000000 + readIP).strvalue, ' VALUE: ', readIP)

				if (readIP < calc) {
					min = offset
				} else {
					max = offset
				}
			}

			while (calc >= await this.mmap.uint24(initialOffset + min * this.realRecordSize!) && ++min < max) {}
		} else {
			min++
		}

		const record = new SypexDBRecord(this,
			(ip.first * 0x1000000) + await this.mmap.uint24(initialOffset + min * this.realRecordSize! - 6),
			await this.mmap.uintCustom(initialOffset + min * this.realRecordSize! - 3, this.rangeRecordSize!))

		return record
	}

	async readIPRaw(ip: SypexIPAddress | null) {
		if (!this.valid || !ip) {
			return null
		}

		if (ip.first == 192 && ip.second == 168) {
			return null // :GWcorbinHolyFuck:
		}

		if (ip.first == 172 && ip.second == 16) {
			return null // :GWcorbinHolyFuck:
		}

		if (ip.first == 10 || ip.first == 127) {
			return null // :GWcorbinHolyFuck:
		}

		const blocks = await this.scanForBlock(ip.first, ip.second, ip.third, ip.fourth)

		if (blocks == null) {
			return blocks
		}

		const record = await this.readRanges(blocks[0], blocks[1], ip)

		if (record == null) {
			return null
		}

		await record.readEntry()

		return record
	}

	async readIP(ip: SypexIPAddress | null) {
		if (!this.valid || !ip) {
			return null
		}

		const record = await this.readIPRaw(ip)

		if (record == null || !record.valid) {
			return null
		}

		return record
	}

	async findIPRaw(ip: SypexIPAddress | null) { return await this.readIPRaw(ip) }
	async findIP(ip: SypexIPAddress | null) { return await this.readIP(ip) }

	headerPack: SypexDBPack | null = null

	async readHeader() {
		if (this.valid) {
			return this
		}

		const header = await this.POINTER.nextChars(3)

		if (header != 'SxG') {
			throw new Error('Invalid header. Is file a Sypex Database?')
		}

		this.version = await this.POINTER.nextByte()
		this.timestamp = await this.POINTER.nextUInt32()
		this.parser = await this.POINTER.nextByte()
		this.codepage = await this.POINTER.nextByte()
		this.indexSize = await this.POINTER.nextByte()
		this.primaryIndexSize = await this.POINTER.nextUInt16()
		this.blocksInsideIndexCell = await this.POINTER.nextUInt16()
		this.rangeRecords = await this.POINTER.nextUInt32()
		this.rangeRecordSize = await this.POINTER.nextUInt8()

		this.maxRegionSize = await this.POINTER.nextUInt16()
		this.maxCitySize = await this.POINTER.nextUInt16()

		this.regionRegistrySize = await this.POINTER.nextUInt32()
		this.cityRegistrySize = await this.POINTER.nextUInt32()

		this.maxCountrySize = await this.POINTER.nextUInt16()
		this.countryRegistrySize = await this.POINTER.nextUInt32()

		this.packingRegionInfoSize = await this.POINTER.nextUInt16()
		this.packingRegionData = await this.POINTER.nextChars(this.packingRegionInfoSize)
		this.headerPack = new SypexDBPack(this)

		//console.log(this.maxRegionSize, this.maxCitySize)

		this.header_ending = this.POINTER.pointer
		this.valid = true

		return this
	}
}

export {SypexDB}

/*
async function test() {
	const testdb = new SypexDB('SxGeoCity.dat')
	await testdb.readHeader()
	const grab = await testdb.readIP(new SypexIPAddress('109.124.18.38'))
	console.log(grab!.entry!.values)
}

test().catch(console.error)
*/

// const testdb = new SypexDB('SxGeoCity.dat')
// testdb.readHeader().then(() => {
//  testdb.readIP(new SypexIPAddress('80.83.200.79')).then((record) => {
//      //console.log(record!.entry!.values)
//  })
//  //console.log(testdb.primary_index_offset, testdb.data_offset)
// })
