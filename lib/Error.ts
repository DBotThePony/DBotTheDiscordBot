

// Copyright (C) 2017-2020 DBotThePony

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


const constructMessage = (errorMessage: string, fieldName?: string, expected?: any, got?: any) => {
	if (!fieldName) {
		return errorMessage
	}

	if (expected == undefined) {
		return errorMessage + '\n\t(field ' + fieldName + ')'
	}

	return errorMessage + ' (field ' + fieldName + ')\n\tExpected: ' + expected + '\n\tGot: ' + got
}

class InvalidStateException extends Error {
	constructor(public errorFromState: string = 'Invalid internal state', public fieldName?: string, public expected?: any, public got?: any) {
		super(constructMessage(errorFromState, fieldName, expected, got))
	}

	name = 'InvalidStateException'
}

export {InvalidStateException}
